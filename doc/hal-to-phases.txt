Mapping of HAL senzors outputs to motor phases
==============================================

  HAL HAL sector  PWM
  321 bin         1 2 3

  001  1    0     0 0 1
  101  5    1     1 0 1
  100  4    2     1 0 0
  110  6    3     1 1 0
  010  2    4     0 1 0
  011  3    5     0 1 1


Moog BN34 -> PXMC
HAL 2     -> 1
HAL 1     -> 3
HAL 3     -> 2
