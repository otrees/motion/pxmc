#include "pxmc_sim_socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
pxmc_create_socket(char *path, int mcs_num)
{
  int s, ret;
  struct sockaddr_un addr;
  
  s = socket(AF_UNIX, SOCK_DGRAM, 0);
  if (s == -1) {
    perror("pxmc_sim: socket");
    exit(1);
  }

  int yes = 1;
  if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
    perror("pxmc_sim: setsockopt");
    exit(1);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  /* Use abstract namespace */
  sprintf(&addr.sun_path[1], path, mcs_num);

  ret = bind(s, (struct sockaddr*)&addr, sizeof(addr));
  if (ret != 0) {
    perror("pxmc_sim: bind");
    close(s);
    exit(1);
  }
  return s;  
}

