//
// C++ Interface: 
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef PXMCMOTORWIDGET_H
#define PXMCMOTORWIDGET_H

#include <QWidget>
#include <QThread>
#include <QMutex>
#include "ui_motor_widget.h"
#include "../pxmc_sim_socket.h"

class PXMCMotorWidget : public QWidget, private Ui::Form
{
  Q_OBJECT

public:
  PXMCMotorWidget(int motor_num, QWidget* parent = 0, Qt::WFlags fl = 0 );
  ~PXMCMotorWidget();
  /*$PUBLIC_FUNCTIONS$*/

public slots:
  /*$PUBLIC_SLOTS$*/

protected:
  /*$PROTECTED_FUNCTIONS$*/
  int socket;
  
protected slots:
  /*$PROTECTED_SLOTS$*/
  void display_data(struct pxmc_to_gui data);

private:
};

#endif

