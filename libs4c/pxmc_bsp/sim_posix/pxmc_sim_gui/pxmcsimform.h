//
// C++ Interface: 
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef PXMCSIMFORM_H
#define PXMCSIMFORM_H

#include <QDialog>
#include "ui_pxmc_sim_form.h"
#include "pxmcmotorwidget.h"
#include <QVector>

class PXMCSimForm : public QDialog, private Ui::MotorForm
{
  Q_OBJECT

public:
  PXMCSimForm(QWidget* parent = 0, Qt::WFlags fl = 0 );
  ~PXMCSimForm();
  /*$PUBLIC_FUNCTIONS$*/

public slots:
  /*$PUBLIC_SLOTS$*/

protected:
  /*$PROTECTED_FUNCTIONS$*/
  QVector<PXMCMotorWidget*> motor_widgets;
  
  

protected slots:
  /*$PROTECTED_SLOTS$*/

private:
    void add_motor();
};

#endif

