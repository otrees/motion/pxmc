//
// C++ Implementation: commthread
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "commthread.h"
#include <sys/socket.h>

CommThread::CommThread(int asocket, QObject *parent)
 : QThread(parent)
{
	socket = asocket;
}


CommThread::~CommThread()
{
}


void CommThread::run()
{
	struct pxmc_to_gui data;
	
	while (1) {
		recv(socket, &data, sizeof(data), 0);
		new_data(data);
	}
}
