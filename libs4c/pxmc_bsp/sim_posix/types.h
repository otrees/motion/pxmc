#ifndef _TYPES_H
#define _TYPES_H

typedef __signed__ char __s8;
typedef unsigned char __u8;

typedef __signed__ short __s16;
typedef unsigned short __u16;

#if __INT_MAX__ == 32767
typedef __signed__ long __s32;
typedef unsigned long __u32;
#else
typedef int __s32;
typedef unsigned int __u32;
#endif

#if defined(__GNUC__) && !defined(__STRICT_ANSI__)
typedef __signed__ long long __s64;
typedef unsigned long long __u64;
#endif

#ifndef __BIT_TYPES_DEFINED__
#define __BIT_TYPES_DEFINED__

typedef __u8  uint8_t;
typedef __s8  int8_t;
typedef __u16 uint16_t;
typedef __s16 int16_t;
typedef __u32 uint32_t;
typedef __s32 int32_t;

#if defined(__GNUC__) && !defined(__STRICT_ANSI__)
typedef __s64 int64_t;
typedef __u64 uint64_t;
#endif

#endif /* !(__BIT_TYPES_DEFINED__) */


/*
 * These aren't exported outside the kernel to avoid name space clashes
 */
#ifdef __KERNEL__

typedef __s8 s8;
typedef __u8 u8;

typedef __s16 s16;
typedef __u16 u16;

typedef __s32 s32;
typedef __u32 u32;

typedef __s64 s64;
typedef __s64 u64;

#define BITS_PER_LONG 32

#endif /* __KERNEL__ */

#endif /* _H8S_TYPES_H */
