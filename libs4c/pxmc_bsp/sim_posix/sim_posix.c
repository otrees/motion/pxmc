#include "sim_posix.h"
#include <pxmc.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "pxmc_sim_socket.h"
#include <unistd.h>
#include <pthread.h>
#include <pxmc_internal.h>

#define PXMC_SIM_HZ 100

volatile unsigned pxmc_msec_counter; // miliseconds counter

#define MAX_MCS 10

int
send_to_gui(pxmc_state_t *mcs)
{
  int socket = mcs->pxms_inp_info;
  int mcs_num = mcs->pxms_out_info;
  struct sockaddr_un addr;
  struct pxmc_to_gui data = { .ap = mcs->pxms_ap, .as = mcs->pxms_as, .ene = mcs->pxms_ene };

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  /* Use abstract namespace */
  sprintf(&addr.sun_path[1], PXMC_SIM_GUI_SOCKET_PATH, mcs_num);

  return sendto(socket, &data, sizeof(data), 0, (struct sockaddr*)&addr, sizeof(addr));
}

int
read_from_gui(pxmc_state_t *mcs, struct gui_to_pxmc *from_gui)
{
  return 0;
}



static int sim_do_input(struct pxmc_state *mcs)
{
  struct gui_to_pxmc from_gui;
  int ret;

  ret = read_from_gui(mcs, &from_gui);
    
  if(mcs->pxms_flg & PXMS_ENR_m) {
    mcs->pxms_as = mcs->pxms_rs;
    mcs->pxms_ap = mcs->pxms_rp;
  } else {
    mcs->pxms_as = from_gui.ap - mcs->pxms_ap;
    mcs->pxms_ap = from_gui.ap;
  }
  return 0;
}

static int sim_do_output(struct pxmc_state *mcs)
{
  send_to_gui(mcs);
  return 0;
}

static inline void interupt_input()
{
  int var;
  pxmc_state_t *mcs;

  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENI_m - check if input (IRC) update is enabled
    if (mcs->pxms_flg&PXMS_ENI_m) {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}

/**
 * Function used to call pxms_do_con and pxms_do_out
 * functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_controller_and_output()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENR_m - check if controller is enabled
    if (mcs->pxms_flg&PXMS_ENR_m || mcs->pxms_flg&PXMS_ENO_m) {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg&PXMS_ENR_m) {
        //static int i=0;
        //i=(i+1)%200;
        //if (i==0) *DIO_PJDR ^= 2;

        pxmc_call(mcs, mcs->pxms_do_con);
        // PXMS_ERR_m - if axis in error state
        if (mcs->pxms_flg&PXMS_ERR_m) mcs->pxms_ene = 0;
      }

      // FIXME: For bushless motors, it is necessary to call do_out
      // even if the controller is not enabled.
      pxmc_call(mcs, mcs->pxms_do_out);
      // error_led(1);
    }
  }
}

/**
 * Function used to call pxms_do_gen functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_generator()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENG_m - check if requested value (position) generator is enabled
    if (mcs->pxms_flg&PXMS_ENG_m) {
      pxmc_call(mcs, mcs->pxms_do_gen);
      // error_led(~2);
    }
  }
}

/**
 * Function used to call pxms_do_deb functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_dbg()
{
  int var;
  pxmc_state_t *mcs;

  pxmc_for_each_mcs(var, mcs) {
    if (mcs->pxms_flg&PXMS_DBG_m) {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}

static int
timeval_subtract (result, x, y)
struct timespec *result, *x, *y;
{
  /* Perform the carry for the later subtraction by updating Y. */
  if (x->tv_nsec < y->tv_nsec) {
    int nsec = (y->tv_nsec - x->tv_nsec) / 1000000000 + 1;
    y->tv_nsec -= 1000000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_nsec - y->tv_nsec > 1000000000) {
    int nsec = (x->tv_nsec - y->tv_nsec) / 1000000000;
    y->tv_nsec += 1000000000 * nsec;
    y->tv_sec -= nsec;
  }
  
  /* Compute the time remaining to wait.
     `tv_nsec' is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_nsec = x->tv_nsec - y->tv_nsec;
  
  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}


static
void *pxmc_loop(void *arg)
{
  struct timespec next, now, req, rem;
  clock_gettime(CLOCK_REALTIME, &next);
  while (1) {
    interupt_input();
    interupt_generator();
    interupt_controller_and_output();
    interupt_dbg();


    next.tv_nsec += 1000000000/PXMC_SIM_HZ;
    if (next.tv_nsec > 1000000000) {
      next.tv_nsec -= 1000000000;
      next.tv_sec += 1;
    }
    clock_gettime(CLOCK_REALTIME, &now);
    timeval_subtract(&req, &next, &now);
    nanosleep(&req, &rem);

    pxmc_msec_counter+=1000/PXMC_SIM_HZ;
  }
}

static void
pxmc_atexit(void)
{
  int i;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(i, mcs) {
    close(mcs->pxms_inp_info);
  }
}

int
pxmc_initialize(void)
{
  int ret;
  int i;
  pxmc_state_t *mcs;

  atexit(pxmc_atexit);

  pxmc_for_each_mcs(i, mcs) {
    mcs->pxms_do_inp = sim_do_input;
    mcs->pxms_do_out = sim_do_output;
    mcs->pxms_do_con = pxmc_pid_con;
    mcs->pxms_inp_info = pxmc_create_socket(PXMC_SIM_MCS_SOCKET_PATH, i);
    mcs->pxms_out_info = i;
  }

  pthread_t id;
  ret = pthread_create(&id, NULL, pxmc_loop, NULL);
  if (ret != 0) {
    perror("pxmc_sim: pthread_create");
    exit(1);
  }

  return 0;
}

int
pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
  return -1;
}       

/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:        Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  return (long)PXMC_SIM_HZ;
}

