#ifndef PXMC_SIM_SOCKET_H
#define PXMC_SIM_SOCKET_H

#ifdef __cplusplus
extern "C" {
#endif

#define PXMC_SIM_GUI_SOCKET_PATH "/pxmc_sim_poxix/gui/%d"
#define PXMC_SIM_MCS_SOCKET_PATH "/pxmc_sim_poxix/mcs/%d"


struct pxmc_to_gui {
  long long ap;
  long as;
  long ene;
};

struct gui_to_pxmc {
  long long ap;
};

int
pxmc_create_socket(char *path, int mcs_num);

#ifdef __cplusplus
} 
#endif

#endif
