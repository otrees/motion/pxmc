/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cpu_def.h - low level CPU support for C programs
              atomic bit operations, interrupts and exceptions

  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

  Functions names and concept inspired by Linux kernel

 *******************************************************************/

#ifndef _CPU_DEF_H
#define _CPU_DEF_H

/* atomic access routines */

/* There should be possible to generate more optimized code
   if %0 changed to %U0 and "r" to "rn", but GCC nor assembler
   wants to switch to aa:8 or aaaa:16 instruction version */

#define __CONST_DATA_XOP_BIT(__nr,__pb,__aln) \
	case __nr: \
          __asm__ __volatile__ ( __aln " %1,%0 /*mybit 1*/\n" : \
		  "+m" (*__pb) : "i" (__nr), "r" (__pb)); \
	  break;

#define __xop_bit(nr,v,__aln) \
	({ volatile __typeof(*v) *__pv =(__typeof(*v) *)(v); \
	   unsigned short __nr=(nr); \
	   volatile char *__pb=(char*)__pv; \
	   __pb+=sizeof(*__pv)-1-(__nr)/8; \
	   __nr&=7; \
	   if(__builtin_constant_p(__nr)) \
	     switch(__nr){ \
	       __CONST_DATA_XOP_BIT(0,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(1,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(2,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(3,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(4,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(5,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(6,__pb,__aln); \
	       __CONST_DATA_XOP_BIT(7,__pb,__aln); \
	     } \
	   else \
	     __asm__ __volatile__ ( __aln " %w1,%0 /*mybit 2*/\n" : \
		  "+m" (*__pb) : "r" (__nr), "r" (__pb)); \
	   })

#define set_bit(nr,v) (*(v) |= (1<<(nr)))
#define clear_bit(nr,v) (*(v) &= ~(1<<(nr)))

#define atomic_clear_mask_b1(mask, v) (*(v) &= ~(mask))
#define atomic_set_mask_b1(mask, v) (*(v) |= (mask))

#define atomic_clear_mask_w1(mask, v) atomic_clear_mask_b1(mask, v)
#define atomic_set_mask_w1(mask, v) atomic_set_mask_b1(mask, v)

#define atomic_clear_mask_w(mask, v) atomic_clear_mask_b1(mask, v)
#define atomic_set_mask_w(mask, v) atomic_set_mask_b1(mask, v)

#define atomic_clear_mask(mask, v) atomic_clear_mask_b1(mask, v)
#define atomic_set_mask(mask, v) atomic_set_mask_b1(mask, v)

/* Arithmetic functions */

#define sat_add_slsl(__x,__y)					\
	do {							\
		typeof(__x) tmp;				\
		tmp = (__x) + (__y);				\
		if ((__x) > 0 && (__y) > 0 && tmp < 0)		\
			tmp = +0x7fffffff;			\
		else if ((__x) < 0 && (__y) < 0 && tmp >= 0)	\
			tmp = -0x80000000;			\
		(__x) = tmp;					\
	} while (0)

#define sat_sub_slsl(__x,__y)  \
    __asm__ ("	sub.l %2,%0\n" \
	"	bvc 2f:8\n" \
	"	bpl 1f:8\n" \
	"	mov.l #0x7fffffff:32,%0\n" \
	"	bt  2f:8\n" \
	"1:	mov.l #0x80000000:32,%0\n" \
	"2:\n" \
      : "=r"(__x) \
      : "0" ((long)__x), "r" ((long)__y) : "cc"); \

#define div_us_ulus(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divxu.w %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define div_ss_slss(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divxs.w %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define muldiv_us(__x,__y,__z) \
  div_ss_slss((long)(__x)*(__y),__z)

#define muldiv_ss(__x,__y,__z) \
  div_us_ulus((unsigned long)(__x)*(__y),__z)

/* Power down modes support */

#define __cpu_sleep() __asm__ __volatile__ ("sleep": : : "memory")

/* IRQ handling code */

#ifdef CONFIG_USE_EXR_LEVELS

#define __sti() __asm__ __volatile__ ("andc #0xf8,exr": : : "memory")

#define __cli() __asm__ __volatile__ ("orc  #0x07,exr": : : "memory")

#define __save_flags(x) \
  do{ \
    unsigned short __exr; \
    __asm__ __volatile__("stc exr,%0":"=m" (__exr) :  :"memory"); \
    (x)=__exr; \
  }while(0)

#define __restore_flags(x) \
  do{ \
    unsigned short __exr=(x); \
    __asm__ __volatile__("ldc %0,exr": :"m" (__exr) :"memory"); \
  }while(0)


#else /* CONFIG_USE_EXR_LEVELS */

#define __sti() __asm__ __volatile__ ("andc #0x7f,ccr": : : "memory")

#define __cli() __asm__ __volatile__ ("orc  #0x80,ccr": : : "memory")

#define __save_flags(x) \
  do{ \
    unsigned short __ccr; \
    __asm__ __volatile__("stc ccr,%0":"=m" (__ccr) :  :"memory"); \
    (x)=__ccr; \
  }while(0)

#define __restore_flags(x) \
  do{ \
    unsigned short __ccr=(x); \
    __asm__ __volatile__("ldc %0,ccr": :"m" (__ccr) :"cc","memory"); \
  }while(0)

#endif /* CONFIG_USE_EXR_LEVELS */

#define __get_vbr(x) 0

#define __get_sp(x) __asm__ __volatile__("mov.l sp,%0":"=r" (x) : :"cc")

#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")

#define cli() __cli()
#define sti() __sti()

#define save_flags(x) __save_flags(x)
#define restore_flags(x) __restore_flags(x)
#define save_and_cli(flags)   do { save_flags(flags); cli(); } while(0)

#define NR_IRQS 256

/* this struct defines the way the registers are stored on the
   stack during a system call. */

/*

#if 0
struct pt_regs {
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     a0;
  long     a1;
  long     a2;
  long     d0;
  long     orig_d0;
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4;
  unsigned vector : 12;
};
#else
struct pt_regs {
  long     d0;
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     d6;
  long     d7;
  long     a0;
  long     a1;
  long     a2;
  long     a3;
  long     a4;
  long     a5;
  long     a6;
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4;
  unsigned vector : 12;
};
#endif

typedef struct irq_handler {
  void            (*handler)(int, void *, struct pt_regs *);
  unsigned long   flags;
  void            *dev_id;
  const char      *devname;
  struct irq_handler *next;
} irq_handler_t;

irq_handler_t *irq_array[NR_IRQS];
void          *irq_vec[NR_IRQS];

int add_irq_handler(int vectno,irq_handler_t *handler);
*/

void *excptvec_get(int vectnum);

void *excptvec_set(int vectnum,void *vect);

int excptvec_initfill(void *fill_vect, int force_all);

#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#endif /* _H8S_CPU_DEF_H */
