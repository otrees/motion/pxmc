#include <pxmc.h>
#include <string.h>

pxmc_state_t mcs0={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:0,
  pxms_rs:0,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:0,
  pxms_out_info:0,
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};


pxmc_state_t mcs1={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155l*256,
  pxms_rs:0, //pxms_subdiv:8,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:0,
  pxms_out_info:0,
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|	//FIXME: nastavit spravne priznaky pro dalsi motorove struktur
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};

pxmc_state_t *pxmc_main_arr[] = {&mcs0,&mcs1};

pxmc_state_list_t pxmc_main_list = {
  pxml_arr:pxmc_main_arr,
  pxml_cnt:sizeof(pxmc_main_arr)/sizeof(pxmc_main_arr[0])
};

