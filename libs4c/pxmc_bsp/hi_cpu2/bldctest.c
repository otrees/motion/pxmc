/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_eurobot.c - 	generic multi axis motion controller
                 	for eurobot competition

  Copyright (c)2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
	  		(c)2006-2007 by Konrad Skup ahblitz@yahoo.de

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

 /*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
//#include <math.h>
#include <pxmc.h>
#include <pxmc_inp_common.h>
#include <pxmc_internal.h>

#include "pxmc_config.h"
#include "pt2048.h"

#include "bldctest.h"

//=================================
/*	 Prototypes */
//=================================
static void init_pwm(void);
static void init_irc(void);
static void init_hal(void);
static void init_sampling(void);

void index_mark_isr(void) __attribute__ ((interrupt_handler));	// zero crossing detectio interrput handler
//=================================
/* 	Definitions of some basic but
 	usefull variables. */
//=================================
int pxmc_sfikhz=2;	// Sampling freq. interrupt - how often we want to collect data
int pxmc_sfi2msec;	// No idea what for it is... - used in: pxmc_tpu_interupt_dbg
volatile unsigned pxmc_msec_counter; // miliseconds counter
extern int index_mark;
#define PWM_HZ 20000
#define PWM_MAX (((CPU_SYS_HZ / PWM_HZ) <= PWBFR1A_DTxm) ? (CPU_SYS_HZ / PWM_HZ) : PWBFR1A_DTxm)
/*struktury charakterizujici motor 0*/
pxmc_state_t mcs_left={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:motor_do_input,
  pxms_do_out:motor_do_output,
  pxms_do_con:pxmc_pid_con,
  pxms_do_ap2hw: tpu_irc_ap2hw,
  pxms_md:20000l<<8, pxms_ms:200, pxms_ma:5,
  pxms_inp_info:(long)TPU_TCNT1,//TPU_TCNT1			/*chanel TPU A,B*/
  pxms_out_info:0,//(long)PWM_PWBFR1A,			/*chanel PWM A,B*/
  pxms_ene:0,
  pxms_p: 50, pxms_i: 10, pxms_d: 50, pxms_s1: 0, pxms_s2: 0,
  pxms_me:PWM_MAX,//PXMC_PWM_PER<<8, //6144
  pxms_ptirc:bldc_ptirc, // IRC count per phase table
  pxms_ptper:bldc_ptper,	// Number of periods per table

 //* @pxms_ptofs:	Offset between table and IRC counter
  pxms_ptshift:0, // Shift of generated phase curves
  pxms_ptvang:bldc_ptirc/4,	// Angle (in irc) between rotor and stator mag. fld.

  pxms_ptptr1:bldc_phase1,
  pxms_ptptr2:bldc_phase2,
  pxms_ptptr3:bldc_phase3,
  pxms_ptamp:0x7fff,
  pxms_hal: 0x40,

  pxms_cfg:PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m |
          PXMS_CFG_HPS_m | PXMS_CFG_HDIR_m| PXMS_CFG_SMTH_m| 0x1
};

pxmc_state_t *pxmc_main_arr[] = {&mcs_left};

pxmc_state_list_t pxmc_main_list = {
  pxml_arr:pxmc_main_arr,
  pxml_cnt:sizeof(pxmc_main_arr) / sizeof(pxmc_main_arr[0])
};

// Table which keeps info for pwm order in hal sensor case
const unsigned char eurobot_bdc_hal_pos_table[8]=
{
	// h3.h2.h1	// order in case of one byte
	//0xff,2,4,3,0,1,5,0xff
  [0]=0xff,
  [5]=0,	//1 0 1 = 5
  [1]=1,	//0 0 1 = 1
  [3]=2,	//0 1 1 = 3
  [2]=3,	//0 1 0 = 2
  [6]=4,	//1 1 0 = 6
  [4]=5,	//1 0 0 = 4
  [7]=0xff,
};

//=========================================
/* 	Here are hardware independent functions
	like for motor control and so one... */
//=========================================
/**
 * pxmc_initialize - Initializes all subsystems.
 * @return:	Always 0.
 *
 * Description: This function initializes all subsystems necessary for 
 * proper work of pxmc. There are switched on: pwm, hall sensors read,
 * irc updates, sampling period is set up and the irq for index mark
 * detection.
*/
int pxmc_initialize(void)
{
		//pxmc_set_default_functions_for_all_motors(pxmc_m_brushless);
		//pxmc_set_irq();

     init_pwm(); //nastaveni HW (TPU, PWM)
     init_hal();
     init_irc();
     init_sampling();

     init_irq();

    return 0;
};

/**
 * pxmc_set_default_functions_for_all_motors - sets default control functions for all motors
 * @motor:	Kind of motor which we are going to use -> see header for
 *			full list of supported motors
 * @return:	True if no errors were detected, false in a case if there
 *			were some problems
 *
 * Description: This function sets up defaults pointers to control functions
 * for all motors which are presented in the system. The default configuration
 * depends on the motor kind.
*/
bool pxmc_set_default_functions_for_all_motors(pxmc_motor_kind_e motor)
{
	int var;
  	pxmc_state_t *mcs;
  	pxmc_for_each_mcs(var, mcs)
  	{
 		pxmc_set_default_functions(mcs, motor);
	}
  	return true;
}

/**
 * pxmc_set_default_functions - sets default control functions for given motor
 * @mcs:	Motion controller state information
 * @motor:	Kind of motor which we are going to use -> see header for
 *			full list of supported motors
 * @return:	True if no errors were detected, false in a case if there
 *			were some problems
 *
 * Description: This function sets up defaults pointers to control functions
 * for a given motor. The default configuration depends on the motor kind.
*/
inline bool pxmc_set_default_functions(pxmc_state_t *mcs,pxmc_motor_kind_e motor)
{
	switch(motor)
	{
		case pxmc_m_stepper:
			/*mcs->pxms_do_inp=pxmc_nofb_inp;
			mcs->pxms_do_con=pxmc_nofb_con;
			mcs->pxms_do_out=pxmc_nofb_out;*/
		break;
		case pxmc_m_brushless:
			mcs->pxms_do_inp=motor_do_input;	// set pointer to funct. for measuring IRC
			mcs->pxms_do_out=motor_do_output;	// set pointer to funct. control energi for outpu (PWM)
			mcs->pxms_do_con=pxmc_pid_con;	// set pointer to PID con. function  (implicit (PXMC.c) or user
			//pxmc_set_default_debug(mcs,m_dc_irc_pwm);	// !!! For compability only

		break;
		case pxmc_m_test:
			mcs->pxms_do_inp=motor_do_input_test;	// set pointer to funct. for measuring IRC
			mcs->pxms_do_out=motor_do_output_test;	// set pointer to funct. control energi for outpu (PWM)
			mcs->pxms_do_con=pxmc_pid_con;	// set pointer to PID con. function  (implicit (PXMC.c) or user
			//pxmc_set_default_debug(mcs,m_dc_irc_pwm);	// !!! For compability only

			pxmc_set_const_out(mcs,0);	// stop the motor

			pxmc_set_flag(mcs,PXMS_ENI_b);	// switch on pxms_do_inp

		break;
		default:
			return false;
	}

   return true;
}

/**
 * pxmc_sfi_isr - Interrupt Sampling Routine
 *
 * This interrupt service routine is used to call routines for different services, like pxms_do_inp, 
 * pxms_do_out and so one. It also updates the pxmc_msec_counter. It works according to interrupts 
 * and is called with freq. equal to pxmc_sfikhz. Function is set in interrupt vector table by: init_sampling() function.
*/
/* int intno, void *dev_id, struct pt_regs *regs */
void pxmc_sfi_isr(void)
{
  /* PXMS_ENI_m = enable input (IRC) update */
  interupt_input();

  /* PXMS_ENG_m - enable requested value (position) generator*/
  interupt_generator();

  /*PXMS_ENR_m - enable controller*/
  interupt_controller_and_output();

  /* PXMS_DBG_m - enable debugging */
  interupt_dbg();

    static int submsec = 0;
    submsec++;
    if (submsec==pxmc_sfikhz) {
        submsec = 0;
        pxmc_msec_counter++;
    }

  // Allow next interrupt...
  *TPU_TSR4&=~TSR4_TGFAm; //Acknowledge an interrupt
}

/**
 * interupt_input - calls pxms_do_inp function
 *
 * The function is used to call pxms_do_inp functions for all motors present on 
 * the board. It's called by pxmc_sfi_isr() during every sampling period.
*/
inline void interupt_input()
{
	int var;
  	pxmc_state_t *mcs;
  	pxmc_for_each_mcs(var, mcs)
	{
		// PXMS_ENI_m - check if input (IRC) update is enabled
		if(mcs->pxms_flg&PXMS_ENI_m)
		{
			pxmc_call(mcs,mcs->pxms_do_inp);
//      	error_led(~1);
		}
	}
}

/**
 * interupt_controller_and_output - calls pxms_do_con and pxms_do_out functions
 * 
 * The function is used to call pxms_do_con and pxms_do_out functions for all 
 * motors present on the board. It's called by pxmc_sfi_isr() during every 
 * sampling period.
*/
inline void interupt_controller_and_output()
{
	int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
		// PXMS_ENR_m - check if controller is enabled
		if(mcs->pxms_flg&PXMS_ENR_m || mcs->pxms_flg&PXMS_ENO_m)
		{

      /* If output only is enabled, we skip the controller */
      if(mcs->pxms_flg&PXMS_ENR_m)
      {
        pxmc_call(mcs,mcs->pxms_do_con);
        // PXMS_ERR_m - if axis in error state
        if(mcs->pxms_flg&PXMS_ERR_m) mcs->pxms_ene=0;
      }

      // FIXME: For bushless motors, it is necessary to call do_out
      // even if the controller is not enabled.
			pxmc_call(mcs,mcs->pxms_do_out);
      // error_led(1);
		}
	}
}

/**
 * interupt_generator - calls pxms_do_gen function
 * 
 * The function is used to call pxms_do_gen functions for
 * all motors present on the board. It's called by 
 * pxmc_sfi_isr() during every sampling period.
*/
inline void interupt_generator()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
		// PXMS_ENG_m - check if requested value (position) generator is enabled
		if(mcs->pxms_flg&PXMS_ENG_m)
		{
			pxmc_call(mcs,mcs->pxms_do_gen);
      // error_led(~2);
		}
  }
}

/**
 * interupt_dbg - calls pxms_do_deb function
 *
 * The function used to call pxms_do_deb functions for all
 * motors present on the board. It's called by 
 * pxmc_sfi_isr() during every sampling period.
*/
inline void interupt_dbg()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
		if(mcs->pxms_flg&PXMS_DBG_m)
		{
			pxmc_call(mcs,mcs->pxms_do_deb);
      // error_led(2);
		}
		if(--pxmc_sfi2msec<=0)
		{
			msec_time++;
			pxmc_sfi2msec=pxmc_sfikhz;
			/* Hook for external finite state machine */
		}
		else
			{
				/* Run ADC measurements */
			}
	}
}

/**
 * index_mark_isr_1 - finds index mark for 1st motor
 *
 * This interrupt routine is used to set up %PXMS_PHA_b flag
 * for the first motor on index mark detection. Function uses
 * TPU for index mark detection. In general the function should
 * work in such manner, that it switch off itself when the second
 * index mark occurs.
*/
void index_mark_isr_1(void)
{
   // static int led = 0;
   static int count = 2;
   pxmc_state_t *mcs = &mcs_left;

    // I think we can detect index mark after detection of hal edge.

   if (mcs->pxms_flg&PXMS_PTI_m)
   {
   count--;
    	if(count==0) { //FIXME constant is different for different motors
        	mcs->pxms_ptofs = (*TPU_TGR1A - 5l/*degrees*/*mcs->pxms_ptirc/360);
        	//pxmc_set_flag(mcs, PXMS_PTI_b); // see the 1st comment
        	pxmc_set_flag(mcs, PXMS_PHA_b);
        	count = 2; //If it is necessary to find index mark later
        	//*TPU_TIER1=0; //Disable future index mark interrupts
    	}
   }
//	  static int i=0;
 // i=(i+1)%200;
// if (i==0)
 {
 //	*DIO_PJDR ^= 2;
  	//*DIO_PJDR ^= 4;
  	*DIO_PJDR = 4;
  }

    *TPU_TSR1 &=(~TSR1_TGFAm);
}

/**
 * index_mark_isr_2 - finds index mark for 2nd motor
 *
 * This interrupt routine works in exactly the same way as
 * index_mark_isr_1(). The only one difference is that it
 * works for second motor.
*/
void index_mark_isr_2(void)
{
 	*DIO_PJDR ^= 2;
  	//*DIO_PJDR ^= 4;

    *TPU_TSR2 &=(~TSR2_TGFAm);
}

/**
 * index_mark_isr - finds index mark for 1st motor
 * 
 * This interrupt routine is used to detect index mark for the first motor. 
 * Function uses IRQ as a source information about index mark crossing. 
 * In general the function was created for HalDetector program and do one 
 * thing. It increases value of global variable: index_marking.
*/
/* static */ void index_mark_isr(void)
{
//    static int count = 2;
    pxmc_state_t *mcs = &mcs_left;

    if (mcs->pxms_flg&PXMS_PTI_m)
    {
    	//count--;
    	//if(count==0) { //FIXME constant is different for different motors
        	//mcs->pxms_ptofs = (*TPU_TGR1A - 5l/*degrees*/*mcs->pxms_ptirc/360);

        	//pxmc_set_flag(mcs, PXMS_PHA_b);
        	//index_marking++;
        	//count = 2; //If it is necessary to find index mark later
    	//}
	}
	index_marking++;
	*DIO_PJDR ^= 2;
    *DIO_PJDR ^= 4;	// for positive speed, index marking is at 1024, for negative it is 1365
  //  index_mark=1;	// using test f. 3 I detected that the index marking is in the range: 1340-1343

    *INT_ISR&=(~ISR_IRQ2Fm);
}
//=========================================
/* 	Here are hardware dependent functions
	like for motor control and so one... */
//=========================================
/*	General functions... */
//=========================================
/**
 * init_pwm - sets up PWM
 *
 * This function sets up PWM for proper work with motor.
*/
static void init_pwm(void)
{
	//     /* setup pwm */
	*SYS_MSTPCRD &= ~MSTPCRD_PWMm;

	*PWM_PWCR1 =0xc4;	//interrupt disabled, counter stopped, frequency phi/16

	*DIO_PJDDR &= ~0xf8;	// hal(d,e,f and g,h) is in as i/o
    *DIO_PJDDR |= (PJDDR_PJ0DDRm | PJDDR_PJ1DDRm | PJDDR_PJ2DDRm);	// led-s are out as i/o

    *PWM_PWOCR1 = PWOCR1_OE1Am |PWOCR1_OE1Cm|PWOCR1_OE1Em;	// enable output as pwm

    *PWM_PWCYR1 = PXMC_PWM_PER;    //constant set up period of PWM counter
    *PWM_PWCR1 |= 0xc8; // 8-start clock, ph/1	//PWCR2_CSTm
}

/**
 * init_irc - sets up IRC
 *
 * This function sets up TPU for index mark detection for 1st and 2nd motor.
*/
static void init_irc(void)
{
    /* setup tpu */
    *SYS_MSTPCRA &= ~MSTPCRA_TPUm; /* Switch on TPU module */
   TPU_PHASE_CNT_INIT(1);
   TPU_PHASE_CNT_INIT(2);
}

/**
 * init_hal - set up hall
 * 
 * This function sets up registers and I/O ports in such a way, that the read
 * of hall sensor is possible.
*/
static void init_hal(void)
{
	*PWM_PWCR2 =0xC4;	//interrupt disabled, counter stopped, frequency phi/16

    *PWM_PWOCR2 &= ~0x07;	// switch off: A,B,C for PWM - leds - now as i/o
    *PWM_PWOCR2 &= ~0xf8;	// switch off: D,E,F(and g,h) for PWM - hal - now as i/o

    *PWM_PWCR2 |= 0xc8;	//PWCR2_CSTm
}

/**
 * init_sampling - sets up TPU 
 *
 * This function sets up TPU main unit to generate interrupts with sampling
 * frequency: %pxmc_sfikhz. It also puts pxmc_sfi_isr() to the interrupt
 * vector table.
*/
static void init_sampling(void)
{
    *SYS_MSTPCRA &= ~MSTPCRA_TPUm; /* Switch on TPU module */

    *TPU_TSTR &= ~TSTR_CST4m;	/*counter n is stoped*/
    /*CNTN cleared by by TGRA, rising edge; source TGRA compare match/input capture  */
    *TPU_TCR4 = (TPCR_TPSC_F1 | TPCR_CKEG_RIS | TPCR_CCLR_TGRA);
    /*TGRA is output compare registr - . 0 output at compare match*/
    *TPU_TIOR4 |= TIOR4_IOA1m;
    /*MDn = 0x000 normal operation */
    *TPU_TMDR4 = TPMDR_MD_NORMAL;
    /*seting interupt vector - from TGRA at registr TSRn*/
    excptvec_set(EXCPTVEC_TGI4A, pxmc_sfi_isr);
    /*TimerInteruptEnableReg - enable IRQ generation from TGRA*/
    *TPU_TIER4 |= TIER4_TGIEAm;
    /*setup TGRA - sampling period*/
    *TPU_TGR4A = (CPU_SYS_HZ / 1000) / pxmc_sfikhz;
	/*TimerStatusRegistr - clear match flag */
    *TPU_TSR4 &= ~TSR4_TGFAm;
    /*Start TCNTn*/
    *TPU_TSTR |= TSTR_CST4m;
}

/**
 * init_irq - sets up IRQ
 *
 * This function set up IRQ interrupt, by putting index_mark_isr() to the 
 * interrupt vector table.
*/
void init_irq(void)
{
	// set up highest priotity for  IRQ2
	*INT_IPRB|=(IPRB_IPR4m|IPRB_IPR5m|IPRB_IPR6m);	// irq2
	// set up detection of rasing egde
	*INT_ISCRL&=(~ISCRL_IRQ2SCAm);
	*INT_ISCRL|=(ISCRL_IRQ2SCBm);
	// connect some function to the interrupts
	excptvec_set(EXCPTVEC_IRQ2,index_mark_isr);	// irq2
	// switch on IRQ2
	*INT_IER|=(IER_IRQ2Em);
}

//=========================================
/*	Control functions for brushless motor...*/
//=========================================
/**
 * motor_do_input - IRC encoder connected to H8S 2638 TPU
 * @mcs:	Motion controller state information
 * @return:	Always 0.
 *
 * Description:
 * The function checks weather %PXMS_PTI_b flag is enabled and if it is,
 * than it calls pxmc_irc_16bit_commindx() function to update pxms_ap 
 * according to IRC.
*/
inline int motor_do_input(struct pxmc_state *mcs)
{
 	short irc;
	irc=*(volatile short*)mcs->pxms_inp_info;
	pxmc_irc_16bit_update(mcs,irc);

	/* Running of the motor commutator */
	//irc=mcs->pxms_rp>>PXMC_SUBDIV(mcs);	// when enabled means without feedback
 	if(mcs->pxms_flg&PXMS_PTI_m) pxmc_irc_16bit_commindx(mcs,irc); //pxmc_irc_16bit_commindx(mcs,irc);

	return 0;
}

unsigned test_index = 0, test_counter=0;

/**
 * motor_do_output - sends/sets up proper values to PWM outputs.
 * @mcs:	Motion controller state information
 * @return:	Always 0.
 *
 * Description:
 * This function has two major tasks. Firstly if flag: %PXMS_PTI_b is not 
 * enabled, the motor_do_output tries to estimate actual position of the motor 
 * according to readings from hall sensor. Secondly, it read the values from
 * pahse table(s) according to the value of pxms_indx. Then it 
 * multiplies is by pxms_ene, and then it sends this result to PWM outputs.
*/
int motor_do_output(struct pxmc_state *mcs)
{
    unsigned short pwm1, pwm2, pwm3;
    short ene = 0;
    int indx = 0;

    if (!(mcs->pxms_flg&PXMS_PTI_m))
    {
        short ptindx;
        short ptirc = mcs->pxms_ptirc;
        short divisor = mcs->pxms_ptper * 6;
        unsigned char hal_pos;

        hal_pos = eurobot_bdc_hal_pos_table[hal_read(mcs)];

        if (hal_pos == 0xff) {
            pxmc_set_errno(mcs, PXMS_E_HAL);
            mcs->pxms_ene = 0; //FIXME is it correct to stop the motor this way?
        } else {
            ptindx = (hal_pos * ptirc + divisor / 2) / divisor;

            if (!(mcs->pxms_flg&PXMS_PTI_m)) {
                if( ((hal_pos!=mcs->pxms_hal)&&(mcs->pxms_hal!=0x40))){
                    if ((ptindx > mcs->pxms_ptindx+ptirc/2) /* underrun */||
                        (mcs->pxms_ptindx>ptindx+ptirc/2) /* overrun */){
                        ptindx=(mcs->pxms_ptindx+ptindx-ptirc)/2;
                        if (ptindx<0)
                            ptindx+=ptirc;
                        }else{
                            ptindx=(mcs->pxms_ptindx+ptindx)/2;
                        }
                        mcs->pxms_ptindx=ptindx;

                        mcs->pxms_ptofs=(mcs->pxms_ap>>PXMC_SUBDIV(mcs))+mcs->pxms_ptshift-ptindx;

                        pxmc_set_flag(mcs,PXMS_PTI_b);
                }else{
                    mcs->pxms_ptindx=ptindx;
                }
                mcs->pxms_hal = hal_pos;
            }
        }
    }

    ene = mcs->pxms_ene;

    if (ene)
    {
        indx = mcs->pxms_ptindx;
        if (ene < 0) {
            // Generating direction of stator mag. field for backward torque
            ene = -ene;
            indx -= mcs->pxms_ptvang;
            if (indx < 0) indx += mcs->pxms_ptirc;
        } else {
            // Generating direction of stator mag. field for forward torque
            indx += mcs->pxms_ptvang;
            if (indx >= mcs->pxms_ptirc)	indx -= mcs->pxms_ptirc;
        }
    }

    //indx %= mcs->pxms_ptirc;
    pwm1 = mcs->pxms_ptptr1[indx];
    pwm2 = mcs->pxms_ptptr2[indx];
    pwm3 = mcs->pxms_ptptr3[indx];

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    pwm1 = (((unsigned long)pwm1*(unsigned int)ene) >> (15+5));
    pwm2 = (((unsigned long)pwm2*(unsigned int)ene) >> (15+5));
    pwm3 = (((unsigned long)pwm3*(unsigned int)ene) >> (15+5));

      *PWM_PWBFR1A = pwm1;
      *PWM_PWBFR1C = pwm2;
      *PWM_PWBFR1E = pwm3;

    return 0;

}

/**
 * tpu_irc_ap2hw -  makes updates in pxms_ptofs and irc
 * @mcs:	Motion controller state information
 * @return:	Always 0.
 *
 * Description:
 * The function updates pxms_ptofs and reads IRC. It need to be called 
 * if pxms_ap is changed by the application.
*/
inline int tpu_irc_ap2hw(struct pxmc_state *mcs)
{//FIXME - move to processor dependent file
	short old_pos;
    if (mcs->pxms_inp_info)
    {
#ifdef PXMC_WITH_PHASE_TABLE
        pxmc_clear_flag(mcs, PXMS_PHA_b);
        old_pos = *(short *)mcs->pxms_inp_info;
        mcs->pxms_ptofs += (mcs->pxms_ap>>PXMC_SUBDIV(mcs)) - old_pos;
#endif
        *(short *)mcs->pxms_inp_info = (mcs->pxms_ap>>PXMC_SUBDIV(mcs));
    }
    return 0;
}
/****************************************
*		Debuging part...				*
****************************************/
//=========================================
/*	Control functions for testing motor...
	All functions in this section are
	deprecated. */
//=========================================
/**
 * motor_do_input_test - IRC encoder connected to H8S 2638 TPU.
 * @mcs:	Motion controller state information
 * @return:	Always 0.
 *
 * Description:
 * Function works in exactly the same manner as motor_do_input(). The only puropse
 * for it, is that it should be used whenever we make some tests with new
 * functionality of the input function.
*/
inline int motor_do_input_test(struct pxmc_state *mcs)
{
 	short irc;
	irc=*(volatile short*)mcs->pxms_inp_info;
	//irc=-mcs->pxms_ap>>PXMC_SUBDIV(mcs);
	pxmc_irc_16bit_update(mcs,irc);

	/* Running of the motor commutator */
 	if(mcs->pxms_flg&PXMS_PTI_m) pxmc_irc_16bit_commindx(mcs,irc);

	return 0;
}

/**
 * motor_do_output_test - sends/sets up proper values to PWM outputs.
 * @mcs:	Motion controller state information
 * @return:	Always 0.
 *
 * Description:
 * Function works in exactly the same manner as motor_do_output(). The only puropse
 * for it, is that it should be used whenever we make some tests with new
 * functionality of the output function.
*/
inline int motor_do_output_test(struct pxmc_state *mcs)
{
	/* short ene=0;
	ene=mcs->pxms_ene;
	if(ene>=0)
	{
		*(volatile __u16 *)(mcs->pxms_out_info) = ((ene) >> 7)&0xefff;
	}
	else
		{
			*(volatile __u16 *)(mcs->pxms_out_info) = (((-ene) >> 7))|0x1000;
		}

 	return 0; */
	
	static unsigned idx=0;	// out index in phase table
    unsigned long pwm1,pwm2,pwm3;
	short ene=mcs->pxms_ene;
	
	idx++;
	idx %= mcs->pxms_ptirc;

    pwm1 = mcs->pxms_ptptr1[idx];
    pwm2 = mcs->pxms_ptptr2[idx];
    pwm3 = mcs->pxms_ptptr3[idx];

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    pwm1 = (((unsigned long)pwm1*(unsigned int)ene) >> (15+5));
    pwm2 = (((unsigned long)pwm2*(unsigned int)ene) >> (15+5));
    pwm3 = (((unsigned long)pwm3*(unsigned int)ene) >> (15+5));

    *PWM_PWBFR1A = pwm1;
    *PWM_PWBFR1C = pwm2;
    *PWM_PWBFR1E = pwm3;

    return 0;
}

/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:        Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  return (long)pxmc_sfikhz*1000;
}


//=========================================
/*	Additional functions. */
//=========================================

/* Local Variables: */
/* c-basic-offset: 2 */
/* End */
