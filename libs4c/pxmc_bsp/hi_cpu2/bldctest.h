/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_eurobot.h - 	generic multi axis motion controller
                 	for eurobot competition

  Copyright (c)2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
	  		(c)2006-2007 by Konrad Skup ahblitz@yahoo.de

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#ifndef _PXMC_EUROBOT_H
#define _PXMC_EUROBOT_H

#include <stdbool.h>
#include <pxmc.h>
#include <mcu_regs.h>
//=================================
/* 	Definitions of some basic but
 	usefull constants in form of
	directives. */
//=================================
#ifndef PXMC_CYCLEKHZ
#define PXMC_CYCLEKHZ 20
#endif
//-------------
#ifndef PXMC_PWM_PER
#define PXMC_PWM_PER (((CPU_SYS_HZ/16)/1000)/PXMC_CYCLEKHZ)
#endif
//-------------
#ifndef PXMC_SFI_TPU_CHN
#define PXMC_SFI_TPU_CHN 4 		/* TPU channel used for timing */
#endif
//-------------
#ifndef PXMC_NR_OF_PHASES_I_
#define PXMC_NR_OF_PHASES_I_ 2048
#endif
//-------------
#ifndef PXMC_WITH_PHASE_TABLE
#define PXMC_WITH_PHASE_TABLE 1
#endif
#ifndef PXMC_USE_VANG
#define PXMC_USE_VANG 1
#endif
//=================================
/* 	Definitions of some basic but
 	usefull constants.	*/
//=================================
typedef unsigned char byte;

/**
 * enum _MOTOR_KIND_ - names of avaible motors
 * @pxmc_m_stepper: stepper motor
 * @pxmc_m_brushless: brushless motor
 * @pxmc_m_test: for tests
 *
 *	This defines kinds of motors which are or could be connected to the board.
*/
enum _MOTOR_KIND_ {pxmc_m_stepper=1,pxmc_m_brushless=2,pxmc_m_test=13};

/**
 * typedef pxmc_motor_kind_e - defines pxmc_motor_kind_e type as _MOTOR_KIND_
 *
 *	This defines new type: pxmc_motor_kind_e type as _MOTOR_KIND_.
*/
typedef enum _MOTOR_KIND_ pxmc_motor_kind_e;

extern const unsigned char eurobot_bdc_hal_pos_table[8];
//extern const unsigned char pxmc_hal_pos_table[];
extern const short bldc_phase1[PXMC_NR_OF_PHASES_I_];
extern const short bldc_phase2[PXMC_NR_OF_PHASES_I_];
extern const short bldc_phase3[PXMC_NR_OF_PHASES_I_];

extern pxmc_state_t mcs_right, mcs_left;
extern volatile unsigned pxmc_msec_counter; // miliseconds counter
extern short index_marking;	// for index marking detection
//=================================
/* 	Definitions of all functions
	used in this library.	*/
//=================================
//-----------------------------
//static void init_pwm(void);
//static void init_irc(void);
//static void init_hal(void);
//static void init_sampling(void);
int pxmc_initialize(void);
//-----------------------------
bool pxmc_set_default_functions_for_all_motors(pxmc_motor_kind_e motor);
inline bool pxmc_set_default_functions(pxmc_state_t *mcs,pxmc_motor_kind_e motor);
//inline short pxmc_set_max_energy(pxmc_state_t *mcs);
void pxmc_sfi_isr(void) __attribute__ ((interrupt_handler));	// sampling frequency interrupt handler
inline void interupt_input();
inline void interupt_controller_and_output();
inline void interupt_generator();
inline void interupt_dbg();

void index_mark_isr_1(void) __attribute__((interrupt_handler));
void index_mark_isr_2(void)__attribute__((interrupt_handler));
void index_mark_isr(void) __attribute__((interrupt_handler));
//-----------------------------
void init_irq(void);
//-----------------------------
inline int motor_do_input(struct pxmc_state *mcs);
inline int motor_do_output(struct pxmc_state *mcs);
inline int tpu_irc_ap2hw(struct pxmc_state *mcs);
//inline int pxmc_do_detection_of_zero_crossing(struct pxmc_state *mcs);	// will be removed
//-----------------------------
inline int motor_do_input_test(struct pxmc_state *mcs);// __attribute__ ((deprecated));
inline int motor_do_output_test(struct pxmc_state *mcs); //__attribute__ ((deprecated));
//inline int pxmc_do_con_test(struct pxmc_state *mcs); //__attribute__ ((deprecated));
//-----------------------------
//=================================
/* 	Definitions of some basic but
 	usefull macros. */
//=================================
#ifndef TPU_PHASE_CNT_INIT
#define TEMP_TPU_PHASE_CNT_INIT(n) \
  do{ \
  /*TimerControlRegistr - TCNT clearing disabled; count rasing adge; internal clock fi/1 */\
  *TPU_TCR##n=0; \
  /*phase counting mode 1 - how will be TCNT incrementd */\
  *TPU_TMDR##n=TPMDR_MD_PHACN1; \
  /* Input capture at rising edge on TIOCAx pin (index mark)  */\
  *TPU_TIOR##n = TIOR##n##_IOA3m; \
  /*AD conversion disabled; interrupt TGIA enabled.; */\
  *TPU_TIER##n=TIER##n##_TGIEAm; \
  excptvec_set(EXCPTVEC_TGI##n##A,index_mark_isr_##n); \
  /*start TCNT*/\
  *TPU_TSTR|=(1<<n); \
}while (0)

// TRICK: See cpp info pages, node Argument Prescan,
// parahraph "Macros that call other macros that stringify or concatenate."
#define TPU_PHASE_CNT_INIT(n) TEMP_TPU_PHASE_CNT_INIT(n)
#endif

/* ***** Debugging ******** */
unsigned test_index, test_counter;

/**
 * hal_read - reads hall sensor
 * @mcs:	Motion controller state information
 * @return:	Value of hall sensor 
 *
 * Description: This function reads and return hall sensor value. 
*/
static inline unsigned char hal_read(struct pxmc_state *mcs)
{
  return (*DIO_PORTJ>>3)&0x07;
}

#define pxmcbsp_is_home(mcs) 0 //(*DIO_PORTF&0x80)

//=================================
#endif

