/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_h2638.h - generic multi axis motion controller
                 hitachi H8S 2638 specific functions

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com
  (C) 2005 by Petr Kovacik

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_H2638_H
#define _PXMC_H2638_H

#ifndef PXMC_SFI_TPU_CHN
#define PXMC_SFI_TPU_CHN 4 		/* TPU channel used for timing */
#endif

#include <pxmc.h>

/* Default mcs structures. These can be overriden in an application by
   providing another pxmc_main_list. */
extern pxmc_state_t mcsX0, mcsX1;

int  pxmc_pwm1f_out(struct pxmc_state *mcs);

int  pxmc_tpuirc_inp(struct pxmc_state *mcs);

void pxmc_sfi_isr(void);

int pxmc_dbg_pwm12(pxmc_state_t *mcs);

int pxmc_pid_con(pxmc_state_t *mcs);

int pxmc_nofb_inp(pxmc_state_t *mcs);

int pxmc_nofb_con(pxmc_state_t *mcs);

int pxmc_nofb_out(pxmc_state_t *mcs);

int tpu_irc_ap2hw(struct pxmc_state *mcs);

/* select operating mode of axis */
int pxmc_axis_mode(pxmc_state_t *mcs, int mode);

void pxmc_dcm_init_fbmode(pxmc_state_t *mcs);

#define pxmcbsp_is_home(mcs) 0 //(*DIO_PORTF&0x80)

#endif
