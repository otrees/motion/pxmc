/* program to test PXMC (for DC motor control) on Hitashi processor H8S2638*/

/* procesor H8S/2638 ver 1.1  */
#include <stdio.h>
#include <stdint.h>
#include <cpu_def.h>
#include <mcu_regs.h>
#include <system_def.h>
#include <string.h>
#include <boot_fn.h>
#include <periph/sci_rs232.h>
#include <cmd_proc.h>
#include <stdlib.h>
#include <pxmc.h>
#include <pxmcbsp.h>

    /* void exit(int status) */
    /* { */
    /*   while(1); */
    /* } */

    

/**********************************************/
//other functions
    
 void deb_led_out(char val)
{
    if (val&1)
        *DIO_PJDR |=PJDR_PJ1DRm; 
    else
        *DIO_PJDR &=~PJDR_PJ1DRm; 

    if (val&2)
        *DIO_PJDR |=PJDR_PJ2DRm; 
    else
        *DIO_PJDR &=~PJDR_PJ2DRm; 

    if (val&4)
        *DIO_PJDR |=PJDR_PJ3DRm; 
    else
        *DIO_PJDR &=~PJDR_PJ3DRm; 
}


void  unhandled_exception(void) __attribute__ ((interrupt_handler));
/**
 * init - shadow registers, outputs..
 * 
 * Initializes P1 and P3 shadow registers, 
 * sets PJ.1, PJ.2, PJ.3 LED as outputs, 
 * initialises interrupt vector.
 */
void init()
{
#if 0    
    DIO_PADR|=0x0f;
    *DIO_PADDR=0x0f; /* A16-A19 are outputs */
#endif    
    
    /* sets shadow registers */
    DIO_P1DDR_shadow=0;
    DIO_P3DDR_shadow=0;  

    /* sets PJ.1, PJ.2, PJ.3 LED output */
    *DIO_PJDDR &= ~(PJDDR_PJ1DDRm | PJDDR_PJ2DDRm |PJDDR_PJ3DDRm);
    SHADOW_REG_SET(DIO_PJDDR,0xee); 

    *DIO_P3DR|=0xc5;
    SHADOW_REG_SET(DIO_P3DDR,0x85);
    
    /* initialises interrupt vector */
    excptvec_initfill(unhandled_exception, 0);
    
    /* shows something on debug leds */
    deb_led_out(1);     
    FlWait(1*100000);
    
}


//end other functions
/**********************************************/


/**********************************************/
//Interrupt routines

void  unhandled_exception(void)
{
    deb_led_out(5);
}

// end Interrupt routines
/**********************************************/


/**********************************************/
//command processor    


cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help={0, 0,"HELP","prints help for commands",
                        cmd_do_help,{(char*)&cmd_list}};

extern cmd_des_t *cmd_stm_default[1];

cmd_des_t const *cmd_list_default[]={
  
  &cmd_des_help,
  CMD_DES_INCLUDE_SUBLIST(cmd_stm_default),
  NULL
};

cmd_des_t const **cmd_list=cmd_list_default;



//end command processor 
/**********************************************/

/*struktury charakterizujici motor 0*/
pxmc_state_t mcsX0={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155l*256,
  pxms_rs:0, //pxms_subdiv:8,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:(long)TPU_TCNT1,//TPU_TCNT1			/*chanel TPU A,B*/
  pxms_out_info:(long)PWM_PWBFR1A,			/*chanel PWM A,B*/
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};


/*struktury charakterizujici motor 1*/
pxmc_state_t mcsX1={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155l*256,
  pxms_rs:0, //pxms_subdiv:8,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:(long)TPU_TCNT2,			/*chanel TPU C,D*/
  pxms_out_info:(long)PWM_PWBFR1C,			/*chanel PWM C,D*/
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|	//FIXME: nastavit spravne priznaky pro dalsi motorove struktur
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};

pxmc_state_t *pxmc_main_arr[] = {&mcsX0,&mcsX1};

#define SUMMOTORS  (sizeof(pxmc_main_arr)/sizeof(pxmc_main_arr[0]))

pxmc_state_list_t pxmc_main_list = {
  pxml_arr:pxmc_main_arr,
  pxml_cnt:SUMMOTORS
};
/* FIXME: Application should be able to ovverride pxmc_main_list in BSP. */

 
/******************/
/*      MAIN      */
/******************/

extern cmd_io_t cmd_io_rs232_line;

int main()
{

 cli();
  
 init();    
 
 sci_rs232_setmode(19200, 0, 0, sci_rs232_chan_default);
 
 sti();
 
/*  pxmc_initialize(); */
/*  pxmc_axis_mode(pxmc_main_list.pxml_arr[0], 4); */

 pxmc_initialize();
 /* Petr Kovacik's version needs the following initialization. FIXME */
/*  pxmc_set_pwm_tpu(); */
/*  pxmc_add_pservice_and_mode(4); /\*Macro -  mod=4 tj. all motors are DC*\/ */

 
 printf("ready\n");
 
 do{ 
     cmd_processor_run(&cmd_io_rs232_line, cmd_list_default);
 }while(1);
 
};

