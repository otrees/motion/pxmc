/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_h2638.c - generic multi axis motion controller
                 h8s2638 hrdware specific functions
 
  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com
  (C) 2005 by Petr Kovacik
  (C) 2007 by Michal Sojka

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/
#include <pxmc.h>
#include <h8s2638h.h>
#include <string.h>
#include <pxmc_h2638.h>

/*struktury charakterizujici motor 0*/
pxmc_state_t mcsX0={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155l*256,
  pxms_rs:0, //pxms_subdiv:8,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:(long)TPU_TCNT1,//TPU_TCNT1			/*chanel TPU A,B*/
  pxms_out_info:(long)PWM_PWBFR1A,			/*chanel PWM A,B*/
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};


/*struktury charakterizujici motor 1*/
pxmc_state_t mcsX1={
  pxms_flg:0,
  pxms_do_inp:0,
  pxms_do_con:0,
  pxms_do_out:0,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155l*256,
  pxms_rs:0, //pxms_subdiv:8,
  pxms_md:8000l<<8, pxms_ms:5000, pxms_ma:10,
  pxms_inp_info:(long)TPU_TCNT2,			/*chanel TPU C,D*/
  pxms_out_info:(long)PWM_PWBFR1C,			/*chanel PWM C,D*/
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:0, pxms_d:1, pxms_s1:0, pxms_s2:0,
  pxms_me:0x1800, //6144
  pxms_ptirc:40, // 2000 irc per rev, 200/4 steps /
  pxms_ptper:1,
  pxms_ptptr1:NULL,
  pxms_ptptr2:NULL,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|	//FIXME: nastavit spravne priznaky pro dalsi motorove struktur
      PXMS_CFG_HPS_m|PXMS_CFG_HDIR_m|0x1
};

pxmc_state_t *pxmc_main_arr[] = {&mcsX0,&mcsX1};

pxmc_state_list_t pxmc_main_list = {
  pxml_arr:pxmc_main_arr,
  pxml_cnt:sizeof(pxmc_main_arr)/sizeof(pxmc_main_arr[0])
};
