/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_inp_lpc_qei.c - generic multi axis motion controller
                       LPC17xx QEI input

  Copyright (C) 2001-2010 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2010 by PiKRON Ltd. - originator
                          http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <cpu_def.h>
#include "pxmc_lpc_qei_compat.h"
#include "pxmc.h"
#include "pxmc_inp_common.h"

/********************************************************************/
/* IRC from QEI  */

int pxmc_inp_lpc_qei_init(int chan)
{
  unsigned int pinsel3_mask = 0;
  unsigned int pinsel3_set  = 0;

  SC->PCONP |= (1 << 18); /*PCQEI*/
  /* SC->PCLKSEL1 &= ~(3 << 0); */
  /* SC->PCLKSEL1 |= (0 << 0); */
#ifdef PINCON
 #if IRC_A_BIT == BIT(20)
  pinsel3_mask |= 3 << ((20-16)*2);
  pinsel3_set  |= 1 << ((20-16)*2);
 #else
  #warning IRC_A_BIT is not set
 #endif

 #if IRC_B_BIT == BIT(23)
  pinsel3_mask |= 3 << ((23-16)*2);
  pinsel3_set  |= 1 << ((23-16)*2);
 #else
  #warning IRC_B_BIT is not set
 #endif

 #if IRC_I_BIT == BIT(24)
  pinsel3_mask |= 3 << ((24-16)*2);
  pinsel3_set  |= 1 << ((24-16)*2);
 #else
  #warning IRC_I_BIT is not set
 #endif

  PINCON->PINSEL3 = (PINCON->PINSEL3 & ~pinsel3_mask) | pinsel3_set; 
#else
  #warning No QEI pin configuration possible
#endif
  /* 0 .. DIRINV, 1 .. SIGMODE, 2 .. CAPMODE, 3 .. INVINX */
  QEI->QEIMAXPOS=~0UL;
  QEI->QEICONF=0x4;
  QEI->QEICON=0xd;

  return 0;
}

int
pxmc_inp_lpc_qei_inp(struct pxmc_state *mcs)
{
  /* int chan=mcs->pxms_inp_info */
  long irc;
  long pos;
  irc=QEI->QEIPOS;
  pos=irc<<PXMC_SUBDIV(mcs);
  mcs->pxms_as=pos-mcs->pxms_ap;
  mcs->pxms_ap=pos;

  /* Running of the motor commutator */
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  return 0;
}

int pxmc_inp_lpc_qei_ap2hw(struct pxmc_state *mcs)
{
  /* int chan=mcs->pxms_inp_info */
  QEI->QEICON=0xd;

  return 0;
}
