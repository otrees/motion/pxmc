/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_lpc_qei_utils.c - generic multi axis motion controller
                      LPC17xx QEI events processing and IRQ support

  Copyright (C) 2001-2011 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2011 by PiKRON Ltd. - originator
                          http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <stddef.h>
#include <string.h>
#include <pxmc.h>
#include <pxmc_lpc_qei.h>
#include "pxmc_lpc_qei_compat.h"

lpc_qei_state_t lpc_qei_state;

IRQ_HANDLER_FNC(lpc_qei_isr);

const struct {
  unsigned offs;
  unsigned bit;
  unsigned mask;
} lpc_qei_cmpos_map[LPC_QEI_CMPOS_CHAN_CNT] = {
  {offsetof(typeof(*QEI),CMPOS0),LPC_QEI_POS0_Int_b,LPC_QEI_POS0_Int_m},
  {offsetof(typeof(*QEI),CMPOS1),LPC_QEI_POS1_Int_b,LPC_QEI_POS1_Int_m},
  {offsetof(typeof(*QEI),CMPOS2),LPC_QEI_POS2_Int_b,LPC_QEI_POS2_Int_m},
};

int lpc_qei_setup_irq(lpc_qei_state_t *qst)
{
  QEI->QEICLR = ~0;
  QEI->QEIIEC = ~0;

  if(request_irq(QEI_IRQn, lpc_qei_isr, 0, "qei", &lpc_qei_state) < 0)
    return -1;

  qst->flg |= LPC_QEI_INITIALIZED_m;

  return 1;
}

int lpc_qei_setup_index_catch(lpc_qei_state_t *qst)
{
  QEI->QEICLR = LPC_QEI_INX_Int_m;
  QEI->QEIIES = LPC_QEI_INX_Int_m;

  return 0;
}

int lpc_qei_set_cmpos_irq_disable(lpc_qei_state_t *qst, int chan, long pos)
{
  long *lp;

  if(chan>=LPC_QEI_CMPOS_CHAN_CNT)
    return -1;

  lp = (long*)(((char*)QEI)+lpc_qei_cmpos_map[chan].offs);

  QEI->QEIIEC = lpc_qei_cmpos_map[chan].mask;
  *lp = pos;
  QEI->QEICLR = lpc_qei_cmpos_map[chan].mask;

  return 0;
}

int lpc_qei_set_cmpos(lpc_qei_state_t *qst, int chan, long pos)
{
  long *lp;

  if(chan>=LPC_QEI_CMPOS_CHAN_CNT)
    return -1;

  lp = (long*)(((char*)QEI)+lpc_qei_cmpos_map[chan].offs);

  QEI->QEIIEC = lpc_qei_cmpos_map[chan].mask;
  *lp = pos;
  QEI->QEICLR = lpc_qei_cmpos_map[chan].mask;
  QEI->QEIIES = lpc_qei_cmpos_map[chan].mask;

  return 0;
}

int lpc_qei_cmpos_irq_enable(lpc_qei_state_t *qst, int chan)
{
  if(chan>=LPC_QEI_CMPOS_CHAN_CNT)
    return -1;
  QEI->QEIIES = lpc_qei_cmpos_map[chan].mask;
  return 0;
}

int lpc_qei_cmpos_irq_disable(lpc_qei_state_t *qst, int chan)
{
  if(chan>=LPC_QEI_CMPOS_CHAN_CNT)
    return -1;
  QEI->QEIIEC = lpc_qei_cmpos_map[chan].mask;
  return 0;
}

long lpc_qei_get_cmpos(lpc_qei_state_t *qst, int chan)
{
  long *lp;

  if(chan>=LPC_QEI_CMPOS_CHAN_CNT)
    return -1;

  lp = (long*)(((char*)QEI)+lpc_qei_cmpos_map[chan].offs);

  return *lp;
}

long lpc_qei_get_pos(lpc_qei_state_t *qst)
{
  return QEI->QEIPOS;
}

IRQ_HANDLER_FNC(lpc_qei_isr)
{
  unsigned int events;
  unsigned int evnum;
  lpc_qei_state_t *qst = (lpc_qei_state_t*)irq_handler_get_context();

  while((events = QEI->QEIINTSTAT & QEI->QEIIE) != 0) {
    evnum = ffs(events)-1;
    QEI->QEIIEC = 1 << evnum;
    if(evnum == LPC_QEI_INX_Int_b) {
      qst->index_occ++;
      qst->index_pos = QEI->QEIPOS;
    }
    if(qst->callback[evnum])
      qst->callback[evnum](qst, qst->context[evnum]);
  }

  return IRQ_HANDLED;
}
