#ifndef _GPIO_H
#define _GPIO_H

#include "iovolatile.h"

typedef struct GPIO {
  __I  uint16_t IN;
  __IO uint16_t OUT;
  __O  uint16_t OUT_SET;
  __O  uint16_t OUT_CLR;
} GPIO_t;

#endif
