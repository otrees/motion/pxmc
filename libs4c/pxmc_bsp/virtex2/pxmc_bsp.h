#ifndef _HARDWARE_H
#define _HARDWARE_H

#include "mcc.h"
#include "quadcount.h"
#include "gpio.h"


struct GPIO GPIO0   asm("0x0140");
struct QCNT QCNT0   asm("0x0148");
struct MCC  MCC0[1] asm("0x2200");

#define MOTOR_VECTOR      0


#endif
