/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_virtex2.c - generic multi axis motion controller
                       Virtex 2 board axis dunctions

  Copyright (C) 2001-2010 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2010 by PiKRON Ltd. - originator
                          http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <string.h>
#include <stdio.h>
#include <system_def.h>
#include <cpu_def.h>
#include <pxmc.h>
#include <pxmc_internal.h>
#include "pxmc_virtex2.h"
#include "mcc.h"
#include "pxmc_bsp.h"

#define PXML_MAIN_CNT 1

/* 24MHz / (2^10) = 23,4Hz */
#define PWM_FREQ 23437,5

#define HAL_ERR_SENSITIVITY 20
#define HAL_ERR_MAX_COUNT    5


unsigned pxmc_virtex2_magnitude;

inline unsigned
pxmc_bdc_hal_rd(pxmc_state_t *mcs)
{
  /* TODO Resolve multiple motor channels */
  unsigned h=GPIO0.IN & 0x07;

  return h;
}

const unsigned char pxmc_bdc_hal_pos_table[8]=
{
  [0]=0xff,
  [7]=0xff,
  [1]=5,
  [5]=4,
  [4]=3,
  [6]=2,
  [2]=1,
  [3]=0,
};


/**
 * pxmc_virtex2_wr - Output of the 3-phase PWM to the hardware
 * WARNING Be aware that MCC must be properly configured not to compute
 * pwm itself
 * @mcs:	Motion controller state information
 */
/*static*/ inline void
pxmc_virtex2_wr(pxmc_state_t *mcs, short pwm1, short pwm2, short pwm3)
{
  int chan=mcs->pxms_out_info;

  hal_mcc_set(MCC0,chan,MCC_PWM1,pwm1);
  hal_mcc_set(MCC0,chan,MCC_PWM2,pwm2);
  hal_mcc_set(MCC0,chan,MCC_PWM3,pwm3);
}

static inline void
pxmc_virtex2_process_hal_error(struct pxmc_state *mcs)
{
  if (mcs->pxms_halerc >= HAL_ERR_SENSITIVITY*HAL_ERR_MAX_COUNT) {
    pxmc_set_errno(mcs, PXMS_E_HAL);
    mcs->pxms_ene = 0;
    mcs->pxms_halerc--;
  } else
    mcs->pxms_halerc+=HAL_ERR_SENSITIVITY;
}

/**
 * pxmc_virtex2_out - Phase output for brush-less 3-phase motor
 * @mcs:	Motion controller state information
 */
int
pxmc_virtex2_out(pxmc_state_t *mcs)
{
  int chan=mcs->pxms_out_info;
  unsigned char hal_pos;
  short pwm1;
  short pwm2;
  short pwm3;
  int indx = 0;
  short ene;
  int mcc_en_ibase=(hal_mcc_get(MCC0,chan,MCC_EN)&MCC_EN_IBASE);
  
  if(!(mcc_en_ibase || mcs->pxms_flg&PXMS_PTI_m) || (mcs->pxms_flg&PXMS_PRA_m)){
    short ptindx;
    short ptirc=mcs->pxms_ptirc;
    short divisor=mcs->pxms_ptper*6;

    hal_pos=pxmc_bdc_hal_pos_table[pxmc_bdc_hal_rd(mcs)];

    if(hal_pos == 0xff) {
      if(mcs->pxms_ene)
        pxmc_virtex2_process_hal_error(mcs);
    } else {
      if(mcs->pxms_halerc)
        mcs->pxms_halerc--;

      ptindx=(hal_pos*ptirc+divisor/2)/divisor;
      
      if((hal_pos!=mcs->pxms_hal)&&(mcs->pxms_hal!=0x40)){
        short ptindx_prev=(mcs->pxms_hal*ptirc+divisor/2)/divisor;

        if((ptindx>ptindx_prev+ptirc/2) ||
            (ptindx_prev>ptindx+ptirc/2)){
          ptindx=(ptindx_prev+ptindx-ptirc)/2;
          if(ptindx<0)
            ptindx+=ptirc;
        }else{
          ptindx=(ptindx_prev+ptindx)/2;
        }

        mcs->pxms_ptindx=ptindx;

        mcs->pxms_ptofs=(mcs->pxms_ap>>PXMC_SUBDIV(mcs))+mcs->pxms_ptshift-ptindx;

        pxmc_set_flag(mcs,PXMS_PTI_b);
        pxmc_clear_flag(mcs,PXMS_PRA_b);

        hal_mcc_set(MCC0,chan,MCC_EN,MCC_EN_VECTOR|MCC_EN_SCALE|MCC_EN_PWMMIN|MCC_EN_PWM);
        
      }else{
        if (!(mcs->pxms_flg&PXMS_PRA_m))
          mcs->pxms_ptindx=ptindx;
      }
      
      mcs->pxms_hal = hal_pos;
    }
  }

  if(!mcc_en_ibase){
    if(!(mcs->pxms_flg&PXMS_PTI_m)){
      hal_mcc_set(MCC0,chan,MCC_ANGLE,mcs->pxms_ptindx);
    }else{
      hal_mcc_set(MCC0,chan,MCC_IBASE,mcs->pxms_ptofs);
      hal_mcc_set(MCC0,chan,MCC_EN,MCC_EN_ALL);
      // TODO shall we keep PTI valid in software when using hw commutation?
      pxmc_clear_flag(mcs,PXMS_PTI_b);
    }
  }

  /*
  if(0) {
      // Check for overcurrent condition
      pxmc_set_errno(mcs, PXMS_E_WINDCURRENT);
  }
  */

  
  ene=mcs->pxms_ene;
  if(ene){
    /*
    indx=mcs->pxms_ptindx;
   #if 0
    // tuning of magnetic field/voltage advance angle
    indx+=(mcs->pxms_s1*mcs->pxms_as)>>(PXMC_SUBDIV(mcs)+8);
   #endif
    if(ene<0){
      // Generating direction of stator mag. field for backward torque
      ene=-ene;
      if((indx-=mcs->pxms_ptvang)<0)
	indx+=mcs->pxms_ptirc;
    }else{
      // Generating direction of stator mag. field for forward torque
      if((indx+=mcs->pxms_ptvang)>=mcs->pxms_ptirc)
	indx-=mcs->pxms_ptirc;
    }
    */
    

    /*
    if(mcs->pxms_ptscale_mult)
      indx=((unsigned long)indx*mcs->pxms_ptscale_mult)>>mcs->pxms_ptscale_shift;
    */

    /*
    pwm1=mcs->pxms_ptptr1[indx];
    pwm2=mcs->pxms_ptptr2[indx];
    pwm3=mcs->pxms_ptptr3[indx];
    */

   #ifdef PXMC_WITH_PT_ZIC
    //if(labs(mcs->pxms_as)>(10<<PXMC_SUBDIV(mcs))) {
      /*
      if(pwm1&PXMC_PT_ZIC_MASK){
        hal_gpio_set_value(PWM1_EN_PIN,0);
      } else {
        hal_gpio_set_value(PWM1_EN_PIN,1);
      }
      if(pwm2&PXMC_PT_ZIC_MASK){
        hal_gpio_set_value(PWM2_EN_PIN,0);
      } else {
        hal_gpio_set_value(PWM2_EN_PIN,1);
      }
      if(pwm3&PXMC_PT_ZIC_MASK){
        hal_gpio_set_value(PWM3_EN_PIN,0);
      } else {
        hal_gpio_set_value(PWM3_EN_PIN,1);
      }
      */
    //}else{
      /*
      hal_gpio_set_value(PWM1_EN_PIN,1);
      hal_gpio_set_value(PWM2_EN_PIN,1);
      hal_gpio_set_value(PWM3_EN_PIN,1);
      */
    //}
    /*
    pwm1&=~PXMC_PT_ZIC_MASK;
    pwm2&=~PXMC_PT_ZIC_MASK;
    pwm3&=~PXMC_PT_ZIC_MASK;
    */
   #endif /*PXMC_WITH_PT_ZIC*/

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    /*
    {
      unsigned long pwm_dc = pxmc_virtex2_magnitude*(unsigned long)ene;
      pwm1=((unsigned long long)pwm1 * pwm_dc)>>(15+15);
      pwm2=((unsigned long long)pwm2 * pwm_dc)>>(15+15);
      pwm3=((unsigned long long)pwm3 * pwm_dc)>>(15+15);
    }
    */
    //pxmc_virtex2_wr(mcs,pwm1,pwm2,pwm3);
    
    hal_mcc_set(MCC0,chan,MCC_ACTION,mcs->pxms_ene>>3);
  }else{
    hal_mcc_set(MCC0,chan,MCC_ACTION,0);
  }

  return 0;
}


int
pxmc_virtex2_init(pxmc_state_t *mcs, int mode)
{
  /* TODO */
  //pxmc_virtex2_magnitude = 1024;

  int chan=mcs->pxms_inp_info;
  
  hal_mcc_set(MCC0,chan,MCC_IBASE,0);
  hal_mcc_set(MCC0,chan,MCC_IPER,1000);
  hal_mcc_set(MCC0,chan,MCC_ACTION,0);
  hal_mcc_set(MCC0,chan,MCC_EN,MCC_EN_VECTOR | MCC_EN_SCALE | MCC_EN_PWMMIN | MCC_EN_PWM);
  
  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  /*return pxmc_hh_gi;*/
  return NULL;
}

pxmc_state_t mcs0={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_inp_virtex2_inp,
  pxms_do_con:pxmc_pid_con,
  pxms_do_out:pxmc_virtex2_out,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:NULL, //WARNING What is it used for?
  pxms_ap:0, pxms_as:0,
  pxms_rp: 55*256, pxms_rs:0,
 #ifndef PXMC_WITH_FIXED_SUBDIV
  pxms_subdiv:8,
 #endif
  pxms_md:8000L<<8, pxms_ms:10000L<<8, pxms_ma:1,
  pxms_inp_info:0,
  pxms_out_info:0,
  pxms_ene:0, pxms_erc:0,
  pxms_p:40, pxms_i:30, pxms_d:100, pxms_s1:200, pxms_s2:0,
  pxms_me:0x0400<<3,  // 'pxms_me' must be minimally 2x smaller then MAX_SHORT
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_I2PT_m*0|0x1,
  pxms_ptper:1,
  pxms_ptirc:1000,
  //pxms_ptvang:250,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
};


pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT]=
			{&mcs0};


pxmc_state_list_t  pxmc_main_list={
  pxml_arr:pxmc_main_arr,
  pxml_cnt:0
};

/* TODO */
void pxmc_virtex2_callback_index(void *state, void *context)
{
  /*
  pxmc_state_t *mcs=(pxmc_state_t *)context;
  short ofs;
  short irc;
  irc=read_index_position();
  */

  /*
  if((mcs->pxms_cfg & PXMS_CFG_I2PT_m) && (mcs->pxms_flg&PXMS_PTI_m)) {
    short diff;
    ofs=irc-mcs->pxms_ptmark;
    diff=ofs-mcs->pxms_ptofs;
    if(diff>=mcs->pxms_ptirc/2)
      diff-=mcs->pxms_ptirc;
    if(diff<=-mcs->pxms_ptirc/2)
      diff+=mcs->pxms_ptirc;
    if(diff<0)
      diff=-diff;
    if(diff>=mcs->pxms_ptirc/6) {
      pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
    } else {
      mcs->pxms_ptofs = ofs;
      pxmc_set_flag(mcs,PXMS_PHA_b);
    }
  }*/ /*else {
    ofs=irc-mcs->pxms_ptofs;
    if((unsigned short)ofs>=(unsigned short)mcs->pxms_ptirc) {
      if(ofs>0) {
        ofs-=mcs->pxms_ptirc;
      } else {
        ofs+=mcs->pxms_ptirc;
      }
    }
    mcs->pxms_ptmark=ofs;
  }*/

  /*lpc_qei_set_cmpos(qst, 0, qst->index_pos-4000);*/
}

static inline void pxmc_sfi_input(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    /* PXMS_ENI_m - check if input (IRC) update is enabled */
    if (mcs->pxms_flg&PXMS_ENI_m) {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}

static inline void pxmc_sfi_controller_and_output(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    /* PXMS_ENR_m - check if controller is enabled */
    if (mcs->pxms_flg&PXMS_ENR_m || mcs->pxms_flg&PXMS_ENO_m) {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg&PXMS_ENR_m) {

        pxmc_call(mcs, mcs->pxms_do_con);
        /* PXMS_ERR_m - if axis in error state */
        if (mcs->pxms_flg&PXMS_ERR_m) mcs->pxms_ene = 0;
      }

      /* for bushless motors, it is necessary to call do_out
        even if the controller is not enabled and PWM should be provided. */
      pxmc_call(mcs, mcs->pxms_do_out);
    }
  }
}

static inline void pxmc_sfi_generator(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    /* PXMS_ENG_m - check if requested value (position) generator is enabled */
    if (mcs->pxms_flg&PXMS_ENG_m) {
      pxmc_call(mcs, mcs->pxms_do_gen);
    }
  }
}

static inline void pxmc_sfi_dbg(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    if (mcs->pxms_flg&PXMS_DBG_m) {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}

void pxmc_sfi_isr(void)
{
  pxmc_sfi_input();
  pxmc_sfi_controller_and_output();
  pxmc_sfi_generator();
  pxmc_sfi_dbg();
}

int pxmc_initialize(void)
{
  int res;

  pxmc_state_t *mcs=&mcs0;

  pxmc_virtex2_init(mcs, 0);

  pxmc_main_list.pxml_cnt=0;
  pxmc_dbg_hist=NULL;
  __memory_barrier();
  pxmc_main_list.pxml_cnt=PXML_MAIN_CNT;

  return 0;
}
