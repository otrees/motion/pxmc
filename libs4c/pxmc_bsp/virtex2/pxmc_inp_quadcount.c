/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_inp_quadcount.c - generic multi axis motion controller
                       QuadCount VHDL IRC Encoder Support

  Copyright (C) 2001-2010 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2010 by PiKRON Ltd. - originator
                          http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <cpu_def.h>
#include "pxmc.h"
#include "pxmc_inp_common.h"
#include "pxmc_virtex2.h"
#include "pxmc_bsp.h"

/********************************************************************/
/* IRC from QEI  */

int
pxmc_inp_virtex2_inp(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  long irc;
  long pos;
  
  irc=hal_irc_get(&QCNT0,chan);
  pos=irc<<PXMC_SUBDIV(mcs);

  // TODO What about using pxmc_irc_16bit_update() from pxmc_inp_common.h
  mcs->pxms_as=pos-mcs->pxms_ap;
  mcs->pxms_ap=pos;

  /* Running of the motor commutator */
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  return 0;
}
