#ifndef _MCC_H
#define _MCC_H

#include "iovolatile.h"

typedef struct MCC {
  __IO uint16_t MCC_EN;         // 0x00 : MCC enable flags
  __IO uint16_t MCC_IRC;        // 0x01 : IRC
  __IO uint16_t MCC_IBASE;      // 0x02 : IRC base (offset between irc and phase table)
  __IO uint16_t MCC_IPER;       // 0x03 : IRC period
  __IO uint16_t MCC_ANGLE;      // 0x04 : Angle (0 <= MCC_ANGLE < MCC_IRC_PER)
  __IO  int16_t MCC_ACTION;     // 0x05 : Action
  __IO uint16_t MCC_PWMMIN;     // 0x06 : Minimal PWM value to implement modified sine wave
  __IO  int16_t MCC_SCALE;      // RESERVED 0x06 : Angle scale to phase table index
  __IO uint16_t MCC_INDEX;      // RESERVED 0x07 : Scaled Angle (index to phase table)
  __IO uint16_t MCC_TOFF;       // RESERVED 0x08 : Table base address
  __IO uint16_t MCC_P2OFF;      // RESERVED 0x09 : Phase 2 offset
  __IO uint16_t MCC_P3OFF;      // RESERVED 0x0A : Phase 3 offset
  __IO uint16_t MCC_IMASK;      // RESERVED 0x0B : Table index mask
  __IO uint16_t MCC_PIDP;       // RESERVED 0x0C : PWM PID - P
  __IO uint16_t MCC_PIDI;       // RESERVED 0x0D : PWM PID - I
  __IO uint16_t MCC_PIDD;       // RESERVED 0x0E : PWM PID - D
  
  // Phase 1
  __IO uint16_t MCC_P1;         // 0x10 : Phase1 value
  __IO uint16_t MCC_PWM1;       // 0x11 : PWM1
       uint16_t reserved2[2];   // RESERVED
  // Phase 2
  __IO uint16_t MCC_P2;         // 0x14 : Phase2 value
  __IO uint16_t MCC_PWM2;       // 0x15 : PWM2
       uint16_t reserved3[2];   // RESERVED
  // Phase 3
  __IO uint16_t MCC_P3;         // 0x18 : Phase3 value
  __IO uint16_t MCC_PWM3;       // 0x19 : PWM3
       uint16_t reserved4[2];   // RESERVED

       uint16_t reserved5[4];   // RESERVED (MCC_t pedded to 32 words)
} MCC_t;


#define MCC_EN_IRC            (1 << 0)
#define MCC_EN_IBASE          (1 << 1)
#define MCC_EN_VECTOR         (1 << 2)
#define MCC_EN_SCALE          (1 << 3)
#define MCC_EN_PWMMIN         (1 << 4)
#define MCC_EN_PWM            (1 << 5)

#define MCC_EN_ALL      (MCC_EN_IRC | MCC_EN_IBASE | MCC_EN_VECTOR | MCC_EN_SCALE | MCC_EN_PWMMIN | MCC_EN_PWM)


#define hal_mcc_get(mcc_base, i, field) \
  (((MCC_t*)mcc_base)[i].field)

#define hal_mcc_set(mcc_base, i, field, value) \
  ((MCC_t*)mcc_base)[i].field = value


#endif
