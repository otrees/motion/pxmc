#ifndef _PXMC_VIRTEX2_H
#define _PXMC_VIRTEX2_H

#include <pxmc.h>

int pxmc_inp_virtex2_inp(struct pxmc_state *mcs);

void pxmc_sfi_isr(void);

#endif /*_PXMC_VIRTEX2_H*/
