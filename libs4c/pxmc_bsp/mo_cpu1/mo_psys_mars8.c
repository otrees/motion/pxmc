/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_psys_mars8.c - position controller subsystem core generic
                and MARS8 hardware specific support

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <ctm4.h>
#include <qadc.h>
#include <tpu.h>
#include <usd_7266.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "mo_psys_fast.h"
#include <string.h>

#define PXML_MAIN_CNT 8

/********************************************************************/
/* controllers configuration */

pxmc_state_t mcs0={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  // pxms_do_out:ctm4_pwm_out,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp: 55*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:1000, pxms_ma:1,
  pxms_inp_info:0,pxms_out_info:0,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x4000/*0x7fff*/,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  0x1,
};

pxmc_state_t mcs1={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  // pxms_do_out:ctm4_pwm_out,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:155*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:1000, pxms_ma:1,
  pxms_inp_info:1,pxms_out_info:1,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x6000,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  PXMS_CFG_HPS_m|0x1,
};

pxmc_state_t mcs2={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:255*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:1000<<8, pxms_ms:2000, pxms_ma:1,
  pxms_inp_info:2,pxms_out_info:2,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x6000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  PXMS_CFG_HPS_m|PXMS_CFG_HRI_m|0x1,
};

pxmc_state_t mcs3={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:355*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:1000<<8, pxms_ms:2000, pxms_ma:1,
  pxms_inp_info:3,pxms_out_info:3,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x7000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  PXMS_CFG_HRI_m|0x2,
};

pxmc_state_t mcs4={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:455*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:2000, pxms_ma:1,
  pxms_inp_info:4,pxms_out_info:4,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x7000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  PXMS_CFG_HRI_m|PXMS_CFG_HDIR_m|0x2,
};

pxmc_state_t mcs5={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:555*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:2000, pxms_ma:1,
  pxms_inp_info:5,pxms_out_info:5,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x7000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  PXMS_CFG_HPS_m|PXMS_CFG_HRI_m|PXMS_CFG_HDIR_m|0x2,
};

pxmc_state_t mcs6={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:655*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:1000, pxms_ma:1,
  pxms_inp_info:6,pxms_out_info:6,
  pxms_ene:0, pxms_erc:0,
  pxms_p:10, pxms_i:2, pxms_d:100, pxms_s1:0, pxms_s2:0,
  pxms_me:0x6000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m,
};

pxmc_state_t mcs7={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:pxmc_fast_usd_irc,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:pxmc_fast_ctm4_pwm,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:pxmc_fast_usd_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp:755*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:1000, pxms_ma:1,
  pxms_inp_info:7,pxms_out_info:7,
  pxms_ene:0, pxms_erc:0,
  pxms_p:10, pxms_i:2, pxms_d:100, pxms_s1:0, pxms_s2:0,
  pxms_me:0x6000,
  pxms_cfg:PXMS_CFG_SMTH_m|PXMS_CFG_MD2E_m,
};

pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT]=
			{&mcs0,&mcs1,&mcs2,&mcs3,&mcs4,&mcs5,&mcs6,&mcs7};


pxmc_state_list_t  pxmc_main_list={
  pxml_arr:pxmc_main_arr,
  pxml_cnt:0
};


#ifdef PXMC_WITH_GEN_SELECTION

pxmc_call_t *pxmc_get_stop_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spdstop_gi;
}

pxmc_call_t *pxmc_get_cont_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_cont_gi;
}

pxmc_call_t *pxmc_get_spdnext_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spdnext_gi;
}

pxmc_call_t *pxmc_get_trp_gi_4axis(pxmc_state_t *mcs, int mode)
{
 #ifdef PXMC_WITH_FAST_TRPREL
  if((mcs->pxms_cfg&PXMS_CFG_CYCL_m)||(mode==PXMC_GO_REL))
    return pxmc_fast_trprel_gi;
  else
 #endif /*PXMC_WITH_FAST_TRPREL*/
    return pxmc_fast_trp_gi;
}

pxmc_call_t *pxmc_get_trp_retgt_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_trp_retgt;
}

pxmc_call_t *pxmc_get_trp_spdfg_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  return pxmc_fast_trp_spdfg_gi;
 #else /*PXMC_WITH_FINE_GRAINED*/
  return 0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
}

pxmc_call_t *pxmc_get_trprel_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FAST_TRPREL
  return pxmc_fast_trprel_gi;
 #else /*PXMC_WITH_FAST_TRPREL*/
  return 0;
 #endif /*PXMC_WITH_FAST_TRPREL*/
}

pxmc_call_t *pxmc_get_spd_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spd_gi;
}

pxmc_call_t *pxmc_get_spdfg_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  return pxmc_fast_spdfg_gi;
 #else /*PXMC_WITH_FINE_GRAINED*/
  return 0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
}

#endif /*PXMC_WITH_GEN_SELECTION*/

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_hh_gi;
}

long pxmc_sfi_sel(long sfi_hz)
{
  return ctm4_sfi_sel(sfi_hz);
}

#if 0
int pxmc_pid(pxmc_state_t *mcs)
{
  short ene;
  ene=(mcs->pxms_rp-mcs->pxms_ap)*mcs->pxms_p;
  ene+=(mcs->pxms_rs-mcs->pxms_as)*mcs->pxms_d;
  mcs->pxms_ene=ene;
  return 0;
}
#endif

#if 0
void do_pxm_control(void)
{
  pxmc_state_t *mcs;
  int i;
  mcs=&mcs0;

  /* tpu_irc_update(mcs); */
  /* usd_irc_update(mcs); */

  /* pxmc_pid(mcs); */
  __asm(
    "	movel %0,%%a4\n"
    "	jsr   pxmc_fast_pid\n"
    ::"a"(mcs)
    :"a0","a1","a2","a3","a4","d0","d1","d2","d3"
  );

  i=mcs->pxms_ene>>6;
  if((i<-100)||(i>100)) i=0;
  ctm4_pwm_set(0,i);
}
#endif

/* main controller routine called from sampling frequency irq */
void do_pxm_control(void)
{
  pxmc_fast_proc_list(pxmc_main_list.pxml_arr,
  			pxmc_main_list.pxml_cnt);
}

/* initialize pxmc_main_list */
int pxmc_initialize(void)
{
  pxmc_main_list.pxml_cnt=0;
  pxmc_dbg_hist=NULL;
  __memory_barrier();
  pxmc_main_list.pxml_cnt=PXML_MAIN_CNT;
  return 0;
}

