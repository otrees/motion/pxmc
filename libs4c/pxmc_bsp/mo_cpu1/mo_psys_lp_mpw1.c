/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_psys_mars8.c - position controller subsystem core generic
                and LP_MPW1 hardware specific support

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <ctm4.h>
#include <qadc.h>
#include <tpu.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_inp_common.h"
#include "mo_psys_fast.h"
#include <periph/tpu_sup.h>
#include <periph/tpu_func.h>
#include <string.h>

#define PXML_MAIN_CNT 1

/*
  IRC PhA  TP8
  IRC PhB  TP9
  IRC INDX TP7

  PWM EN1  TP1
  PWM EN2  TP3
  PWM EN3  TP5

  PWM IN1  PW0 .. PWM5/TPU0
  PWM IN2  PW1 .. PWM6/TPU2
  PWM IN2  PW2 .. PWM7/TPU4

  HAL IRQ3 PF3
  HAL IRQ4 PF4
  HAL IRQ5 PF5

  Valves   TP10-15

*/

#define MO_CTM4PWM3F_EN1_CH  1
#define MO_CTM4PWM3F_EN2_CH  3
#define MO_CTM4PWM3F_EN3_CH  5

/********************************************************************/
/* HAL sensor readback */

int
mo_bdc_hal_init(pxmc_state_t *mcs)
{
  *SIM_DDRF&=~(0x7<<3);
  *SIM_PFPAR&=~(0x7<<3);
  return 0;
}

inline unsigned
mo_bdc_hal_rd(pxmc_state_t *mcs)
{
  return (*SIM_PORTF0>>3)&0x7;
}

unsigned char mo_bdc_hal_pos_table[8]=
{
  [0]=0xff,
  [7]=0xff,
  [1]=0,/*0*/
  [5]=1,/*1*/
  [4]=2,/*2*/
  [6]=3,/*3*/
  [2]=4,/*4*/
  [3]=5,/*5*/
};

volatile uint16_t * const pxmc_ctm4pwm3f_des[8]={
  CTM4_PWM5B,
  CTM4_PWM6B,
  CTM4_PWM7B,
  CTM4_PWM8B,
  CTM4_DASM3B,
  CTM4_DASM4B,
  CTM4_DASM9B,
  CTM4_DASM10B
};

/**
 * pxmc_ctm4pwm3f_wr - Output of the 3-phase PWM to the hardware
 * @mcs:	Motion controller state information
 */
/*static*/ inline void
pxmc_ctm4pwm3f_wr(pxmc_state_t *mcs, short pwm1, short pwm2, short pwm3)
{
  unsigned long out_info=mcs->pxms_out_info;

  *pxmc_ctm4pwm3f_des[(out_info>>0)&0x7]=pwm1;
  *pxmc_ctm4pwm3f_des[(out_info>>4)&0x7]=pwm2;
  *pxmc_ctm4pwm3f_des[(out_info>>8)&0x7]=pwm3;
}

int forced_hal_pos;

/**
 * mo_ctm4pwm3f_out - Phase output for brush-less 3-phase motor
 * @mcs:	Motion controller state information
 */
int
mo_ctm4pwm3f_out(pxmc_state_t *mcs)
{
  static unsigned char hal_old=0x40;
  unsigned char hal_pos;
  short pwm1;
  short pwm2;
  short pwm3;
  short indx;
  short ene;

  if(!(mcs->pxms_flg&PXMS_PTI_m) || !(mcs->pxms_flg&PXMS_PHA_m)){
    short ptindx;
    short ptirc=mcs->pxms_ptirc;
    short divisor=mcs->pxms_ptper*6;

    hal_pos=mo_bdc_hal_pos_table[mo_bdc_hal_rd(mcs)];
    /*hal_pos=forced_hal_pos;*/

    ptindx=(hal_pos*ptirc+divisor/2)/divisor;

    if(!(mcs->pxms_flg&PXMS_PTI_m)){
      if(((hal_pos!=hal_old)&&(hal_old!=0x40)) && 0){
        if((ptindx>mcs->pxms_ptindx+ptirc/2) ||
	   (mcs->pxms_ptindx>ptindx+ptirc/2)){
          ptindx=(mcs->pxms_ptindx+ptindx-ptirc)/2;
	  if(ptindx<0)
	    ptindx+=ptirc;
	}else{
            ptindx=(mcs->pxms_ptindx+ptindx)/2;
	}
	mcs->pxms_ptindx=ptindx;

        mcs->pxms_ptofs=(mcs->pxms_ap>>PXMC_SUBDIV(mcs))+mcs->pxms_ptshift-ptindx;

        pxmc_set_flag(mcs,PXMS_PTI_b);
      }else{
        mcs->pxms_ptindx=ptindx;
      }
    }else{

    }
  }

  ene=mcs->pxms_ene;
  if(ene){
    indx=mcs->pxms_ptindx;
    if(ene<0){
      /* Generating direction of stator mag. field for backward torque */
      ene=-ene;
      if((indx-=mcs->pxms_ptvang)<0)
	indx+=mcs->pxms_ptirc;
    }else{
      /* Generating direction of stator mag. field for forward torque */
      if((indx+=mcs->pxms_ptvang)>=mcs->pxms_ptirc)
	indx-=mcs->pxms_ptirc;
    }

    pwm1=mcs->pxms_ptptr1[indx];
    pwm2=mcs->pxms_ptptr2[indx];
    pwm3=mcs->pxms_ptptr3[indx];

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    pwm1=((unsigned long)pwm1*(unsigned int)ene)>>(15+6);
    pwm2=((unsigned long)pwm2*(unsigned int)ene)>>(15+6);
    pwm3=((unsigned long)pwm3*(unsigned int)ene)>>(15+6);

    pxmc_ctm4pwm3f_wr(mcs,pwm1,pwm2,pwm3);
    return 0;
  }else{
    pxmc_ctm4pwm3f_wr(mcs,0,0,0);
    return 0;
  }
}

int
test_hal(pxmc_state_t *mcs, int hal_pos, int ene)
{
  forced_hal_pos=hal_pos;
  mcs->pxms_ene=ene;
  mo_ctm4pwm3f_out(mcs);
  return mo_bdc_hal_pos_table[mo_bdc_hal_rd(mcs)];
}

short irc_idx;

short match_occured;

void tpu_irc_idx_match(void)
{
  match_occured++;
}

int
tpu_irc_idx_inp(struct pxmc_state *mcs)
{
  short irc;
  int idx_chan;

  irc=tpu_fqd_read_count(mcs->pxms_inp_info);
  pxmc_irc_16bit_update(mcs,irc);

  /* Running of the motor commutator */
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  idx_chan=mcs->pxms_inp_info-1;
  if(tpu_test_cisr(idx_chan)){
    irc_idx=*tpu_param_addr(idx_chan,NITC_FINAL_TRANS_TIME);
    tpu_irc_idx_match();

    if(!(mcs->pxms_flg&PXMS_PHA_m)){

    }
    tpu_clear_cisr(idx_chan);
  }

  if(tpu_test_cisr(6)){
    irc_idx=*tpu_param_addr(idx_chan,NITC_FINAL_TRANS_TIME);
    tpu_irc_idx_match();
    tpu_clear_cisr(6);
  }

  return 0;
}

/********************************************************************/
/* controllers configuration */

int tpu_irc_inp(struct pxmc_state *mcs);
int tpu_irc_ap2hw(struct pxmc_state *mcs);

pxmc_state_t mcs0={
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp:tpu_irc_idx_inp,
  pxms_do_con:pxmc_fast_pid,
  pxms_do_out:mo_ctm4pwm3f_out,
  pxms_do_deb:0,
  pxms_do_gen:0,
  pxms_do_ap2hw:tpu_irc_ap2hw,
  pxms_ap:0, pxms_as:0,
  pxms_rp: 55*256, pxms_rs:0, pxms_subdiv:8,
  pxms_md:800<<8, pxms_ms:1000, pxms_ma:1,
  pxms_inp_info:8,
  pxms_out_info:0x00000201,
  pxms_ene:0, pxms_erc:0,
  pxms_p:220, pxms_i:40, pxms_d:450, pxms_s1:0, pxms_s2:0,
  pxms_me:0x4000/*0x7fff*/,
  pxms_cfg:PXMS_CFG_MD2E_m|PXMS_CFG_HLS_m|
	  0x1,

  pxms_ptper:1,
  pxms_ptirc:1000,
};


pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT]=
			{&mcs0};


pxmc_state_list_t  pxmc_main_list={
  pxml_arr:pxmc_main_arr,
  pxml_cnt:0
};


#ifdef PXMC_WITH_GEN_SELECTION

pxmc_call_t *pxmc_get_stop_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spdstop_gi;
}

pxmc_call_t *pxmc_get_cont_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_cont_gi;
}

pxmc_call_t *pxmc_get_spdnext_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spdnext_gi;
}

pxmc_call_t *pxmc_get_trp_gi_4axis(pxmc_state_t *mcs, int mode)
{
 #ifdef PXMC_WITH_FAST_TRPREL
  if((mcs->pxms_cfg&PXMS_CFG_CYCL_m)||(mode==PXMC_GO_REL))
    return pxmc_fast_trprel_gi;
  else
 #endif /*PXMC_WITH_FAST_TRPREL*/
    return pxmc_fast_trp_gi;
}

pxmc_call_t *pxmc_get_trp_retgt_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_trp_retgt;
}

pxmc_call_t *pxmc_get_trp_spdfg_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  return pxmc_fast_trp_spdfg_gi;
 #else /*PXMC_WITH_FINE_GRAINED*/
  return 0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
}

pxmc_call_t *pxmc_get_trprel_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FAST_TRPREL
  return pxmc_fast_trprel_gi;
 #else /*PXMC_WITH_FAST_TRPREL*/
  return 0;
 #endif /*PXMC_WITH_FAST_TRPREL*/
}

pxmc_call_t *pxmc_get_spd_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_spd_gi;
}

pxmc_call_t *pxmc_get_spdfg_gi_4axis(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  return pxmc_fast_spdfg_gi;
 #else /*PXMC_WITH_FINE_GRAINED*/
  return 0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
}

#endif /*PXMC_WITH_GEN_SELECTION*/

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_fast_hh_gi;
}

long pxmc_sfi_sel(long sfi_hz)
{
  return ctm4_sfi_sel(sfi_hz);
}

/* main controller routine called from sampling frequency irq */
void do_pxm_control(void)
{
  pxmc_fast_proc_list(pxmc_main_list.pxml_arr,
  			pxmc_main_list.pxml_cnt);
}

/* initialize pxmc_main_list */
int pxmc_initialize(void)
{
  pxmc_state_t *mcs=&mcs0;

  tpu_fqd_init(mcs->pxms_inp_info, mcs->pxms_inp_info+1, 2);
  tpu_nitc_init(mcs->pxms_inp_info-1, 2, NITC_CHCTRL_FALLING_EDGE, 0,
                mcs->pxms_inp_info*16+FQD_POSITION_COUNT*2, 1, 0);

  tpu_nitc_init(/*TPU6*/6, 2, NITC_CHCTRL_FALLING_EDGE, 1,
                0, 1, 1);

  mo_bdc_hal_init(mcs);

  mcs->pxms_ptvang=pxmc_ptvang_deg2irc(mcs,90);

  pxmc_init_ptable(mcs, PXMC_PTPROF_SIN3FUP);

  pxmc_ctm4pwm3f_wr(mcs, 0, 0, 0);

  tpu_qom_init(MO_CTM4PWM3F_EN1_CH, 1, 1, 0, 0, NULL, -1, 0, 0);
  tpu_qom_init(MO_CTM4PWM3F_EN2_CH, 1, 1, 0, 0, NULL, -1, 0, 0);
  tpu_qom_init(MO_CTM4PWM3F_EN3_CH, 1, 1, 0, 0, NULL, -1, 0, 0);

  pxmc_main_list.pxml_cnt=0;
  pxmc_dbg_hist=NULL;
  __memory_barrier();
  pxmc_main_list.pxml_cnt=PXML_MAIN_CNT;
  return 0;
}

