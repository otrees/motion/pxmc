/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_dq_trans.c - generic multi axis motion controller
             DQ and other PMSM transformations

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>

/* 1 / sqrt(3) * 65536 */
#define PXMC_RECI16_SQRT3  37837

#define pxmc_offsbychar(_ptr, _offs) \
   ((typeof(&(_ptr)[0]))((char*)(_ptr) + (_offs)))


static inline
void pxmc_dq2alpbet(int32_t *p_alp,  int32_t *p_bet,
                    int32_t d,       int32_t q,
                    int32_t sin_val, int32_t cos_val)
{
  *p_alp = d * cos_val - q * sin_val;
  *p_bet = d * sin_val + q * cos_val;
}

static inline
void pxmc_alpbet2dq(int32_t *p_d,    int32_t *p_q,
                    int32_t alp,     int32_t bet,
                    int32_t sin_val, int32_t cos_val)
{
  *p_d =  alp * cos_val + bet * sin_val;
  *p_q = -alp * sin_val + bet * cos_val;
}

static inline
void pxmc_alpbet2pwm3ph(uint32_t *p_pwm1, uint32_t *p_pwm2,
                        uint32_t *p_pwm3,
                        int32_t  pwm_alp, int32_t  pwm_bet)
{
  int32_t pwm_bet_div_2_k3;
  uint32_t pwm1;
  uint32_t pwm2;
  uint32_t pwm3;
  int32_t  alp_m_bet_d2k3;
  uint32_t state23_msk;
  uint32_t pwm23_shift;
  uint32_t bet_sgn = pwm_bet >> 31;
  uint32_t alp_sgn = pwm_alp >> 31;

  pwm_bet_div_2_k3 = PXMC_RECI16_SQRT3 * (pwm_bet >> 16);

  alp_m_bet_d2k3 = (alp_sgn ^ pwm_alp) - (bet_sgn ^ (pwm_bet_div_2_k3 + bet_sgn));
  alp_m_bet_d2k3 = alp_m_bet_d2k3 >> 31;

  state23_msk = alp_sgn & ~alp_m_bet_d2k3;

  /*
   *   bet alp amb s23
   *    0   0   0   0 -> 0 (000)
   *    0   0  -1   0 -> 1 (001)
   *   -1   0  -1   0 -> 1 (001)
   *   -1   0   0  -1 -> 2 (010)
   *   -1  -1   0  -1 -> 3 (011)
   *   -1  -1  -1   0 -> 4 (100)
   *    0  -1  -1   0 -> 4 (100)
   *    0  -1   0   0 -> 5 (101)
   */

  pwm1 = pwm_alp & state23_msk;
  pwm2 = pwm_bet_div_2_k3 - pwm1;
  pwm3 = -pwm_bet_div_2_k3 - pwm1;
  pwm2 &= (~bet_sgn | state23_msk);
  pwm3 &= (bet_sgn | state23_msk);
  pwm1 = pwm_alp + pwm2 + pwm3;
  pwm1 &= ~state23_msk;
  pwm1 >>= 16;
  pwm23_shift = 15 - state23_msk;
  pwm2 >>= pwm23_shift;
  pwm3 >>= pwm23_shift;

  *p_pwm1 = pwm1;
  *p_pwm2 = pwm2;
  *p_pwm3 = pwm3;
}

static inline
void pxmc_cur3ph2alpbet(int32_t *p_cur_alp, int32_t *p_cur_bet, int32_t cur1,
                        int32_t cur2, int32_t cur3, unsigned int phs)
{
  int32_t cur_alp;
  int32_t cur_bet;

  if ((phs == 5) || (phs == 0))
    cur_alp = -(cur2 + cur3);
  else
    cur_alp = cur1;

  if ((phs == 5) || (phs == 0))
    cur_bet = cur2 - cur3;
  else if ((phs == 3) || (phs == 4))
    cur_bet = 2 * cur2 + cur1;
  else /* 1 2 */
    cur_bet = -(2 * cur3 + cur1);


  cur_bet *= PXMC_RECI16_SQRT3;
  cur_bet >>= 16;

  *p_cur_alp = cur_alp;
  *p_cur_bet = cur_bet;
}

static inline
void pxmc_cur3phls2alpbet(int32_t *p_cur_alp, int32_t *p_cur_bet, int32_t curadc[3],
                           uint32_t pwm_prew[3], uint32_t pwm_cycle)
{
  uint32_t pwm1 = pwm_prew[0];
  uint32_t pwm2 = pwm_prew[1];
  uint32_t pwm3 = pwm_prew[2];

  int32_t  cur_alp;
  int32_t  cur_bet;

  int32_t  cur1;
  int32_t  cur2;
  int32_t  cur3;

  int32_t  *pcurmult;
  uint32_t curmult_idx;
  uint32_t pwm_reci;

  /*
   *   u1>u2 u2>u3 u1>u3               cm
   *     0     1     1 ->  1 (1, 0, 2) 0
   *     0     1     0 ->  2 (1, 2, 0) 2
   *     0     0     0 ->  3 (2, 1, 0) 1
   *     1     0     0 ->  4 (2, 0, 1) 0
   *     1     0     1 ->  5 (0, 2, 1) 2
   *     1     1     1 ->  0 (0, 1, 2) 1
   */

  uint32_t u1gtu2 = (int32_t)(pwm2 - pwm1) >> 31;
  uint32_t u1gtu3 = (int32_t)(pwm3 - pwm1) >> 31;
  uint32_t u2gtu3 = (int32_t)(pwm3 - pwm2) >> 31;
  uint32_t state50_msk = u1gtu2 & u1gtu3;

 #if 0
  uint32_t sz4idx = sizeof(*pwm_prew);
  uint32_t sz4cur = sizeof(*curadc);
  uint32_t curmult_idx2curadc;

  /* Variant where curmult_idx is directly computed as byte offset to uint32_t array */
  curmult_idx = (((u1gtu3 ^ u1gtu2) | (1 * sz4idx)) ^ u2gtu3 ^ u1gtu2) & (3 * sz4idx);
  pwm_reci = pwm_cycle - *pxmc_offsbychar(pwm_prew, curmult_idx);
 #else
  curmult_idx = (((u1gtu3 ^ u1gtu2) | 1) ^ u2gtu3 ^ u1gtu2) & 3;
  pwm_reci = pwm_cycle - pwm_prew[curmult_idx];
 #endif

  pwm_reci = ((pwm_cycle << 16) + pwm_reci / 2) / pwm_reci;

 #if 0
  curmult_idx2curadc = ((2 * sz4cur) << (0 * sz4idx)) |
                       ((0 * sz4cur) << (1 * sz4idx)) |
                       ((1 * sz4cur) << (2 * sz4idx));
  curmult_idx = (curmult_idx2curadc >> curmult_idx) & (sz4cur | (sz4cur << 1));
  pcurmult = curadc;
  pcurmult = pxmc_offsbychar(pcurmult, curmult_idx);
 #else
  pcurmult = &curadc[curmult_idx];
 #endif
  *pcurmult = (int32_t)(pwm_reci * (*pcurmult)) >> 16;

  cur1 = curadc[0];
  cur2 = curadc[1];
  cur3 = curadc[2];

  cur_alp = -(cur2 + cur3);                /* 5 0 */
  cur_alp &= state50_msk;                  /* 1 2 3 4 */
  cur_alp |= cur1 & ~state50_msk;

  cur_bet = (-2 * cur3 - cur1) & u2gtu3;   /* 1 2 */
  cur_bet |= (2 * cur2 + cur1) & ~u2gtu3;  /* 3 4 */
  cur_bet &= ~state50_msk;
  cur_bet |= (cur2 - cur3) & state50_msk;  /* 5 0 */

  cur_bet *= PXMC_RECI16_SQRT3;
  cur_bet >>= 16;

  *p_cur_alp = cur_alp;
  *p_cur_bet = cur_bet;
}

static inline
void pxmc_alpbet2pwm2ph(uint32_t *p_pwm1, uint32_t *p_pwm2,
                        uint32_t *p_pwm3, uint32_t *p_pwm4,
                        int32_t  pwm_alp, int32_t  pwm_bet)
{
  uint32_t alp_sgn = pwm_alp >> 31;
  uint32_t bet_sgn = pwm_bet >> 31;
  pwm_alp >>= 16;
  pwm_bet >>= 16;
  *p_pwm1 = -pwm_alp & alp_sgn;
  *p_pwm2 = pwm_alp & ~alp_sgn;
  *p_pwm3 = -pwm_bet & bet_sgn;
  *p_pwm4 = pwm_bet & ~bet_sgn;
}

static inline
void pxmc_cur2ph2alpbet(int32_t *p_cur_alp, int32_t *p_cur_bet,
                        int32_t curadc[4], uint32_t pwm_prew[4])
{
  int32_t bet_pwm = pwm_prew[2];
  uint32_t bet_sgn = (bet_pwm - 1) >> 31;
  int32_t alp_pwm = pwm_prew[0];
  uint32_t alp_sgn = (alp_pwm - 1) >> 31;
  *p_cur_bet = (curadc[3] & ~bet_sgn) -
               (curadc[2] & bet_sgn);
  *p_cur_alp = (curadc[1] & ~alp_sgn) -
               (curadc[0] & alp_sgn);
}
