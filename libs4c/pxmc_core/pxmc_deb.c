/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_deb.c - generic multi axis motion controller
               debugging support

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <malloc.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"

pxmc_dbg_hist_t *pxmc_dbg_hist=NULL;

/**
 * pxmc_dbg_histfree - Frees motion history buffer
 * @hist:	Motion history  buffer
 */
int pxmc_dbg_histfree(pxmc_dbg_hist_t *hist)
{
  if(hist==NULL){
    hist=pxmc_dbg_hist;
    pxmc_dbg_hist=NULL;
  }
  if(hist==NULL) return 0;
  if(hist->buff!=NULL) free(hist->buff);
  free(hist);
  return 0;
}

/**
 * pxmc_dbg_histalloc - Allocates new motion history buffer
 * @count:	Number of allocated slots in motion history  buffer
 */
pxmc_dbg_hist_t *pxmc_dbg_histalloc(int count)
{
  pxmc_dbg_hist_t *hist;
  hist=malloc(sizeof(pxmc_dbg_hist_t));
  if(hist==NULL) return NULL;
  if((hist->buff=malloc(sizeof(hist->buff[0])*count))==NULL)
  {
    free(hist);
    return 0;
  }
  memset(hist->buff,0,sizeof(hist->buff[0])*count);
  hist->end=hist->buff+count;
  hist->ptr=NULL;
  return hist;
}

/**
 * pxmc_dbg_ene_as - Stores actual speed and output energy
 * @mcs:	Motion controller state information
 */
int pxmc_dbg_ene_as(pxmc_state_t *mcs)
{
  long *ptr;
  if(pxmc_dbg_hist){
    ptr=pxmc_dbg_hist->ptr;
    if(ptr&&(ptr<pxmc_dbg_hist->end-1)){
      *(ptr++)=mcs->pxms_as>>PXMC_SUBDIV(mcs);
      *(ptr++)=mcs->pxms_ene;
      pxmc_dbg_hist->ptr=ptr;
    }
  }
  return 0;
}

int pxmc_dbg_gnr_gi(pxmc_state_t *mcs)
{
  long *ptr;
  if((mcs->pxms_flg&PXMS_DBG_m)&&!(mcs->pxms_flg&PXMS_ERR_m)
     &&pxmc_dbg_hist){
    ptr=pxmc_dbg_hist->ptr;
    if(!ptr){
      mcs->pxms_rs=0;
      return 0;
    }
    if(ptr<pxmc_dbg_hist->end-1){
      mcs->pxms_rp+=mcs->pxms_rs=ptr[1]<<PXMC_SUBDIV(mcs);
      return 0;
    }
  }
  mcs->pxms_rp+=mcs->pxms_rs;
  mcs->pxms_do_gen=pxmc_get_stop_gi_4axis(mcs);
  return 0;
}

/**
 * pxmc_dbg_gnr - Generator of speed profile stored in history buffer
 * @mcs:	Motion controller state information
 */
int pxmc_dbg_gnr(pxmc_state_t *mcs)
{
  if(!pxmc_dbg_hist||!(mcs->pxms_flg&PXMS_DBG_m)) return -1;
  if(pxmc_set_gen_prep(mcs)<0) return -1;
  pxmc_set_gen_smth(mcs,pxmc_dbg_gnr_gi,PXMS_BSY_m);
  return 0;
}

/**
 * pxmc_dbgset - Selects debugging options for axis
 * @mcs:	Motion controller state information
 * @do_deb:	Debugging callback function
 * @dbgflg:	0 .. disables debugging, 1 .. enables debugging
 *		for axis
 */
int pxmc_dbgset(pxmc_state_t *mcs, pxmc_call_t *do_deb, int dbgflg)
{
  if(dbgflg>0){
    __memory_barrier();
    if(!do_deb) {
      if(!mcs->pxms_do_deb)
        mcs->pxms_do_deb=pxmc_dbg_ene_as;
    } else mcs->pxms_do_deb=do_deb;
    __memory_barrier();
    pxmc_set_flag(mcs,PXMS_DBG_b);
  }else{
    pxmc_clear_flag(mcs,PXMS_DBG_b);
  }
  return 0;
}


