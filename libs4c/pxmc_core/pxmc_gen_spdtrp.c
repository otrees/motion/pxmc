/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_gen_spdtrp.c - generic multi axis motion controller
                   generator for trapezoid and simple speed movement

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <limits.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"

/********************************************************************/
/* Architecture optimized functions */

#ifdef PXMC_WITH_FINE_GRAINED

#if  defined(__H8300__) || defined(H8300)
/**
 * pxmc_add_cspdfg - Adds fine-grained speed to requested position
 * @mcs:	Motion controller state information
 *
 * Adds fine-grained speed (@pxms_rs,@pxms_rsfg) to requested position
 * (@pxms_rp,@pxms_rpfg). Written is asm.
 */
static inline void
pxmc_add_cspdfg(pxmc_state_t *mcs)
{
  asm (
    "	add.w	%3,%1\n"
    "	bcc	1f\n"
    "	adds	#1,%0\n"
    "1:	add.l	%2,%0\n"
   : "=r" (mcs->pxms_rp),"=r" (mcs->pxms_rpfg)
   : "r" (mcs->pxms_rs),"r" (mcs->pxms_rsfg),
     "0" (mcs->pxms_rp),"1" (mcs->pxms_rpfg)
   : "cc"
  );
}

#elif defined(__MSP430) || defined(MSP430)

static inline void
pxmc_add_cspdfg(pxmc_state_t *mcs)
{
  asm (
    "	add.w	%A3,%A1  /*pxmc_add_cspdfg*/\n"
   #if PXMC_SUBDIVFG(0)>16
    "	addc.w	%B3,%B1\n"
   #endif
    "	addc.w	%A2,%A0\n"
    "	addc.w	%B2,%B0\n"
   : "=g" (mcs->pxms_rp),"=g" (mcs->pxms_rpfg)
   : "g" (mcs->pxms_rs),"g" (mcs->pxms_rsfg),
     "0" (mcs->pxms_rp),"1" (mcs->pxms_rpfg)
   : "cc"
  );
}

#elif defined(__arm__) || defined(__thumb2__)

#ifndef __STRINGIFY
#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#endif
#ifndef STRINGIFY
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif

static inline void
pxmc_add_cspdfg(pxmc_state_t *mcs)
{
  asm (
    "		/*pxmc_add_cspdfg*/\n"
    "	adds	%1,%1,%3\n"
   #if PXMC_SUBDIVFG(0)<32
    "	lsls	%1,%1, #32-" STRINGIFY(PXMC_SUBDIVFG(0)) "\n"
   #endif
    "	adcs	%0,%0,%2\n"
   #if PXMC_SUBDIVFG(0)<32
    "	lsrs	%1,%1, #32-" STRINGIFY(PXMC_SUBDIVFG(0)) "\n"
   #endif
   : "=r" (mcs->pxms_rp),"=r" (mcs->pxms_rpfg)
   : "r" (mcs->pxms_rs),"r" (mcs->pxms_rsfg),
     "0" (mcs->pxms_rp),"1" (mcs->pxms_rpfg)
   : "cc"
  );
}

#elif defined (__unix) || defined(__rtems__) || defined(__riscv)

void
pxmc_add_cspdfg(pxmc_state_t *mcs)
{
  int max_bits = sizeof(unsigned long) * CHAR_BIT;
  unsigned long mask = ~0L;
  unsigned long rpfg;
  unsigned long rpfg_new;

  if (PXMC_SUBDIVFG(mcs) < max_bits)
    mask = (1 << PXMC_SUBDIVFG(mcs)) - 1;

  rpfg = mcs->pxms_rpfg & mask;
  mcs->pxms_rpfg = rpfg_new = (rpfg + mcs->pxms_rsfg) & mask;
  mcs->pxms_rp += mcs->pxms_rs + (rpfg_new < rpfg? 1: 0);
}

#else
#error pxmc_add_cspdfg not defined for this architecture
#endif

#else /*PXMC_WITH_FINE_GRAINED*/

static inline void
pxmc_add_cspdfg(pxmc_state_t *mcs)
{
   mcs->pxms_rp+=mcs->pxms_rs;
}

#endif /*PXMC_WITH_FINE_GRAINED*/

/********************************************************************/
/* Speed related functions */

/**
 * pxmc_add_vspd - Adds variable speed to requested position
 * @mcs:	Motion controller state information
 * @spd:	New requested speed
 *
 * Sets requested speed (@pxms_rs) and adds it to requested
 * position (@pxms_rp).
 */
static inline void
pxmc_add_vspd(pxmc_state_t *mcs, long spd)
{
  mcs->pxms_rp+=mcs->pxms_rs=spd;
}

/**
 * pxmc_set_spd - Sets new immediate value of speed
 * @mcs:	Motion controller state information
 * @spd:	New requested speed
 *
 * Sets requested speed (@pxms_rs) and clears (@pxms_rsfg)
 */
static inline void
pxmc_set_spd(pxmc_state_t *mcs, long spd)
{
  mcs->pxms_rs=spd;
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
}

#ifdef PXMC_WITH_FINE_GRAINED
/**
 * pxmc_set_spdfg - Sets new immediate value of fine-grained speed
 * @mcs:	Motion controller state information
 * @spd:	New requested fine-grained speed
 *
 * Sets requested speed (@pxms_rs) and clears (@pxms_rsfg)
 */
static inline void
pxmc_set_spdfg(pxmc_state_t *mcs, long spd)
{
  mcs->pxms_rsfg=spd;
 #ifdef PXMC_SPDFG_ONLY
  mcs->pxms_rs=spd<0? -1: 0;
 #else
  mcs->pxms_rs=spd>>PXMC_SUBDIVFG(mcs);
 #endif
}

/**
 * pxmc_spdfg_gnr - Constant speed fine-grained generator
 * @mcs:	Motion controller state information
 */
int
pxmc_spdfg_gnr(pxmc_state_t *mcs)
{
  pxmc_add_cspdfg(mcs);
  return 0;
}
#endif /*PXMC_WITH_FINE_GRAINED*/

/********************************************************************/
/* generators */

/**
 * pxmc_spd_gacc - Smooth transition to requested speed
 * @spd:	Pointer to controlled speed
 * @rspd:	Value of requested final speed
 * @acc:	Allowed acceleration
 *
 * This function modifies @spd by @acc increments until
 * required value @rspd is reached. It returns 0 when
 * speed is reached, else returns 1.
 */
int
pxmc_spd_gacc(long *spd,long rspd,long acc)
{
  if(rspd==*spd) return 0;
  if(rspd>*spd){
    long nspd=*spd;
    /*sat_add_slsl(nspd,acc);*/
    nspd+=acc;
    *spd=(rspd>nspd)?nspd:rspd;
    return 1;
  }else{
    long nspd=*spd;
    /*sat_sub_slsl(nspd,acc);*/
    nspd-=acc;
    *spd=(rspd<nspd)?nspd:rspd;
    return 1;
  }
}

/**
 * pxmc_nop_gd - No-operation generator state
 * @mcs:	Motion controller state information
 */
int
pxmc_nop_gd(pxmc_state_t *mcs)
{
  return 0;
}

/**
 * pxmc_cont_gi - Initializes continuation generator
 * @mcs:	Motion controller state information
 *
 * This type of generator is temporarily used
 * when new generator parameters are computed.
 */
int
pxmc_cont_gi(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m){
    pxmc_clear_flag(mcs,PXMS_ENG_b);
    pxmc_clear_flag(mcs,PXMS_BSY_b);
    return 0;
  }
  pxmc_add_cspdfg(mcs);
  return 0;
}

/*------------------------------------------------------------------*/

static int pxmc_trp_gdu10(pxmc_state_t *mcs);
static int pxmc_trp_gdu20(pxmc_state_t *mcs);
static int pxmc_trp_gdu30(pxmc_state_t *mcs);
static int pxmc_trp_gdd10(pxmc_state_t *mcs);
static int pxmc_trp_gdd20(pxmc_state_t *mcs);
static int pxmc_trp_gdd30(pxmc_state_t *mcs);
int pxmc_trp_gend1(pxmc_state_t *mcs);
int pxmc_trp_gend(pxmc_state_t *mcs);

/**
 * pxmc_trp_gi - Initializes trapezoid generator
 * @mcs:	Motion controller state information
 *
 * This complex generator realizes motion to requested
 * end position @pxms_ep with trapezoid speed profile
 * constrained by @pxms_acc and @pxms_ms parameters.
 */
int
pxmc_trp_gi(pxmc_state_t *mcs)
{
  long rp, rs, ep;
  pxmc_set_flag(mcs,PXMS_BSY_b);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  ep=mcs->pxms_ep;
  mcs->pxms_gen_tep=ep;
  if(ep==rp)
    return pxmc_trp_gend1(mcs);
  if(ep>rp){
    ep=ep-rp;
    mcs->pxms_gen_ttpfr=ep&1;
    mcs->pxms_gen_ttp=rp+(ep>>1);
    mcs->pxms_gen_tac=mcs->pxms_ma;
    mcs->pxms_gen_tsp=mcs->pxms_ms;
    mcs->pxms_gen_tspfg=0;
    mcs->pxms_do_gen=pxmc_trp_gdu10;
  }else{
    ep=rp-ep;
    mcs->pxms_gen_ttpfr=ep&1;
    mcs->pxms_gen_ttp=rp-(ep>>1);
    mcs->pxms_gen_tac=mcs->pxms_ma;
    mcs->pxms_gen_tsp=-mcs->pxms_ms;
    mcs->pxms_gen_tspfg=0;
    mcs->pxms_do_gen=pxmc_trp_gdd10;
  }
  mcs->pxms_rs=0;
  return 0;
}

#ifdef PXMC_WITH_FINE_GRAINED
/**
 * pxmc_trp_spdfg_gi - Initializes fine-grained trapezoid generator
 * @mcs:	Motion controller state information
 *
 * This complex generator realizes motion to requested
 * end position @pxms_ep with trapezoid speed profile
 * constrained by @pxms_acc and @pxms_gen_tspfg parameters.
 */
int
pxmc_trp_spdfg_gi(pxmc_state_t *mcs)
{
  long rp, rs, ep;
  pxmc_set_flag(mcs,PXMS_BSY_b);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  ep=mcs->pxms_ep;
  mcs->pxms_gen_tep=ep;
  if(ep==rp)
    return pxmc_trp_gend1(mcs);
  if(ep>rp){
    ep=ep-rp;
    mcs->pxms_gen_ttpfr=ep&1;
    mcs->pxms_gen_ttp=rp+(ep>>1);
    mcs->pxms_gen_tac=mcs->pxms_ma;
    if(mcs->pxms_gen_tspfg<0)
      mcs->pxms_gen_tspfg=-mcs->pxms_gen_tspfg;
   #ifdef PXMC_SPDFG_ONLY
    mcs->pxms_gen_tsp=0;
   #else
    mcs->pxms_gen_tsp=mcs->pxms_gen_tspfg>>PXMC_SUBDIVFG(&mcs);
   #endif
    mcs->pxms_do_gen=pxmc_trp_gdu10;
  }else{
    ep=rp-ep;
    mcs->pxms_gen_ttpfr=ep&1;
    mcs->pxms_gen_ttp=rp-(ep>>1);
    mcs->pxms_gen_tac=mcs->pxms_ma;
    if(mcs->pxms_gen_tspfg>0)
      mcs->pxms_gen_tspfg=-mcs->pxms_gen_tspfg;
   #ifdef PXMC_SPDFG_ONLY
    mcs->pxms_gen_tsp=-1;
   #else
    mcs->pxms_gen_tsp=mcs->pxms_gen_tspfg>>PXMC_SUBDIVFG(&mcs);
   #endif
    mcs->pxms_do_gen=pxmc_trp_gdd10;
  }
  mcs->pxms_rs=0;
  return 0;
}
#endif /*PXMC_WITH_FINE_GRAINED*/

int
pxmc_trp_gend(pxmc_state_t *mcs)
{
  mcs->pxms_rp=mcs->pxms_gen_tep;
  return pxmc_trp_gend1(mcs);
}

int
pxmc_trp_gend1(pxmc_state_t *mcs)
{
  mcs->pxms_rs=0;
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  pxmc_clear_flag(mcs,PXMS_ENG_b);
  pxmc_clear_flag(mcs,PXMS_BSY_b);
  mcs->pxms_do_gen=pxmc_nop_gd;
  return 0;
}

/*------------------------------*/

/* accelerated upward motion */
int
pxmc_trp_gdu10(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  /*sat_add_slsl(rs,mcs->pxms_gen_tac);*/
  rs+=mcs->pxms_gen_tac;
  if(rs<mcs->pxms_gen_tsp){
    sat_add_slsl(rp,rs);
    if(rp>=mcs->pxms_gen_ttp){
      /* one half of motion reached */
      rp=mcs->pxms_rp;
      /* rp=2*ttp+ttpfr-rp */
      rp=((mcs->pxms_gen_ttp<<1)|(mcs->pxms_gen_ttpfr&1))-rp;
      mcs->pxms_do_gen=pxmc_trp_gdu30;
    }
  }else{
    /* naximal speed reached */
    /* ttp=2*ttp+ttpfr-rp */
    mcs->pxms_gen_ttp=((mcs->pxms_gen_ttp<<1)|(mcs->pxms_gen_ttpfr&1))-rp;
    rs=mcs->pxms_gen_tsp;
   #ifdef PXMC_WITH_FINE_GRAINED
    mcs->pxms_rsfg=mcs->pxms_gen_tspfg;
   #endif /*PXMC_WITH_FINE_GRAINED*/
    rp+=rs;
    mcs->pxms_do_gen=pxmc_trp_gdu20;
  }
  mcs->pxms_rp=rp;
  mcs->pxms_rs=rs;
  return 0;
}

/* upward motion with constant speed */
int
pxmc_trp_gdu20(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  pxmc_add_cspdfg(mcs);
  rp=mcs->pxms_rp;
  if(rp<mcs->pxms_gen_ttp)
    return 0;
  rs=mcs->pxms_gen_tsp-mcs->pxms_gen_tac;
  if(rs<=0)
    return pxmc_trp_gend(mcs);
  rp=mcs->pxms_gen_ttp+rs;
  mcs->pxms_rs=rs;
  mcs->pxms_rp=rp;
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_do_gen=pxmc_trp_gdu30;
  return 0;
}

/* upward motion with deaccelerated speed */
int
pxmc_trp_gdu30(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  rs-=mcs->pxms_gen_tac;
  if(rs<=0)
    return pxmc_trp_gend(mcs);
  rp+=rs;
  mcs->pxms_rs=rs;
  mcs->pxms_rp=rp;
  return 0;
}

/*------------------------------*/

/* accelerated downward motion */
int
pxmc_trp_gdd10(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  /*sat_sub_slsl(rs,mcs->pxms_gen_tac);*/
  rs-=mcs->pxms_gen_tac;
  if(rs>mcs->pxms_gen_tsp){
    sat_add_slsl(rp,rs);
    if(rp<=mcs->pxms_gen_ttp){
      /* one half of motion reached */
      rp=mcs->pxms_rp;
      /* rp=2*ttp-ttpfr-rp */
      rp=((mcs->pxms_gen_ttp<<1)-(mcs->pxms_gen_ttpfr&1))-rp;
      mcs->pxms_do_gen=pxmc_trp_gdd30;
    }
  }else{
    /* naximal speed reached */
    /* ttp=2*ttp-ttpfr-rp */
    mcs->pxms_gen_ttp=((mcs->pxms_gen_ttp<<1)-(mcs->pxms_gen_ttpfr&1))-rp;
    rs=mcs->pxms_gen_tsp;
   #ifdef PXMC_WITH_FINE_GRAINED
    mcs->pxms_rsfg=mcs->pxms_gen_tspfg;
   #endif /*PXMC_WITH_FINE_GRAINED*/
    rp+=rs;
    mcs->pxms_do_gen=pxmc_trp_gdd20;
  }
  mcs->pxms_rp=rp;
  mcs->pxms_rs=rs;
  return 0;
}

/* downward motion with constant speed */
int
pxmc_trp_gdd20(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  pxmc_add_cspdfg(mcs);
  rp=mcs->pxms_rp;
  if(rp>mcs->pxms_gen_ttp)
    return 0;
  rs=mcs->pxms_gen_tsp+mcs->pxms_gen_tac;
  if(rs>=0)
    return pxmc_trp_gend(mcs);
  rp=mcs->pxms_gen_ttp+rs;
  mcs->pxms_rs=rs;
  mcs->pxms_rp=rp;
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_do_gen=pxmc_trp_gdd30;
  return 0;
}

/* downward motion with deaccelerated speed */
int
pxmc_trp_gdd30(pxmc_state_t *mcs)
{
  long rp, rs;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);
  rp=mcs->pxms_rp;
  rs=mcs->pxms_rs;
  rs+=mcs->pxms_gen_tac;
  if(rs>=0)
    return pxmc_trp_gend(mcs);
  rp+=rs;
  mcs->pxms_rs=rs;
  mcs->pxms_rp=rp;
  return 0;
}

/*------------------------------------------------------------------*/

int pxmc_spd_gd10(pxmc_state_t *mcs);
int pxmc_spd_gd20(pxmc_state_t *mcs);
int pxmc_stop_gi(pxmc_state_t *mcs);
int pxmc_spdnext_gd(pxmc_state_t *mcs);
int pxmc_spdnext_gend(pxmc_state_t *mcs);

/**
 * pxmc_spd_gi - Initializes constant speed generator
 * @mcs:	Motion controller state information
 *
 * Requested speed is smoothly changed to @pxms_gen_spd_sp
 * with acceleration defined by @pxms_gen_spd_ac.
 * New generator @pxms_gen_spd_next can be selected after
 * required speed is reached.
 */
int
pxmc_spd_gi(pxmc_state_t *mcs)
{
  pxmc_set_flag(mcs,PXMS_BSY_b);
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_do_gen=pxmc_spd_gd10;
  return pxmc_spd_gd10(mcs);
}

int
pxmc_spd_gd10(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  if(mcs->pxms_gen_spd_timeout){
    if(!--mcs->pxms_gen_spd_timeout)
      return pxmc_stop_gi(mcs);
  }

  if(!pxmc_spd_gacc(&(mcs->pxms_rs),
	mcs->pxms_gen_spd_sp,mcs->pxms_gen_spd_ac)){
   #ifdef PXMC_WITH_FINE_GRAINED
    mcs->pxms_rsfg=mcs->pxms_gen_spd_spfg;
   #endif /*PXMC_WITH_FINE_GRAINED*/
    mcs->pxms_do_gen=pxmc_spd_gd20;
  };
  mcs->pxms_rp+=mcs->pxms_rs;
  return 0;
}

int
pxmc_spd_gd20(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_trp_gend1(mcs);

  if(mcs->pxms_gen_spd_timeout){
    if(!--mcs->pxms_gen_spd_timeout)
      return pxmc_stop_gi(mcs);
  }

  pxmc_add_cspdfg(mcs);
  return 0;
}

/**
 * pxmc_spdnext_gi - Initializes transition to zero and then generator change
 * @mcs:	Motion controller state information
 */
int
pxmc_spdnext_gi(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_do_gen=pxmc_spdnext_gd;
  return pxmc_spdnext_gd(mcs);
}

/**
 * pxmc_stop_gi - Initializes transition to zero speed and then stop
 * @mcs:	Motion controller state information
 */
int
pxmc_stop_gi(pxmc_state_t *mcs)
{
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_rsfg=0;
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_gen_spd_next=0;
  mcs->pxms_do_gen=pxmc_spdnext_gd;
  return pxmc_spdnext_gd(mcs);
}


int
pxmc_spdnext_gd(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  if(pxmc_spd_gacc(&(mcs->pxms_rs),0,mcs->pxms_ma)){
    mcs->pxms_rp+=mcs->pxms_rs;
    return 0;
  }

  if(!mcs->pxms_gen_spd_next)
    return pxmc_spdnext_gend(mcs);

  mcs->pxms_do_gen=(pxmc_call_t*)mcs->pxms_gen_spd_next;
  return 0;
}

int
pxmc_spdnext_gend(pxmc_state_t *mcs)
{
  pxmc_set_spd(mcs,0);
  pxmc_clear_flag(mcs,PXMS_ENG_b);
  pxmc_clear_flag(mcs,PXMS_BSY_b);
  mcs->pxms_do_gen=pxmc_nop_gd;
  return 0;
}
