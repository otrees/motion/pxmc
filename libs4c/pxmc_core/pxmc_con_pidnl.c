/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_con_pidnl.c - generic multi axis motion controller
                   PID controller with nonlinearity to faster
                   decrease integration component

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <limits.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"

typedef int_fast32_t pxmc_pidnl_int_t;
typedef int_fast16_t pxmc_pidnl_short_t;

#define PXMC_PIDNL_CON_FOI_SHR 12
#define PXMC_PIDNL_CON_FOD_SHR 10

/**
 * pxmc_pidnl_con - Position PID Controller
 * @mcs:	Motion controller state information
 */
int
pxmc_pidnl_con(pxmc_state_t *mcs)
{
  pxmc_pidnl_int_t dp, ds, pd, i, foi, fod;
  pxmc_pidnl_int_t ene;
  dp=mcs->pxms_rp-mcs->pxms_ap;
  ds=mcs->pxms_rs-mcs->pxms_as;
  if((dp>mcs->pxms_md)||(dp<-mcs->pxms_md)) {
    if(mcs->pxms_cfg&PXMS_CFG_MD2E_m) {
      pxmc_set_errno(mcs,PXMS_E_MAXPD);
      mcs->pxms_ene=0;
      return 0;
    }
    if(dp>0) dp=mcs->pxms_md;
      else dp=-mcs->pxms_md;
  }
  dp>>=PXMC_SUBDIV(mcs);
  ds>>=PXMC_SUBDIV(mcs);

  pd=(pxmc_pidnl_short_t)dp*(pxmc_pidnl_int_t)mcs->pxms_p+
     (pxmc_pidnl_short_t)ds*(pxmc_pidnl_int_t)mcs->pxms_d;

  fod=mcs->pxms_fod;
  fod=fod*mcs->pxms_s1+(ds<<(PXMC_PIDNL_CON_FOD_SHR+2));
  fod=(fod+(1<<(PXMC_PIDNL_CON_FOD_SHR-1)))>>PXMC_PIDNL_CON_FOD_SHR;
  mcs->pxms_fod=fod;
  ene=(fod*mcs->pxms_s2)>>8;

  foi=mcs->pxms_foi;
  if(pd>0) {
    if(pd>mcs->pxms_me) pd=mcs->pxms_me;
    i=(pxmc_pidnl_short_t)pd*(pxmc_pidnl_int_t)mcs->pxms_i;
    foi+=i;
    if(foi<0) {
      foi+=i;
      if(foi>0)
        foi=0;
    }
    i=(foi+(1L<<(PXMC_PIDNL_CON_FOI_SHR-1)))>>PXMC_PIDNL_CON_FOI_SHR;
    ene+=pd+i;
    if(ene>mcs->pxms_me) {
      foi-=(ene-mcs->pxms_me)<<PXMC_PIDNL_CON_FOI_SHR;
      ene=mcs->pxms_me;
      if(!mcs->pxms_as) {
        if((mcs->pxms_erc+=0x20)<0) {
          pxmc_set_errno(mcs,PXMS_E_OVERL);
          ene=0;
        }
      } else
        if(mcs->pxms_erc)
          mcs->pxms_erc-=1;
    } else {
      mcs->pxms_erc = 0;
    }
  } else if(pd<0) {
    if(pd<-mcs->pxms_me) pd=-mcs->pxms_me;
    i=(pxmc_pidnl_short_t)pd*(pxmc_pidnl_int_t)mcs->pxms_i;
    foi+=i;
    if(foi>0) {
      foi+=i;
      if(foi<0)
        foi=0;
    }
    i=(foi+(1L<<(PXMC_PIDNL_CON_FOI_SHR-1)))>>PXMC_PIDNL_CON_FOI_SHR;
    ene+=pd+i;
    if(ene<-mcs->pxms_me) {
      foi-=(ene+mcs->pxms_me)<<PXMC_PIDNL_CON_FOI_SHR;
      ene=-mcs->pxms_me;
      if(!mcs->pxms_as) {
        if((mcs->pxms_erc+=0x20)<0) {
          pxmc_set_errno(mcs,PXMS_E_OVERL);
          ene=0;
        }
      } else
        if(mcs->pxms_erc)
          mcs->pxms_erc-=1;
    } else {
      mcs->pxms_erc = 0;
    }
  } else {
    if(foi > 0)
      foi--;
    else if(foi < 0)
      foi++;
    ene+=(foi+(1L<<(PXMC_PIDNL_CON_FOI_SHR-1)))>>PXMC_PIDNL_CON_FOI_SHR;
    mcs->pxms_erc = 0;
  }
  mcs->pxms_foi=foi;

  mcs->pxms_ene=ene;

  return 0;
}
