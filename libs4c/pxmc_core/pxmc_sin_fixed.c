/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_sin_fixed.c - generic multi axis motion controller
             fixed sine approximation

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <pxmc_sin_fixed.h>

/**
 * pxmc_sin_fixed - fixed math sine computation
 * @x:	The argument value - 2^32 corresponds to 2 * PI or 360 deg
 *
 * Returns sine of value @x argument where unit is 2^16, i.e.
 * return value 0x10000 is equivalent to +1.0 and -0x10000 to -1.0
 */
int32_t pxmc_sin_fixed(uint32_t x)
{
  return pxmc_sin_fixed_inline(x, 16);
}

/**
 * pxmc_sin_fixed_zic - indicate range for +/-10 degree zero current correction
 * @x:	The argument value - 2^32 corresponds to 2 * PI or 360 deg
 *
 * Returns non zero value if input is in the 0 or PI +/- 10 deg range.
 */
int pxmc_sin_fixed_zic(uint32_t x)
{
  return pxmc_sin_fixed_zic_inline(x);
}

/**
 * pxmc_sincos_fixed - fixed math sine computation
 * @pysin: pointer to location where sine value is returned
 * @pycos: pointer to location where cosine value is returned
 * @x:	The argument value - 2^32 corresponds to 2 * PI or 360 deg
 *
 * Returns sine and cosine of value @x argument where unit is 2^16, i.e.
 * return value 0x10000 is equivalent to +1.0 and -0x10000 to -1.0
 */
void pxmc_sincos_fixed(int32_t *pysin, int32_t *pycos, uint32_t x)
{
  return pxmc_sincos_fixed_inline(pysin, pycos, x, 16);
}
