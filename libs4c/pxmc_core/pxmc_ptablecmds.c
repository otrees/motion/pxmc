/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_ptablecmds.c - generic multi axis motion controller
             phase table commands for command processor

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>
#include "pxmc.h"
#include "pxmc_cmds.h"

/**
 * cmd_do_regptmod_short_val - read or write axis 'short int' parameter and re-initialize axis mode
 *
 * if the command typed is a write function, records the value,
 * if it is a read function returns the value asked.
 * in addition to regular axis parameter write, recompute of the phase table
 * and control mode state is invoked and commutator state is reinitialized.
 */
int cmd_do_regptmod_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  short *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(short*)((long)des->info[0]+(char*)mcs);
  if(opchar==':'){
    if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;
    pxmc_axis_release(mcs);
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    if((val<((long)des->info[1])) || (val>((long)des->info[2])))
      return -CMDERR_BADPAR;
    *ptr=val;
    val=pxmc_axis_mode(mcs,0);
    if(val<0)
      return val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0;
}

cmd_des_t const cmd_des_ptirc={0, CDESM_OPCHR|CDESM_RW,
			"REGPTIRC?","number of irc pulses per phase table",cmd_do_regptmod_short_val,
			{(char*)pxmc_state_offs(pxms_ptirc),
			 (char*)4,(char*)10000}};
cmd_des_t const cmd_des_ptper={0, CDESM_OPCHR|CDESM_RW,
			"REGPTPER?","number of elmag. revolutions per phase table",cmd_do_regptmod_short_val,
			{(char*)pxmc_state_offs(pxms_ptper),
			 (char*)1,(char*)100}};
cmd_des_t const cmd_des_ptmark={0, CDESM_OPCHR|CDESM_RW,
			"REGPTMARK?","phase index at encoder index mark in irc pulses",cmd_do_regptmod_short_val,
			{(char*)pxmc_state_offs(pxms_ptmark),
			 (char*)0,(char*)10000}};
cmd_des_t const cmd_des_ptshift={0, CDESM_OPCHR|CDESM_RW,
			"REGPTSHIFT?","shift (in irc) of generated phase curves",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_ptshift),
			 0}};
cmd_des_t const cmd_des_ptvang={0, CDESM_OPCHR|CDESM_RW,
			"REGPTVANG?","angle (in irc) between rotor and stator mag. fld.",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_ptvang),
			 0}};
cmd_des_t const cmd_des_pwm1cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM1COR?","PWM1 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm1cor),
			 0}};
cmd_des_t const cmd_des_pwm2cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM2COR?","PWM2 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm2cor),
			 0}};
cmd_des_t const cmd_des_pwm3cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM3COR?","PWM3 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm2cor),
			 0}};
cmd_des_t const cmd_des_pthalph={0, CDESM_OPCHR|CDESM_RD,
			"REGPTHALPH?","hal input phase",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_hal),
			 0}};

cmd_des_t const *const cmd_pxmc_ptable[]={
  &cmd_des_ptirc,
  &cmd_des_ptper,
  &cmd_des_ptmark,
  &cmd_des_ptshift,
  &cmd_des_ptvang,
  &cmd_des_pwm1cor,
  &cmd_des_pwm2cor,
  &cmd_des_pwm3cor,
  &cmd_des_pthalph,
  NULL
};
