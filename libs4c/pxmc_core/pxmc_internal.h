/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_internal.h - generic multi axis motion controller
                    internal functions

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_INTERNAL_H_
#define _PXMC_INTERNAL_H_

#include "pxmc.h"

int pxmc_spd_gacc(long *spd,long rspd,long acc);

int pxmc_cont_gi(pxmc_state_t *mcs);
int pxmc_trp_gi(pxmc_state_t *mcs);
int pxmc_trp_spdfg_gi(pxmc_state_t *mcs);
int pxmc_spd_gi(pxmc_state_t *mcs);
int pxmc_spdnext_gi(pxmc_state_t *mcs);
int pxmc_spdnext_gend(pxmc_state_t *mcs);
int pxmc_stop_gi(pxmc_state_t *mcs);

int pxmc_pid_con(pxmc_state_t *mcs);
int pxmc_pidnl_con(pxmc_state_t *mcs);

short pxmc_ptvang_deg2irc(pxmc_state_t *mcs, int deg);

int pxmc_init_ptable_sin(pxmc_state_t *mcs);
int pxmc_init_ptable_triang(pxmc_state_t *mcs);
int pxmc_init_ptable_trapez(pxmc_state_t *mcs, int flatrat);
int pxmc_init_ptable_sin3ph(pxmc_state_t *mcs);
int pxmc_init_ptable_sin3phup(pxmc_state_t *mcs);
int pxmc_init_ptable(pxmc_state_t *mcs, int profile);

#define PXMC_PTPROF_DEFAULT  0
#define PXMC_PTPROF_SIN      1
#define PXMC_PTPROF_TRIANG   2
#define PXMC_PTPROF_TRAPEZ   3
#define PXMC_PTPROF_3PH      16
#define PXMC_PTPROF_SIN3PH   17
#define PXMC_PTPROF_SIN3FUP  18

/**
 * struct pxmc_ptprofile - precomputed Phase Table Profile
 * @ptname:	Textual identification of the phase profile
 * @ptid:	Numeric identification
 * @ptirc:	IRC count per phase table
 * @ptper:	Number of periods per table
 * @ptamp:	Amplitude of phase table profile (max value)
 * @ptphnum:	Number of phases in the table
 * @ptptr1:	Pointer to commutation table for phase 1
 * @ptptr2:	Pointer to commutation table for phase 2
 * @ptptr3:	Pointer to commutation table for phase 3
 */
typedef struct pxmc_ptprofile {
  const char *ptname;
  unsigned ptid;
  short ptirc;
  short ptper;
  unsigned short ptamp;
  unsigned short ptphnum;
  const short *ptptr1;
  const short *ptptr2;
  const short *ptptr3;
} pxmc_ptprofile_t;

int pxmc_ptable_set_profile(pxmc_state_t *mcs, const pxmc_ptprofile_t *ptprofile,
                        unsigned int ptirc, unsigned int ptper);

extern const pxmc_ptprofile_t pxmc_ptprofile_sin;
extern const pxmc_ptprofile_t pxmc_ptprofile_sin3ph;
extern const pxmc_ptprofile_t pxmc_ptprofile_sin3phup;
extern const pxmc_ptprofile_t pxmc_ptprofile_sin3phup_zic;

/********************************************************************/
/* generator variant selection from axis configuration */

#ifndef PXMC_WITH_GEN_SELECTION
static inline pxmc_call_t *
pxmc_get_stop_gi_4axis(pxmc_state_t *mcs)
  {return pxmc_stop_gi;}
static inline pxmc_call_t *
pxmc_get_cont_gi_4axis(pxmc_state_t *mcs)
  {return pxmc_cont_gi;}
static inline pxmc_call_t *
pxmc_get_spdnext_gi_4axis(pxmc_state_t *mcs)
  {return pxmc_spdnext_gi;}
static inline pxmc_call_t *
pxmc_get_trp_gi_4axis(pxmc_state_t *mcs, int mode)
  {return pxmc_trp_gi;}
static inline pxmc_call_t *
pxmc_get_trp_retgt_4axis(pxmc_state_t *mcs)
  {return 0 /*pxmc_trp_retgt*/;}
static inline pxmc_call_t *
pxmc_get_trp_spdfg_gi_4axis(pxmc_state_t *mcs)
 #ifdef PXMC_WITH_FINE_GRAINED
  {return pxmc_trp_spdfg_gi;}
 #else /*PXMC_WITH_FINE_GRAINED*/
  {return 0;}
 #endif /*PXMC_WITH_FINE_GRAINED*/
static inline pxmc_call_t *
pxmc_get_trprel_gi_4axis(pxmc_state_t *mcs)
  {return 0 /*pxmc_trprel_gi*/;}
static inline pxmc_call_t *
pxmc_get_spd_gi_4axis(pxmc_state_t *mcs)
  {return pxmc_spd_gi;}
static inline pxmc_call_t *
pxmc_get_spdfg_gi_4axis(pxmc_state_t *mcs)
 #ifdef PXMC_WITH_FINE_GRAINED
  {return pxmc_spd_gi;}
 #else /*PXMC_WITH_FINE_GRAINED*/
  {return 0;}
 #endif /*PXMC_WITH_FINE_GRAINED*/
#else /*PXMC_WITH_GEN_SELECTION*/
pxmc_call_t *pxmc_get_stop_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_cont_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_spdnext_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_trp_gi_4axis(pxmc_state_t *mcs, int mode);
pxmc_call_t *pxmc_get_trp_retgt_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_trp_spdfg_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_trprel_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_spd_gi_4axis(pxmc_state_t *mcs);
pxmc_call_t *pxmc_get_spdfg_gi_4axis(pxmc_state_t *mcs);
#endif /*PXMC_WITH_GEN_SELECTION*/

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs);

int pxmc_pthalalign(pxmc_state_t *mcs, long r2acq, long spd, pxmc_call_t fin_fnc);

#endif /*_PXMC_INTERNAL_H_*/
