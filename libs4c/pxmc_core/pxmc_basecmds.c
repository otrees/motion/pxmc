/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_basecmds.c - generic multi axis motion controller
             generic commands for command processor

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>
#include "pxmc.h"
#include "pxmc_cmds.h"

/**
 * cmd_opchar_getreg - selects the right axis
 *
 * pxmc is designed for multi axis motion control, so each axis must be identificated.
 * This done by a capital letter. The first axis must be A, the 2nd B, etc.
 */
pxmc_state_t *cmd_opchar_getreg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned chan;
  pxmc_state_t *mcs;
  chan=*param[1]-'A';
  if(chan>=pxmc_main_list.pxml_cnt) return NULL;
  mcs=pxmc_main_list.pxml_arr[chan];
  if(!mcs) return NULL;
  return mcs;
}

/**
 * cmd_do_reg_go - checks the command format validity and calls pxmc_go.
 *
 * if pxmc_go returns -1, cmd_do_reg_go returns -1.
 */
int cmd_do_reg_go(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val<<=PXMC_SUBDIV(mcs);
  val=pxmc_go(mcs,val,(int)(intptr_t)des->info[0],(int)(intptr_t)des->info[1]);
  if(val<0)
    return val;
  return 0;
}

/**
 * cmd_do_pwm - checks the command format validity and calls pxmc_set_const_out.
 *
 */
int cmd_do_pwm(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  pxmc_set_const_out(mcs,val);
  return 0;
}

/**
 * cmd_do_reg_hh - checks the command format validity and calls pxmc_hh (home hardware).
 *
 * if pxmc_hh returns -1, cmd_do_reg_hh returns -1.
 */
int cmd_do_reg_hh(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=pxmc_hh(mcs);
  if(val<0)
    return val;
  return 0;
}


/**
 * cmd_do_reg_spd - checks the command format validity and calls pxmc_spd.
 *
 * if pxmc_spd returns -1, cmd_do_reg_spd returns -1.
 */
int cmd_do_reg_spd(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val=pxmc_spd(mcs,val,0);
  if(val<0)
    return val;
  return 0;
}

#ifdef PXMC_WITH_FINE_GRAINED
/**
 * cmd_do_reg_spdfg - checks the command format validity and calls pxmc_spdfg.
 *
 * if pxmc_spdfg returns -1, cmd_do_reg_spdfg returns -1.
 */
int cmd_do_reg_spdfg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val=pxmc_spdfg(mcs,val,0);
  if(val<0)
    return val;
  return 0;
}

/**
 * cmd_do_reg_spdfgt - checks the command format validity and calls pxmc_spdfg.
 *
 * if pxmc_spdfgt returns -1, cmd_do_reg_spdfg returns -1.
 */
int cmd_do_reg_spdfgt(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val,timeout;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
  if(si_long(&p,&timeout,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val=pxmc_spdfg(mcs,val,timeout);
  if(val<0)
    return val;
  return 0;
}
#endif /*PXMC_WITH_FINE_GRAINED*/

int cmd_do_reg_spdt(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val,timeout;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
  if(si_long(&p,&timeout,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val=pxmc_spd(mcs,val,timeout);
  if(val<0)
    return val;
  return 0;
}

/**
 * cmd_do_stop - checks the command format validity and calls pxmc_stop.
 *
 */
int cmd_do_stop(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_stop(mcs,0);
  return 0;
}

/**
 * cmd_do_release - checks the command format validity and calls pxmc_axis_release(mcs).
 *
 */
int cmd_do_release(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_axis_release(mcs);
  return 0;
}

/**
 * cmd_do_clrerr - checks the command format validity, clears the error flag.
 *
 * it also stop the rotation calling pxmc_axis_release(mcs)
 */
int cmd_do_clrerr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_axis_release(mcs);
  pxmc_clear_flag(mcs,PXMS_ERR_b);
  return 0;
}

/**
 * cmd_do_zero - checks the command format validity, sets axis position to 0.
 *
 * it also stop the rotation calling pxmc_axis_release(mcs)
 */
int cmd_do_zero(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_axis_release(mcs);
  pxmc_axis_set_pos(mcs,0);
  return 0;
}

/**
 * cmd_do_reg_rw_pos - read or write function, param is converted in 'long' and shifted
 *
 * if the command typed is a write function, records the value,
 * if it is a read function returns the value asked.
 */
int cmd_do_reg_rw_pos(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  long *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(long*)((intptr_t)des->info[0]+(char*)mcs);
  if(opchar==':'){
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    *ptr=val<<PXMC_SUBDIV(mcs);
  }else{
    return cmd_opchar_replong(cmd_io, param, (*ptr)>>PXMC_SUBDIV(mcs), 0, 0);
  }
  return 0;
}

/**
 * cmd_do_reg_short_val - read or write function, param is converted in 'integer'
 *
 * if the command typed is a write function, records the value,
 * if it is a read function returns the value asked.
 */
int cmd_do_reg_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  short *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(short*)((intptr_t)des->info[0]+(char*)mcs);
  if(opchar==':'){
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0;
}

/**
 * cmd_do_reg_long_val - read or write function, param is converted in 'long'
 *
 * if the command typed is a write function, records the value,
 * if it is a read function returns the value asked.
 */
int cmd_do_reg_long_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  long *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(long*)((intptr_t)des->info[0]+(char*)mcs);
  if(opchar==':'){
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0;
}

int cmd_do_stop_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    pxmc_stop(pxmc_main_list.pxml_arr[i],0);
  }
  return 0;
}

int cmd_do_release_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    pxmc_axis_release(mcs);
  }
  return 0;
}

/* motor executable commands */
cmd_des_t const cmd_des_go={0, CDESM_OPCHR|CDESM_RW,
			"G?","go to target position",cmd_do_reg_go,
			{0,(char*)PXMC_GO_ABS}};
cmd_des_t const cmd_des_gorel={0, CDESM_OPCHR|CDESM_RW,
			"GR?","go relative",cmd_do_reg_go,
			{0,(char*)PXMC_GO_REL}};
cmd_des_t const cmd_des_pwm={0, CDESM_OPCHR|CDESM_RW,
			"PWM?","direct axis PWM output",cmd_do_pwm,{}};
cmd_des_t const cmd_des_hh={0, CDESM_OPCHR,"HH?","hard home request for axis",cmd_do_reg_hh,{}};
cmd_des_t const cmd_des_spd={0, CDESM_OPCHR,"SPD?","speed request for axis",cmd_do_reg_spd,{}};
#ifdef PXMC_WITH_FINE_GRAINED
cmd_des_t const cmd_des_spdfg={0, CDESM_OPCHR,"SPDFG?",
			"fine grained speed request for axis",cmd_do_reg_spdfg,{}};
cmd_des_t const cmd_des_spdfgt={0, CDESM_OPCHR,"SPDFGT?",
                        "fine grained speed request for axis with timeout",cmd_do_reg_spdfgt,{}};
#endif
cmd_des_t const cmd_des_spdt={0, CDESM_OPCHR,
			"SPDT?","speed request with timeout",cmd_do_reg_spdt,{}};
cmd_des_t const cmd_des_stop={0, CDESM_OPCHR,
			"STOP?","stop motion of requested axis",cmd_do_stop,{}};
cmd_des_t const cmd_des_release={0, CDESM_OPCHR,
			"RELEASE?","releases axis closed loop control",cmd_do_release,{}};
cmd_des_t const cmd_des_zero={0, CDESM_OPCHR,
			"ZERO?","zero actual position",cmd_do_zero,{}};
cmd_des_t const cmd_des_clrerr={0, CDESM_OPCHR,
			"PURGE?"," clear 'axis in error state' flag",cmd_do_clrerr,{}};
/* motors and controllers variables */
cmd_des_t const cmd_des_ap={0, CDESM_OPCHR|CDESM_RD,
			"AP?","actual position",cmd_do_reg_rw_pos,
			{(char*)pxmc_state_offs(pxms_ap),
			 0}};
cmd_des_t const cmd_des_st={0, CDESM_OPCHR|CDESM_RD,
			"ST?","axis status bits encoded in number",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_flg),
			 0}};
cmd_des_t const cmd_des_axerr={0, CDESM_OPCHR|CDESM_RD,
			"AXERR?","last axis error code",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_errno),
			 0}};
cmd_des_t const cmd_des_regp={0, CDESM_OPCHR|CDESM_RW,
			"REGP?","controller proportional gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_p),
			 0}};
cmd_des_t const cmd_des_regi={0, CDESM_OPCHR|CDESM_RW,
			"REGI?","controller integral gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_i),
			 0}};
cmd_des_t const cmd_des_regd={0, CDESM_OPCHR|CDESM_RW,
			"REGD?","controller derivative gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_d),
			 0}};
cmd_des_t const cmd_des_regs1={0, CDESM_OPCHR|CDESM_RW,
			"REGS1?","controller S1",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_s1),
			 0}};
cmd_des_t const cmd_des_regs2={0, CDESM_OPCHR|CDESM_RW,
			"REGS2?","controller S2",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_s2),
			 0}};
cmd_des_t const cmd_des_regmd={0, CDESM_OPCHR|CDESM_RW,
			"REGMD?","maximal allowed position error",cmd_do_reg_rw_pos,
			{(char*)pxmc_state_offs(pxms_md),
			 0}};
cmd_des_t const cmd_des_regms={0, CDESM_OPCHR|CDESM_RW,
			"REGMS?","maximal speed",cmd_do_reg_long_val,
			{(char*)pxmc_state_offs(pxms_ms),
			 0}};
cmd_des_t const cmd_des_regacc={0, CDESM_OPCHR|CDESM_RW,
			"REGACC?","maximal acceleration",cmd_do_reg_long_val,
			{(char*)pxmc_state_offs(pxms_ma),
			 0}};
cmd_des_t const cmd_des_regme={0, CDESM_OPCHR|CDESM_RW,
			"REGME?","maximal PWM energy or voltage for axis",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_me),
			 0}};
cmd_des_t const cmd_des_regcfg={0, CDESM_OPCHR|CDESM_RW,
			"REGCFG?","hard home and profile configuration",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_cfg),
			 0}};

cmd_des_t const *const cmd_pxmc_base[]={
  &cmd_des_go,
  &cmd_des_gorel,
  &cmd_des_pwm,
  &cmd_des_hh,
  &cmd_des_spd,
  &cmd_des_spdt,
 #ifdef PXMC_WITH_FINE_GRAINED
  &cmd_des_spdfg,
  &cmd_des_spdfgt,
 #endif
  &cmd_des_stop,
  &cmd_des_release,
  &cmd_des_zero,
  &cmd_des_clrerr,
  &cmd_des_ap,
  &cmd_des_st,
  &cmd_des_axerr,
  &cmd_des_regp,
  &cmd_des_regi,
  &cmd_des_regd,
  &cmd_des_regs1,
  &cmd_des_regs2,
  &cmd_des_regmd,
  &cmd_des_regms,
  &cmd_des_regacc,
  &cmd_des_regme,
  &cmd_des_regcfg,
  NULL
};
