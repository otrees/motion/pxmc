/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_ptcompute.c - generic multi axis motion controller
                  phase tables generation support

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <limits.h>
#include "pxmc.h"
#include "pxmc_internal.h"

#define PXMC_PTAMP (0x7fff)

int
pxmc_init_ptable_sin(pxmc_state_t *mcs)
{
  int ptirc=mcs->pxms_ptirc;
  int ptper=mcs->pxms_ptper;
  float ptamp=mcs->pxms_ptamp;
  int i;
  float a;

  for(i=0;i<ptirc;i++) {
    a=i*(float)ptper*2*M_PI/ptirc;
    mcs->pxms_ptptr1[i]=sin(a)*ptamp;
    mcs->pxms_ptptr2[i]=cos(a)*ptamp;
  }
  return 0;
}

int
pxmc_init_ptable_sin3ph(pxmc_state_t *mcs)
{
  int ptirc=mcs->pxms_ptirc;
  int ptper=mcs->pxms_ptper;
  float ptamp=mcs->pxms_ptamp;
  int i;
  float a;

  for(i=0;i<ptirc;i++) {
    a=i*(float)ptper*2*M_PI/ptirc;
    mcs->pxms_ptptr1[i]=cos(a)*ptamp;
    mcs->pxms_ptptr2[i]=cos(a+2*M_PI/3)*ptamp;
    mcs->pxms_ptptr3[i]=cos(a+4*M_PI/3)*ptamp;
  }
  return 0;
}

int
pxmc_init_ptable_sin3phup(pxmc_state_t *mcs)
{
  int ptirc=mcs->pxms_ptirc;
  int ptper=mcs->pxms_ptper;
  float ptamp=mcs->pxms_ptamp;
  int i;
  int min_val;
  float a;

  ptamp/=2;

  for(i=0;i<ptirc;i++) {
    a=i*(float)ptper*2*M_PI/ptirc;
    mcs->pxms_ptptr1[i]=cos(a)*ptamp;
    mcs->pxms_ptptr2[i]=cos(a+2*M_PI/3)*ptamp;
    mcs->pxms_ptptr3[i]=cos(a+4*M_PI/3)*ptamp;

    min_val=mcs->pxms_ptptr1[i];
    if(min_val>mcs->pxms_ptptr2[i])
      min_val=mcs->pxms_ptptr2[i];
    if(min_val>mcs->pxms_ptptr3[i])
      min_val=mcs->pxms_ptptr3[i];

    mcs->pxms_ptptr1[i]-=min_val;
    mcs->pxms_ptptr2[i]-=min_val;
    mcs->pxms_ptptr3[i]-=min_val;
  }
  return 0;
}

int
pxmc_init_ptable_triang(pxmc_state_t *mcs)
{
  int ptirc=mcs->pxms_ptirc;
  int ptper=mcs->pxms_ptper;
  unsigned int ptamp=mcs->pxms_ptamp;
  int i;
  long l;

  for(i=0;i<ptirc;i++) {
    l=i;
    l=(l*ptper*2+3*ptirc/2)%(ptirc*2);	/* phase per ^= 2*ptirc */
    if(l<=ptirc) l=ptirc-2*l;
    else l=2*l-3*ptirc;
    mcs->pxms_ptptr1[i]=(ptamp*l)/ptirc;

    l=i;
    l=(l*ptper*2)%(ptirc*2);		/* phase per ^= 2*ptirc */
    if(l<=ptirc) l=ptirc-2*l;
    else l=2*l-3*ptirc;
    mcs->pxms_ptptr2[i]=(ptamp*l)/ptirc;
  }
  return 0;
}

int
pxmc_init_ptable_trapez(pxmc_state_t *mcs, int flatrat)
{
  int ptirc=mcs->pxms_ptirc;
  int ptper=mcs->pxms_ptper;
  unsigned int ptamp=mcs->pxms_ptamp;
  int i;
  long l;
  long lim;

  lim=((long)256*ptamp)/flatrat;
  for(i=0;i<ptirc;i++) {
    l=i;
    l=(l*ptper*2+3*ptirc/2)%(ptirc*2);	/* phase per ^= 2*ptirc */
    if(l<=ptirc) l=ptirc-2*l;
    else l=2*l-3*ptirc;
    l=(ptamp*l)/ptirc;

    if(l>lim) l=ptamp;
    else if(l<-lim) l=-ptamp;
    else l=(l*flatrat)/256;

    mcs->pxms_ptptr1[i]=l;

    l=i;
    l=(l*ptper*2)%(ptirc*2);		/* phase per ^= 2*ptirc */
    if(l<=ptirc) l=ptirc-2*l;
    else l=2*l-3*ptirc;
    l=(ptamp*l)/ptirc;

    if(l>lim) l=ptamp;
    else if(l<-lim) l=-ptamp;
    else l=(l*flatrat)/256;

    mcs->pxms_ptptr2[i]=l;
  }
  return 0;
}

/**
 * pxmc_init_ptable - Initializes phase tables
 * @mcs:	Motion controller state information
 * @profile:	Phase profile selection
 *
 * This function initializes phase tables @pxms_ptptr1 and @pxms_ptptr2.
 * Profile is computed by one of profile functions -
 * pxmc_init_ptable_sin(), pxmc_init_ptable_triang(),
 * pxmc_init_ptable_trapez()
 */
int
pxmc_init_ptable(pxmc_state_t *mcs, int profile)
{
  int ptirc=mcs->pxms_ptirc;
  int ret;

  if(!mcs->pxms_ptper || !mcs->pxms_ptirc)
    return -1 /*CMDERR_BADPAR*/;

  if(mcs->pxms_ptptr1)
    free(mcs->pxms_ptptr1);
  if(mcs->pxms_ptptr2)
    free(mcs->pxms_ptptr2);
  if(mcs->pxms_ptptr3)
    free(mcs->pxms_ptptr3);

  mcs->pxms_ptptr1=malloc(ptirc*sizeof(mcs->pxms_ptptr1[0]));
  mcs->pxms_ptptr2=malloc(ptirc*sizeof(mcs->pxms_ptptr2[0]));
  if(!mcs->pxms_ptptr1 || !mcs->pxms_ptptr2)
    return -1 /*CMDERR_NOMEM*/;

  if(profile>=PXMC_PTPROF_3PH){
    mcs->pxms_ptptr3=malloc(ptirc*sizeof(mcs->pxms_ptptr3[0]));
    if(!mcs->pxms_ptptr3)
      return -1 /*CMDERR_NOMEM*/;
  }

  if(!mcs->pxms_ptamp)
    mcs->pxms_ptamp=PXMC_PTAMP;

  mcs->pxms_ptindx=0;

  switch(profile){
    case PXMC_PTPROF_SIN:
      ret=pxmc_init_ptable_sin(mcs);
      break;
    case PXMC_PTPROF_TRIANG:
      ret=pxmc_init_ptable_triang(mcs);
      break;
    case PXMC_PTPROF_TRAPEZ:
      ret=pxmc_init_ptable_trapez(mcs, 512);
      break;
    case PXMC_PTPROF_3PH:
    case PXMC_PTPROF_SIN3PH:
      ret=pxmc_init_ptable_sin3ph(mcs);
      break;
    case PXMC_PTPROF_SIN3FUP:
      ret=pxmc_init_ptable_sin3phup(mcs);
      break;

    default:
      ret=pxmc_init_ptable_triang(mcs);
  }

  return ret;
}
