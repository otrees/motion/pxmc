/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc.h -     generic multi axis motion controller interface
               position controller subsystem core definitions

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_H_
#define _PXMC_H_

#include "pxmc_config.h"

#ifdef PXMC_WITH_FINE_GRAINED
#ifndef PXMC_SUBDIVFG
/* additional number of fraction bits for fine grainned generator */
#define PXMC_SUBDIVFG(mcs) 16
#endif

#if PXMC_SUBDIVFG(0)>16
typedef long pxmc_fg_t;
#else
typedef short pxmc_fg_t;
#endif

#if PXMC_SUBDIVFG(0)>=32
  #define PXMC_SPDFG_ONLY
#endif
#endif /*PXMC_WITH_FINE_GRAINED*/

#ifdef PXMC_WITH_FLAGS_LONG_TYPE
/* some architectures prefer/require long type for flags */
typedef unsigned long pxmc_flags_t;
#else /*PXMC_WITH_FLAGS_LONG_TYPE*/
typedef unsigned short pxmc_flags_t;
#endif /*PXMC_WITH_FLAGS_LONG_TYPE*/

#ifndef PXMC_INFO_TYPE_DEFINED
#define PXMC_INFO_TYPE_DEFINED
#if defined(PXMC_WITH_STDINT_INCLUDED)&&(INTPTR_MAX>LONG_MAX)
typedef intptr_t pxmc_info_t;
#else /* long type is enough to hold pointer */
/* type used mainly for generators and pxms_do_out/pxms_do_inp */
/* the type has to be able hold position value and pointer data type */
typedef long pxmc_info_t;
#endif /* long type is enough to hold pointer */
#endif /*PXMC_INFO_TYPE_DEFINED*/

/* General measurement, controller, output
   and profile generator structure */

/* controler state flags stored in "pxms_flg" field */

#define PXMS_ENI_b  0	/* enable input (IRC) update */
#define PXMS_ENR_b  1	/* enable controller (regulator) execution and output update */
#define PXMS_ENG_b  2	/* enable requested value (position) generator */
#define PXMS_ERR_b  3	/* axis in error state */
#define PXMS_BSY_b  4	/* axis busy */
#define PXMS_DBG_b  5	/* enable debugging */
#define PXMS_CMV_b  6	/* in coordinated group */
#define PXMS_CQF_b  7	/* command queue full */
#define PXMS_PHA_b  8	/* phase alligned */
#define PXMS_PTI_b  9	/* update index pointing to the phase table (ptindx) */
#define PXMS_ENO_b 10	/* enable output update only (without the controller); usefull for brushless motors */
#define PXMS_PRA_b 11	/* realign phase-table base from HAL sensor */

#define PXMS_ENI_m  (1<<PXMS_ENI_b)
#define PXMS_ENR_m  (1<<PXMS_ENR_b)
#define PXMS_ENG_m  (1<<PXMS_ENG_b)
#define PXMS_ERR_m  (1<<PXMS_ERR_b)
#define PXMS_BSY_m  (1<<PXMS_BSY_b)
#define PXMS_DBG_m  (1<<PXMS_DBG_b)
#define PXMS_CMV_m  (1<<PXMS_CMV_b)
#define PXMS_CQF_m  (1<<PXMS_CQF_b)
#define PXMS_PHA_m  (1<<PXMS_PHA_b)
#define PXMS_PTI_m  (1<<PXMS_PTI_b)
#define PXMS_ENO_m  (1<<PXMS_ENO_b)
#define PXMS_PRA_m  (1<<PXMS_PRA_b)

/* cnfiguration flags stored in "pxms_cfg" field */

#define PXMS_CFG_HDIR_b	3  /* initial direction */
#define PXMS_CFG_HRI_b	4  /* use revolution index from HP HEDS */
#define PXMS_CFG_HMC_b	5  /* find mark center */
#define PXMS_CFG_HLS_b	6  /* use limit switch */
#define PXMS_CFG_HPS_b	7  /* polarity of switch */
#define PXMS_CFG_SMTH_b	8  /* smooth speed at changes and stop */
#define PXMS_CFG_MD2E_b	10 /* difference abs(pxms_ap-pxms_rp)>pxms_md => error */
#define PXMS_CFG_CYCL_b	11 /* axis is cyclic one => overflow is intended */
#define PXMS_CFG_I2PT_b	12 /* use index mark to adjust phase table alignment */

#define PXMS_CFG_HSPD_m	7  /* initial speed of hardhome pxms_ms>>SSS */
#define PXMS_CFG_HDIR_m	(1<<PXMS_CFG_HDIR_b)
#define PXMS_CFG_HRI_m	(1<<PXMS_CFG_HRI_b)
#define PXMS_CFG_HMC_m	(1<<PXMS_CFG_HMC_b)
#define PXMS_CFG_HLS_m	(1<<PXMS_CFG_HLS_b)
#define PXMS_CFG_HPS_m	(1<<PXMS_CFG_HPS_b)
#define PXMS_CFG_SMTH_m	(1<<PXMS_CFG_SMTH_b)
#define PXMS_CFG_MD2E_m	(1<<PXMS_CFG_MD2E_b)
#define PXMS_CFG_CYCL_m	(1<<PXMS_CFG_CYCL_b)
#define PXMS_CFG_I2PT_m	(1<<PXMS_CFG_I2PT_b)

struct pxmc_state;

/* these functions use special calling convention if PXMC_WITH_FAST_CALL deffined */
typedef int pxmc_call_t(struct pxmc_state *mcs);

/**
 * struct pxmc_state - Motor Controller State Information
 * @pxms_flg: Holds flag enabling encoder (%PXMS_ENI_m), controller
 *	(%PXMS_ENR_m) and trajectory generator (%PXMS_ENG_m).
 *	Other flags represent status information - busy (%PXMS_BSY_m),
 *	error (%PXMS_ERR_m) and membership in coordinated group
 *	(%PXMS_CMV_m). Flag %PXMS_PHA_m is used for initial phase
 *	alignment. Flag %PXMS_DBG_m selects debugging for axis.
 * @pxms_do_inp: Pointer to function responsible for reading the encoder
 *	and updating @pxms_ap and @pxms_as.
 * @pxms_do_con: Pointer to position controller which computes @pxms_ene
 *	from axis state.
 * @pxms_do_out:	Transfers computed @pxms_ene to PWM subsystem.
 * @pxms_do_deb:	Debugging support routine.
 * @pxms_do_gen:	Trajectory generator
 * @pxms_do_ap2hw:	Preset new actual position into HW (is not mandatory)
 * @pxms_ap:	Actual motor position multiplied by (1<<%PXMC_SUBDIV)
 * @pxms_as:	Actual motor speed multiplied by (1<<%PXMC_SUBDIV)
 * @pxms_rp:	Required motor position *(1<<%PXMC_SUBDIV)
 * @pxms_rpfg:	Position extension for Fine Grained generator
 * @pxms_rs:	Required motor speed *(1<<%PXMC_SUBDIV)
 * @pxms_rsfg:	Speed extension for Fine Grained generator
 * @pxms_subdiv:	Optional per axis subdivision of hardware (IRC) unit if %PXMC_SUBDIV is not fixed value
 * @pxms_md:	Maximal accepted position difference
 * @pxms_ms:	Maximal speed in same units as @pxms_as and @pxms_rs
 * @pxms_ma:	Maximal acceleration
 * @pxms_inp_info: Additional info for @pxms_do_inp to select which irc to use
 * @pxms_out_info: Additional info for @pxms_do_out where @pxms_ene should be sent
 * @pxms_ene_d:	Output PWM direct (D) component
 * @pxms_ene:	Computed output energy / value of PWM signal (quadrature Q component for DQ)
 * @pxms_erc:	Axis error counter
 * @pxms_p:	Controller proportional constant
 * @pxms_i:	Controller integration constant
 * @pxms_d:	Controller derivative constant
 * @pxms_s1:	Controller special 1 constant
 * @pxms_s2:	Controller special 2 constant
 * @pxms_me:	Maximal allowed output energy or PWM
 * @pxms_foi:	Temporary for I computation
 * @pxms_fod:	Temporary for D computation
 * @pxms_tmp:	Temporary for help and debugging
 * @pxms_ptirc:	IRC count per phase table
 * @pxms_ptper:	Number of periods per table
 * @pxms_ptofs:	Offset between table and IRC counter (0 <= irc - pxms_ptofs < pxms_ptirc)
 * @pxms_ptshift: Shift of generated phase curves (high speed correction)
 * @pxms_ptmark:  Position of IRC index mark in the phase table
 * @pxms_ptvang:	 Angle (in irc) between rotor and stator mag. fld.
 * @pxms_ptindx:	 Index into commutation table
 * @pxms_ptptr1:	 Pointer to commutation table for phase 1
 * @pxms_ptptr2:	 Pointer to commutation table for phase 2
 * @pxms_ptptr3:	 Pointer to commutation table for phase 3
 * @pxms_ptscale_mult:	 Multiplication factor for ROM phase table scaling
 * @pxms_ptscale_shift:	 Right shift for ROM phase table scaling
 * @pxms_ptamp:		 Amplitude of phase table profile (max value)
 * @pxms_pwm1cor: Correction for PWM1 generator
 * @pxms_pwm2cor: Correction for PWM2 generator
 * @pxms_pwm3cor: Correction for PWM3 generator
 * @pxms_cur_d_act: Measured current D component
 * @pxms_cur_q_act: Measured current Q component
 * @pxms_errno:	 Error code
 * @pxms_cfg:	 Config information for axis
 * @pxms_ep:	 End position of movement
 * @pxms_gen_st:	 Status for generators
 * @pxms_gen_info: Field available for trajectory generators computations.
 * @pxms_hal:    Last value read from HALL sensors
 * @pxms_halerc: Error counter of hall errors
 *
 * This structure holds all state information for motion
 * control of one axis equipped with stepper or brush-less motor
 * with or without encoder feedback.
 */
typedef struct pxmc_state {
  /* axis flags */
  pxmc_flags_t pxms_flg;	/* controller and axis flags */
  /* sample time handling routines */
  pxmc_call_t *pxms_do_inp;	/* update pxms_ap and pxms_as */
  pxmc_call_t *pxms_do_con;	/* compute pxms_ene */
  pxmc_call_t *pxms_do_out;	/* output pxms_ene */
  pxmc_call_t *pxms_do_deb;	/* debugging support */
  pxmc_call_t *pxms_do_gen;	/* position generator */
  /* helper routines */
  pxmc_call_t *pxms_do_ap2hw;	/* set value of actual position to hw */
  /* axis actual state */
  long pxms_ap;		/* actual motor position *(1<<PXMC_SUBDIV) */
  long pxms_as;		/* actual motor speed *(1<<PXMC_SUBDIV) */
  /* controler requests */
  long pxms_rp;		/* required motor position *(1<<pxms_SUBDIV) */
 #ifdef PXMC_WITH_FINE_GRAINED
  pxmc_fg_t pxms_rpfg;	/* position extension for FG generator */
 #endif /*PXMC_WITH_FINE_GRAINED*/
  long pxms_rs;		/* required motor speed *(1<<pxms_SUBDIV) */
 #ifdef PXMC_WITH_FINE_GRAINED
  pxmc_fg_t pxms_rsfg;	/* speed extension for FG generator */
 #endif /*PXMC_WITH_FINE_GRAINED*/
  /* axis parameters */
 #ifndef PXMC_WITH_FIXED_SUBDIV
  short pxms_subdiv;	/* subdivision of hardware unit */
 #endif /*PXMC_WITH_FIXED_SUBDIV*/
  long pxms_md;		/* maximal pos. difference */
  long pxms_ms;		/* maximal speed */
  long pxms_ma;		/* maximal acceleration */
  pxmc_info_t pxms_inp_info; /* which irc to use */
  pxmc_info_t pxms_out_info; /* where put energy */
  /* controler outputs */
 #ifdef PXMC_WITH_EXTENDED_STATE
  short pxms_ene_d;	/* output energy or PWM direct (D) component */
 #endif /* PXMC_WITH_EXTENDED_STATE */
  short pxms_ene;	/* output energy or PWM quadrature (Q) component */
  short pxms_erc;	/* error counter */
  /* basic parameters for pid */
  short pxms_p;		/* proportional */
  short pxms_i;		/* integration */
  short pxms_d;		/* derivative */
  short pxms_s1;	/* special 1 */
  short pxms_s2;	/* special 2 */
  short pxms_me;	/* maximal energy or PWM output */
  /* helpers for regulators */
 #ifndef PXMC_WITH_FOI_FOD_LONG_TYPE
  short pxms_foi;	/* for I computation */
  short pxms_fod;	/* for D computation */
 #else /*PXMC_WITH_FOI_FOD_LONG_TYPE*/
  long pxms_foi;	/* for I computation */
  long pxms_fod;	/* for D computation */
 #endif /*PXMC_WITH_FOI_FOD_LONG_TYPE*/
  long  pxms_tmp;	/* for help and debugging */
 #ifdef PXMC_WITH_PHASE_TABLE
  /* informations for brushless motors */
  short	pxms_ptirc;	/* IRC count per phase table */
  short	pxms_ptper;	/* number of periods per table */
  short	pxms_ptofs;	/* offset between table and IRC counter */
  short	pxms_ptshift;	/* shift of generated phase curves */
  short	pxms_ptmark;	/* position of IRC index mark */
  short	pxms_ptvang;	/* angle (in irc) between rotor and stator mag. fld.*/
  short	pxms_ptindx;	/* index into commutation table */
  short	*pxms_ptptr1;	/* pointer to commutation table for phase 1 */
  short	*pxms_ptptr2;	/* pointer to commutation table for phase 2 */
  short	*pxms_ptptr3;	/* pointer to commutation table for phase 3 */
  unsigned long pxms_ptscale_mult; /* multiplication factor for table scaling */
  unsigned short pxms_ptscale_shift; /* right shift for table scaling */
  unsigned short pxms_ptamp; /* amplitude of phase table profile (max value) */
  /* correction constants for PWM outputs */
  short	pxms_pwm1cor;	/* correction for PWM1 generator */
  short	pxms_pwm2cor;	/* correction for PWM2 generator */
  short	pxms_pwm3cor;	/* correction for PWM3 generator */
 #endif /*PXMC_WITH_PHASE_TABLE*/
 #ifdef PXMC_WITH_EXTENDED_STATE
  long  pxms_cur_d_act; /* measured current D component */
  long  pxms_cur_q_act; /* measured current Q component */
 #endif /* PXMC_WITH_EXTENDED_STATE */
 #ifdef PXMC_WITH_CURRENTFB
  /* current feedback controller */
  long  pxms_curfb_acum;/* accumulator for current measurement */
  long  pxms_curfb_acur;/* accumulator for reactiv current */
  short pxms_curfb_act;	/* actual meassured value of total current */
  short pxms_curfb_acr;	/* actual meassured value of reactive current */
  short pxms_curfb_p;	/* current feedback proportional constant */
  short pxms_curfb_i;	/* current feedback integrative constant  */
  short pxms_curfb_ir;	/* current feedback integrative constant  */
  long  pxms_curfb_foi;	/* storage of integration accumulation */
  short pxms_curfb_out;	/* actual current controller output value */
 #endif /*PXMC_WITH_CURRENTFB*/
  /* error code */
  short	pxms_errno;	/* error code */
  /* informations for position generator */
  short pxms_cfg;	/* config info for axis */
  long  pxms_ep;	/* end position of movement */
  short pxms_gen_st;	/* status for generators */
  pxmc_info_t pxms_gen_info[8];
  short pxms_hal;       /* last value read from HALL sensors */
  short pxms_halerc;    /* error counter of hall errors */
} pxmc_state_t;

/* evaluate offset of field from beginning of pxmc_state in bytes */
#define pxmc_state_offs(_fld) \
		((size_t)&((pxmc_state_t *)0L)->_fld)

#define pxmc_set_errno(mcs,err) \
  do { mcs->pxms_errno=err; pxmc_set_flag(mcs,PXMS_ERR_b); } while(0)

#define pxmc_list_for_each_mcs(pxmc_main_list, i, pxms) for ((i)=0; (pxms)=(pxmc_main_list).pxml_arr[(i)],(i)<pxmc_main_list.pxml_cnt; (i)++)
#define pxmc_for_each_mcs(i, pxms) pxmc_list_for_each_mcs(pxmc_main_list, i, pxms)

#ifndef PXMC_FLAG_OPS_DEFINED
#define PXMC_FLAG_OPS_DEFINED
#ifndef PXMC_WITH_FLAGS_BYBITS_ONLY

#ifdef PXMC_WITH_FLAGS_LONG_TYPE

#define pxmc_clear_flags(mcs,mask) \
  atomic_clear_mask(mask,&(mcs->pxms_flg))

#define pxmc_set_flags(mcs,mask) \
  atomic_set_mask(mask,&(mcs->pxms_flg))

#define pxmc_clear_flag(mcs,nr) \
  atomic_clear_mask(1<<(nr),&(mcs->pxms_flg))

#define pxmc_set_flag(mcs,nr) \
  atomic_set_mask(1<<(nr),&(mcs->pxms_flg))

#else /*PXMC_WITH_FLAGS_LONG_TYPE*/

#define pxmc_clear_flags(mcs,mask) \
  atomic_clear_mask_w(mask,&(mcs->pxms_flg))

#define pxmc_set_flags(mcs,mask) \
  atomic_set_mask_w(mask,&(mcs->pxms_flg))

#define pxmc_clear_flag(mcs,nr) \
  atomic_clear_mask_w(1<<(nr),&(mcs->pxms_flg))

#define pxmc_set_flag(mcs,nr) \
  atomic_set_mask_w(1<<(nr),&(mcs->pxms_flg))

#endif /*PXMC_WITH_FLAGS_LONG_TYPE*/

#else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/

#define pxmc_clear_flag(mcs,nr) \
  clear_bit(nr,&((mcs)->pxms_flg))

#define pxmc_set_flag(mcs,nr) \
  set_bit(nr,&((mcs)->pxms_flg))

#endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
#endif /*PXMC_FLAG_OPS_DEFINED*/

#define PXMS_E_COMM  0x105	/* 261 - offset of commutation error */
#define PXMS_E_MAXPD 0x106	/* 262 - difference of position over limit */
#define PXMS_E_OVERL 0x107	/* 263 - overload */
#define PXMS_E_HAL   0x108	/* 264 - hal problem */
#define PXMS_E_POWER_STAGE 0x109/* 265 - power stage fault signal */
#define PXMS_E_I2PT_TOOBIG 0x10A/* 266 - too big diffrence between hal and index phase offset */
#define PXMS_E_WINDCURRENT 0x10B/* 267 - winding current too high */
#define PXMS_E_WINDCURADC  0x10C/* 268 - winding current sensing ADC failure */
#define PXMS_E_MCC_FAULT   0x10D/* 269 - motion control coprocesor failure */
#define PXMS_E_UV_PROT     0x10E/* 270 - undervoltage protection */

#ifdef PXMC_WITH_FAST_CALL
/* call routine with pxmc_fast calling convention */
extern int pxmc_fast_call(pxmc_state_t *mcs, int fnc(pxmc_state_t *mcs));
#define pxmc_call(_st,_fnc) pxmc_fast_call((_st),(_fnc))
#else /*PXMC_WITH_FAST_CALL*/
#define pxmc_call(_st,_fnc) ((_fnc)(_st))
#endif /*PXMC_WITH_FAST_CALL*/


/* List of controller states */

typedef struct pxmc_state_list{
  pxmc_state_t **pxml_arr;
  short        pxml_cnt;
}pxmc_state_list_t;

/* routines for axis/controller state manipulation and examination */

/* set direct constant output */
void pxmc_set_const_out(pxmc_state_t *mcs,int val);

/* smooth connection of controler to axis */
int pxmc_connect_controller(pxmc_state_t *mcs);

/* prepares for change of generator */
int pxmc_set_gen_prep(pxmc_state_t *mcs);

/* sets generator, check non-zero speed and provide smooth stop
   before requested function, must pxmc_set_gen_prep be called before */
int pxmc_set_gen_smth(pxmc_state_t *mcs, pxmc_call_t *do_gen_gi, int flg);

/* start of trapezoidal speed profile motion to the position*/
int pxmc_go(pxmc_state_t *mcs, long val, int res, int mode);
#define PXMC_GO_ABS 0		/* absolute mode */
#define PXMC_GO_REL 1		/* relative mode to RP */

/* constant speed generator */
int pxmc_spd(pxmc_state_t *mcs, long val, int timeout);

#ifdef PXMC_WITH_FINE_GRAINED
/* start of trapezoidal speed profile motion
   to requested the position with set up finegrained speed */
int pxmc_go_spdfg(pxmc_state_t *mcs, long endpos, long speed, int mode);

/* constant fine grained speed generator */
int pxmc_spdfg(pxmc_state_t *mcs, long val, int timeout);
#endif /*PXMC_WITH_FINE_GRAINED*/

/* stop motion */
int pxmc_stop(pxmc_state_t *mcs, int mode);

/* set new actual and requested position */
int pxmc_axis_set_pos(pxmc_state_t *mcs, long pos);

/* release control of given axis */
void pxmc_axis_release(pxmc_state_t *mcs);

/* tuning and development support */
typedef struct pxmc_dbg_hist{
  long *ptr;
  long *buff;
  long *end;
} pxmc_dbg_hist_t;

extern pxmc_dbg_hist_t *pxmc_dbg_hist;

#ifdef PXMC_WITH_DBG_HIST

int pxmc_dbg_histfree(pxmc_dbg_hist_t *hist);
pxmc_dbg_hist_t *pxmc_dbg_histalloc(int count);

int pxmc_dbgset(pxmc_state_t *mcs, pxmc_call_t *do_deb, int dbgflg);

int pxmc_dbg_gnr(pxmc_state_t *mcs);

int pxmc_dbg_ene_as(pxmc_state_t *mcs);

#endif /*PXMC_WITH_DBG_HIST*/

/* Hard mome finding */
int pxmc_hh(pxmc_state_t *mcs);

/* Reads sampling frequency of axis */
long pxmc_get_sfi_hz(pxmc_state_t *mcs);

/* Setting sampling frequency of PXMC subsystem */
long pxmc_sfi_sel(long sfi_hz);

/* select operating mode of axis */
int pxmc_axis_mode(pxmc_state_t *mcs, int mode);
#define PXMC_AXIS_MODE_NOCHANGE		0	/* keep previous mode */
#define PXMC_AXIS_MODE_STEPPER		1	/* stepper motor mode */
#define PXMC_AXIS_MODE_STEPPER_WITH_IRC 2	/* stepper with IRC */
#define PXMC_AXIS_MODE_STEPPER_WITH_PWM 3	/* stepper with PWM */
#define PXMC_AXIS_MODE_DC 		4	/* DC motor */
#define PXMC_AXIS_MODE_BLDC 		5	/* BDLC motor */

/* read mode number for axis */
int pxmc_axis_rdmode(pxmc_state_t *mcs);

/* pxmc_go central flags enabling retargeting of the unfinished movement
 * and style of computation of the end position */
extern int pxmc_go_flg;

/* Controlers for main sample frequency */
extern pxmc_state_list_t  pxmc_main_list;

/* initialize pxmc_main_list */
int pxmc_initialize(void);

/* main controller routine called from sampling frequency irq */
void do_pxm_control(void);

/* coordinated movement generator called from sampling frequency irq */
void do_pxmc_coordmv(void);

/* subsystem for queued comparators called from sampling frequency irq */
void do_pxmc_cmpque(void);

int pxmc_clear_power_stop(void);

#endif /* _PXMC_H_ */
