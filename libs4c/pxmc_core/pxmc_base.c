/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_base.c - generic multi axis motion controller
             version supports feedback and feedback-less stepper
	     motor control, DC and brush-less motors

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"

/********************************************************************/
/* controllers general interface */

/**
 * pxmc_set_const_out - Sets direct constant output.[extern API]
 * @mcs:	Motion controller state information
 * @val:	New value of energy/PWM output limited
 *		by @pxms_me.
 */
void pxmc_set_const_out(pxmc_state_t *mcs,int val)
{
  if(mcs->pxms_flg&PXMS_CMV_m){
    /* more inteligence should come here */
    pxmc_clear_flag(mcs,PXMS_CMV_b);
  }
 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_clear_flags(mcs,PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
  pxmc_set_flags(mcs,PXMS_ENO_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_clear_flag(mcs,PXMS_ENG_b);
  pxmc_clear_flag(mcs,PXMS_ENR_b);
  pxmc_clear_flag(mcs,PXMS_BSY_b);
  pxmc_set_flag(mcs,PXMS_ENO_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  if(val>mcs->pxms_me) val=mcs->pxms_me;
  if(val<-mcs->pxms_me) val=-mcs->pxms_me;
  mcs->pxms_ene=val;
 #ifdef PXMC_WITH_EXTENDED_STATE
  mcs->pxms_ene_d=0;
 #endif /* PXMC_WITH_EXTENDED_STATE */
  pxmc_call(mcs,mcs->pxms_do_out);
}


/**
 * pxmc_connect_controller_prep - Preparation of connection of controler to axis.
 * @mcs:	Motion controller state information
 */
int pxmc_connect_controller_prep(pxmc_state_t *mcs)
{
  mcs->pxms_rp=mcs->pxms_ap;
  mcs->pxms_rs=mcs->pxms_foi=mcs->pxms_fod=mcs->pxms_erc=0;
 #ifdef PXMC_WITH_CURRENTFB
  mcs->pxms_curfb_foi=0;
 #endif /*PXMC_WITH_CURRENTFB*/

  return 0;
}

/**
 * pxmc_connect_controller - Smooth connection of controler to axis.[extern API]
 * @mcs:	Motion controller state information
 */
int pxmc_connect_controller(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m) return -1;
  if(mcs->pxms_flg&PXMS_ENR_m) return 0;

  /* smooth connection of controler to axis */
  if(pxmc_connect_controller_prep(mcs)<0)
    return -1;

  __memory_barrier();
 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  return 0;
}

/**
 * pxmc_set_gen_prep - Prepares axis for change of generator.[extern API]
 * @mcs:	Motion controller state information
 */
int pxmc_set_gen_prep(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m)
  	return -1;
  if(mcs->pxms_flg&PXMS_CMV_m)
  	return -1;
  mcs->pxms_do_gen=pxmc_get_cont_gi_4axis(mcs);
  __memory_barrier();

  if(mcs->pxms_flg&PXMS_ERR_m) return -1;
  if(mcs->pxms_flg&PXMS_ENR_m) return 0;

  /* smooth connection of controler to axis */
  return pxmc_connect_controller_prep(mcs);
}

/**
 * pxmc_set_gen_smth - Smooth change to new generator.[extern API]
 * @mcs:	Motion controller state information
 * @do_gen_gi:	Initial state of next generator
 * @flg:	Additional flags to set (%PXMS_BSY_m)
 *
 * Checks non-zero speed and provide smooth stop,
 * than change to requested generator.
 */
int pxmc_set_gen_smth(pxmc_state_t *mcs, pxmc_call_t *do_gen_gi, int flg)
{
  __memory_barrier();
 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m|PXMS_ENG_m|flg);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
  pxmc_set_flag(mcs,PXMS_ENG_b);
  if(flg&PXMS_BSY_m)
    pxmc_set_flag(mcs,PXMS_BSY_b);
  /* more flags may follow */
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/

  if((mcs->pxms_rs==0)||!(mcs->pxms_cfg&PXMS_CFG_SMTH_m)){
    __memory_barrier();
    mcs->pxms_do_gen=do_gen_gi;
  }else{
    mcs->pxms_gen_spd_next=(pxmc_info_t)do_gen_gi;	/* next generator */
    __memory_barrier();
    mcs->pxms_do_gen=pxmc_get_spdnext_gi_4axis(mcs);
  }
  return 0;
}

int pxmc_go_flg=0;	/* pxmc_go central flags */

/**
 * pxmc_go - Starts movement to requested position.[extern API]
 * @mcs:	Motion controller state information
 * @val:	Requested target position
 * @res:	Left for future use
 * @mode:	Motion mode flags.
 *
 * Starts trapezoidal speed profile motion to the position
 * @val.
 */
int pxmc_go(pxmc_state_t *mcs, long val, int res, int mode)
{
  pxmc_call_t *do_gen_init=pxmc_get_trp_gi_4axis(mcs, mode);

  if(!do_gen_init)
    return -1;

  /* Try to re-target to the new final position if previous command is still processed */
  if((mode!=PXMC_GO_REL)&&(pxmc_go_flg&1)&&(mcs->pxms_flg&PXMS_BSY_m)){
    pxmc_call_t *do_trp_retgt=pxmc_get_trp_retgt_4axis(mcs);

    if(do_trp_retgt){
      mcs->pxms_ep=val;
      if(pxmc_call(mcs,do_trp_retgt)>=0)
        return 0;
    }
  }

  if(pxmc_set_gen_prep(mcs)<0) return -1;

  if(mode==PXMC_GO_REL){
    if(pxmc_go_flg&2)
      mcs->pxms_ep=mcs->pxms_ep+val;
    else
      mcs->pxms_ep=mcs->pxms_rp+val;
  }else{
    mcs->pxms_ep=val;
  }

  pxmc_set_gen_smth(mcs,do_gen_init,PXMS_BSY_m);
  return 0;
}

#ifdef PXMC_WITH_FINE_GRAINED
/**
 * pxmc_go_spdfg - Starts movement to position with selected speed.[extern API]
 * @mcs:	Motion controller state information
 * @endpos:	Requested target position
 * @speed:	Fine-grained maximal speed of movement
 * @mode:	Motion mode flags
 *
 * Starts trapezoidal speed profile motion
 * to requested end position with set up fine-grained speed
 */
int pxmc_go_spdfg(pxmc_state_t *mcs, long endpos, long speed, int mode)
{
  pxmc_call_t *do_gen_init=pxmc_get_trp_spdfg_gi_4axis(mcs);

  if(!do_gen_init)
    return -1;

  if(speed<=0)
    return -1;
 #ifndef PXMC_SPDFG_ONLY
  if((speed>>PXMC_SUBDIVFG(mcs))>mcs->pxms_ms)
    return -1;
 #endif
  if(pxmc_set_gen_prep(mcs)<0) return -1;

  if(mode&1){
    mcs->pxms_ep=mcs->pxms_rp+endpos;
  }else{
    mcs->pxms_ep=endpos;
  }
 #ifdef PXMC_SPDFG_ONLY
  mcs->pxms_gen_tsp=0;
 #else
  mcs->pxms_gen_tsp=speed>>PXMC_SUBDIVFG(mcs);
 #endif
  mcs->pxms_gen_tspfg=speed;

  pxmc_set_gen_smth(mcs,do_gen_init,PXMS_BSY_m);
  return 0;
}
#endif /*PXMC_WITH_FINE_GRAINED*/

/**
 * pxmc_stop - Stops motion of axis.[extern API]
 * @mcs:	Motion controller state information
 * @mode:	Value 1 means emergency immediate stop,
 *		else smooth stop is proceeded.
 */
int pxmc_stop(pxmc_state_t *mcs, int mode)
{
  if(mcs->pxms_flg&PXMS_CMV_m){
    /* more inteligence should come here */
    if((mcs->pxms_flg&PXMS_ENR_m)&&!(mcs->pxms_flg&PXMS_ENG_m)
        &&!(mcs->pxms_flg&PXMS_ERR_m)){
      mcs->pxms_do_gen=pxmc_get_cont_gi_4axis(mcs);
      __memory_barrier();
      pxmc_set_flag(mcs,PXMS_ENG_b);
    }
    pxmc_clear_flag(mcs,PXMS_CMV_b);
  }

  if(pxmc_set_gen_prep(mcs)<0){
   #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
    pxmc_clear_flags(mcs,PXMS_ENG_m|PXMS_BSY_m);
   #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    pxmc_clear_flag(mcs,PXMS_ENG_b);
    pxmc_clear_flag(mcs,PXMS_BSY_b);
   #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    mcs->pxms_rs=0;
    return -1;
  }

  if((mcs->pxms_rs==0)||!(mcs->pxms_cfg&PXMS_CFG_SMTH_m)||
      !(mcs->pxms_flg&PXMS_ENR_m)||mode){
   #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
    pxmc_clear_flags(mcs,PXMS_ENG_m|PXMS_BSY_m);
   #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    pxmc_clear_flag(mcs,PXMS_ENG_b);
    pxmc_clear_flag(mcs,PXMS_BSY_b);
   #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    mcs->pxms_rs=0;
  }else{
    mcs->pxms_gen_spd_next=0;	/* no next generator */
    __memory_barrier();
    mcs->pxms_do_gen=pxmc_get_spdnext_gi_4axis(mcs);
  }
  return 0;
}

/**
 * pxmc_spd - Starts constant speed motion.[extern API]
 * @mcs:	Motion controller state information
 * @val:	Requested speed
 * @timeout:	Non zero value results in smooth stop after
 *		@timeout sampling periods
 */
int pxmc_spd(pxmc_state_t *mcs, long val, int timeout)
{
  pxmc_call_t *do_gen_init=pxmc_get_spd_gi_4axis(mcs);

  if(!do_gen_init)
    return -1;

  if(pxmc_set_gen_prep(mcs)<0) return -1;

  mcs->pxms_gen_st=0;
  mcs->pxms_gen_spd_ac=mcs->pxms_ma;	/* acceleration */
  mcs->pxms_gen_spd_sp=val;		/* speed */
 #ifdef PXMC_WITH_FINE_GRAINED
  mcs->pxms_gen_spd_spfg=0;		/* fine grained speed */
 #endif /*PXMC_WITH_FINE_GRAINED*/
  mcs->pxms_gen_spd_timeout=timeout;	/* timeout */

 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_BSY_b);
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
  pxmc_set_flag(mcs,PXMS_ENG_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  __memory_barrier();

  mcs->pxms_do_gen=do_gen_init;

  return 0;
}

#ifdef PXMC_WITH_FINE_GRAINED
/**
 * pxmc_spdfg - Starts constant fine-grained speed motion.[extern API]
 * @mcs:	Motion controller state information
 * @val:	Requested fine-grained speed
 * @timeout:	Non zero value results in smooth stop after
 *		@timeout sampling periods
 */
int pxmc_spdfg(pxmc_state_t *mcs, long val, int timeout)
{
  pxmc_call_t *do_gen_init=pxmc_get_spdfg_gi_4axis(mcs);

  if(!do_gen_init)
    return -1;

  if(pxmc_set_gen_prep(mcs)<0) return -1;

  mcs->pxms_gen_st=0;
  mcs->pxms_gen_spd_ac=mcs->pxms_ma;	/* acceleration */
  mcs->pxms_gen_spd_spfg=val;		/* fine grained speed */
 #ifndef PXMC_SPDFG_ONLY
  mcs->pxms_gen_spd_sp=val>>PXMC_SUBDIVFG(mcs); /* speed */
 #else
  mcs->pxms_rs=val<0? -1: 0;
 #endif

  mcs->pxms_gen_spd_timeout=timeout;	/* timeout */

 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_BSY_b);
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
  pxmc_set_flag(mcs,PXMS_ENG_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  __memory_barrier();

  mcs->pxms_do_gen=do_gen_init;

  return 0;
}
#endif /*PXMC_WITH_FINE_GRAINED*/

/**
 * pxmc_axis_set_pos - Set axis actual position
 * @mcs:        Motion controller state information
 * @pos:        New forced position for IRC counter
 *              in the standard subdiv. format
 */
int pxmc_axis_set_pos(pxmc_state_t *mcs, long pos)
{
  int old;

  if(mcs->pxms_flg&(PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m|PXMS_CMV_m)){
    /* Cannot set new position value when busy */
    return -1;
  }
  if(!mcs->pxms_do_ap2hw){
    return -1;
  }
  old=mcs->pxms_flg&PXMS_ENI_m;
  pxmc_clear_flag(mcs,PXMS_ENI_b);
  mcs->pxms_ap=pos;
  mcs->pxms_rp=pos;
  pxmc_call(mcs, mcs->pxms_do_ap2hw);
  if(old)
    pxmc_set_flag(mcs,PXMS_ENI_b);
  return 0;
}

/**
 * pxmc_axis_release - Release control of given axis.[extern API]
 * @mcs:	Motion controller state information
 */
void pxmc_axis_release(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_CMV_m){
    /* more inteligence should come here */
    pxmc_clear_flag(mcs,PXMS_CMV_b);
  }
 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_clear_flags(mcs,PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m|PXMS_ENO_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_clear_flag(mcs,PXMS_ENG_b);
  pxmc_clear_flag(mcs,PXMS_ENR_b);
  pxmc_clear_flag(mcs,PXMS_BSY_b);
  pxmc_clear_flag(mcs,PXMS_ENO_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  __memory_barrier();
  mcs->pxms_ene=0;
 #ifdef PXMC_WITH_EXTENDED_STATE
  mcs->pxms_ene_d=0;
 #endif /* PXMC_WITH_EXTENDED_STATE */
  pxmc_call(mcs,mcs->pxms_do_out);
}
