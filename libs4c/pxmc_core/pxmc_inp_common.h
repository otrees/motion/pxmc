/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_inp_common.h - generic multi axis motion controller
                      common reusable routines for position input

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_INP_COMMON_H_
#define _PXMC_INP_COMMON_H_

#include "pxmc.h"

/* update PXMC state according 16 bit IRC counter value */
static inline void
pxmc_irc_16bit_update(pxmc_state_t *mcs, short irc)
{
  long pos;
  long spd;

  long val_mag=1l<<(PXMC_SUBDIV(mcs)+16);
  pos=(long)(unsigned short)irc<<PXMC_SUBDIV(mcs);
  pos|= mcs->pxms_ap &~(val_mag-1);
  spd=pos-mcs->pxms_ap;
  if(spd<=-(val_mag>>1)) {pos+=val_mag;spd+=val_mag;}
  else if(spd>=(val_mag>>1)) {pos-=val_mag;spd-=val_mag;}

  mcs->pxms_ap=pos;
  mcs->pxms_as=spd;
}

#ifdef PXMC_WITH_PHASE_TABLE
static inline void
pxmc_irc_16bit_commindx(struct pxmc_state *mcs, short irc)
{
  short ofs;
  irc+=mcs->pxms_ptshift;
  ofs=irc-mcs->pxms_ptofs;
  if((unsigned short)ofs>=(unsigned short)mcs->pxms_ptirc)
  {
    if(ofs>0) {
      mcs->pxms_ptofs+=mcs->pxms_ptirc;
    } else {
      mcs->pxms_ptofs-=mcs->pxms_ptirc;
    }
    ofs=irc-mcs->pxms_ptofs;
    if((unsigned short)ofs>=(unsigned short)mcs->pxms_ptirc) {
      mcs->pxms_ptindx=0;
      pxmc_clear_flag(mcs,PXMS_PHA_b);
      pxmc_set_errno(mcs,PXMS_E_COMM);
      return;
    }
  }
  mcs->pxms_ptindx=ofs;
}
#else /*PXMC_WITH_PHASE_TABLE*/
static inline void
pxmc_irc_16bit_commindx(struct pxmc_state *mcs, short irc){}
#endif /*PXMC_WITH_PHASE_TABLE*/

#endif /*_PXMC_INP_COMMON_H_*/
