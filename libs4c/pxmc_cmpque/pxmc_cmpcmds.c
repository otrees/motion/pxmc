/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_cmpcmds.c - generic multi axis motion controller interface
                   commands for queues of request for comparator

  (C) 2001-2007 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2007 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/


#include <cpu_def.h>
#include <system_def.h>
#include <cmd_proc.h>
#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include <utils.h>
#include <stdlib.h>

#include <pxmc.h>
#include <pxmc_cmpque.h>

#include <gen_dio.h>

cmd_io_t *cmpque_last_used_cmd_io;

typedef struct cmpque_dio_entry {
  pxmc_cmpque_entry_t base;
  short flags;
  short regnum;
  long repoffs;
  long private;
  long dionum;
  long diomask;
  long dioxor;
  long dioin;
  struct cmd_io *cmd_io;
  ul_list_node_t group_node;
} cmpque_dio_entry_t;

void cmpque_dio_send(cmpque_dio_entry_t *cmp);

void cmpque_dio_destroy(cmpque_dio_entry_t *cmp)
{
  if(pxmc_cmpque_entry_is_queued(&cmp->base)) {
    pxmc_cmpque_delete(cmp->base.queue, &cmp->base);
  }

  if(cmp->group_node.next != NULL) {
    list_del(&cmp->group_node);
  }
  free(cmp);
}

int cmpque_dio_proc(pxmc_cmpque_entry_t *entry, int code)
{
  cmpque_dio_entry_t *cmp = UL_CONTAINEROF(entry, cmpque_dio_entry_t, base);

  if(code & PXMC_CMPQUE_CMP_m) {
    if(cmp->flags&DIO_CMP_SETDIO)
      dio_mod(&dio_hw_des[cmp->dionum],cmp->diomask,cmp->dioxor);

  if(cmp->flags&DIO_CMP_GETDIO)
     cmp->dioin=dio_get(&dio_hw_des[cmp->dionum]);

  /*cmp->base.cmp_pos+=cmp->repoffs;*/

    return 0;
  }

  if(code & PXMC_CMPQUE_DONE_m) {

    switch (code & PXMC_CMPQUE_DONE_m) {
      case PXMC_CMPQUE_DONE_NORMAL:
        cmpque_dio_send(cmp);
	break;
    }
    cmpque_dio_destroy(cmp);
  }
  return 0;
}

void cmpque_dio_send(cmpque_dio_entry_t *cmp)
{
  char s[20];
  struct cmd_io *cmd_io = cmp->cmd_io;
  long pos;

  if(!(cmp->flags&DIO_CMP_SILENT) && (cmd_io != NULL)) {
    cmd_io_write(cmd_io,"CMPQUE",6);
    cmd_io_putc(cmd_io,'A'+cmp->regnum);
    cmd_io_putc(cmd_io,'!');
    pos = cmp->base.cmp_pos;
    pos >>= cmp->base.queue->mcs->pxms_subdiv;
    i2str(s,pos,0,0);
    cmd_io_write(cmd_io,s,strlen(s));
    cmd_io_putc(cmd_io,',');
    if(cmp->flags&DIO_CMP_GETDIO){
      i2str(s,cmp->dioin,0,0);
      cmd_io_write(cmd_io,s,strlen(s));
    }else{
      cmd_io_putc(cmd_io,'N');
    }
  }
}

void cmpque_dio_cmd_io_ceased(struct cmd_io *cmd_io)
{
  int var;
  pxmc_state_t *mcs;

  pxmc_for_each_mcs(var, mcs) {
    pxmc_cmpque_queue_t *queue;
    pxmc_cmpque_entry_t *entry;

    queue = pxmc_cmpque4mcs(mcs, 0);
    if (queue == NULL)
      continue;

    entry = pxmc_cmpque_entry_first(queue);
    while (entry != NULL) {
      if (entry->fnc == cmpque_dio_proc) {
      cmpque_dio_entry_t *cmp = UL_CONTAINEROF(entry, cmpque_dio_entry_t, base);
        if (cmp->cmd_io == cmd_io)
          cmp->cmd_io = NULL;
      }
      entry = pxmc_cmpque_entry_next(queue, entry);
    }
  }
}

/*******************************************************************/

pxmc_cmpque_queue_t *
cmd_opchar_cmpque(cmd_io_t *cmd_io, const struct cmd_des *des,
                                      char *param[], unsigned *pregnum)
{
  unsigned chan;
  pxmc_state_t *mcs;
  pxmc_cmpque_queue_t *queue;
  chan=*param[1]-'A';
  if(chan>=pxmc_main_list.pxml_cnt) return NULL;
  mcs=pxmc_main_list.pxml_arr[chan];
  if(!mcs) return NULL;
  if(pregnum != NULL)
    *pregnum = chan;
  queue = pxmc_cmpque4mcs(mcs, 1);
  return queue;
}

int cmd_do_cmpque_set(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  cmpque_dio_entry_t *cmp;
  char *p;
  int opchar;
  unsigned regnum;
  long flags,pos,repoffs,dionum,diomask,dioxor;
  pxmc_cmpque_queue_t *queue;
  int res;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((queue=cmd_opchar_cmpque(cmd_io,des,param,&regnum))==NULL) return -CMDERR_BADREG;

  switch(opchar){
    case ':':
      p=param[3];
      if(si_long(&p,&flags,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p);
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      si_skspace(&p);
      if(si_long(&p,&pos,0)<0) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      si_skspace(&p);
      if(si_long(&p,&repoffs,0)<0) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      if(si_long(&p,&dionum,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p); if(!*p){
        dioxor=dionum;
	diomask=~0l;
        dionum=0;
      } else {
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&diomask,0)<0) return -CMDERR_BADPAR;
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&dioxor,0)<0) return -CMDERR_BADPAR;
	si_skspace(&p); if(*p) return -CMDERR_GARBAG;
      }

      cmp = malloc(sizeof(*cmp));
      if(!cmp) return -CMDERR_NOMEM;
      memset(cmp, 0, sizeof(*cmp));

      cmp->flags=flags|DIO_CMP_GETDIO;
      cmp->regnum=regnum;
      cmp->base.cmp_pos=pos<<queue->mcs->pxms_subdiv;
      cmp->repoffs=repoffs<<queue->mcs->pxms_subdiv;
      cmp->dionum=dionum;
      cmp->diomask=diomask;
      cmp->dioxor=dioxor;
      cmp->cmd_io = cmd_io;
      cmp->base.fnc = cmpque_dio_proc;


      if(flags & DIO_CMP_GT)
        cmp->base.cmp_req |= PXMC_CMPQUE_CMP_GT;
      if(flags & DIO_CMP_LT)
        cmp->base.cmp_req |= PXMC_CMPQUE_CMP_LT;
      if(!(flags & DIO_CMP_REPEAT) || (repoffs != 0))
        cmp->base.cmp_req |= PXMC_CMPQUE_CMP_SINGLE;
      if(flags & DIO_CMP_OCC)
        cmp->base.cmp_req |= PXMC_CMPQUE_CMP_NOMISS;

      res = pxmc_cmpque_insert(queue, &cmp->base);
      if(res<0) {
        cmpque_dio_destroy(cmp);
	return -CMDERR_NODEV;
      }

      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_cmpque_list(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  pxmc_cmpque_entry_t *entry;
  pxmc_cmpque_queue_t *queue;
  unsigned regnum;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((queue=cmd_opchar_cmpque(cmd_io,des,param,&regnum))==NULL) return -CMDERR_BADREG;

  switch(opchar){
    case ':':
      printf("list of cmpque %c\n", 'A'+regnum);
      gavl_cust_for_each(pxmc_cmpque_entry, queue, entry) {
        printf("cmp pos %ld cmp 0x%02x/0x%02x done 0x%02x %c\n",
	  entry->cmp_pos >> queue->mcs->pxms_subdiv,
	  entry->cmp_code, entry->cmp_req, entry->done_code,
	  (entry->linear_queue_node.next==&queue->linear_queue_carrier)?'v':
	  (entry->linear_queue_node.prev==&queue->linear_queue_carrier)?'^':'.');
      }
      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_cmpque_flush(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  pxmc_cmpque_queue_t *queue;
  pxmc_cmpque_entry_t *entry;
  int done_code;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((queue=cmd_opchar_cmpque(cmd_io,des,param,NULL))==NULL) return -CMDERR_BADREG;

  switch(opchar){
    case ':':
      do {
        entry = pxmc_cmpque_entry_first(queue);
	if(!entry)
	  break;
        pxmc_cmpque_delete(queue, entry);
	done_code = entry->done_code;
        if(!done_code)
          done_code = PXMC_CMPQUE_DONE_UNKNOWN;
        entry->fnc(entry, done_code);
      } while(1);
      return 0;
  }
  return -CMDERR_OPCHAR;
}

/*******************************************************************/

#define CMPQUE_FROMTRIG_PARAMS_DIO_MAX 4

typedef struct cmpque_fromtrig_params_dio_slot {
  long posoffs;
  long dionum;
  long diomask;
  long dioxor;
  short flags;
} cmpque_fromtrig_params_dio_slot_t;

typedef struct cmpque_fromtrig_params {
  short flags;
  short regnum;
  struct cmd_io *cmd_io;
  pxmc_cmpque_queue_t *queue;
  ul_list_node_t global_node;
  ul_list_head_t entry_group_list;
  dio_trig_notify_t trig_notify;
  long private;
  long distance_min;
  long distance_noise;
  long last_base_pos;
  long occ_cnt;
  int  chkseq_act;
  int  dio_slot_cnt;
  cmpque_fromtrig_params_dio_slot_t dio_slot[CMPQUE_FROMTRIG_PARAMS_DIO_MAX];
} cmpque_fromtrig_params_t;

typedef struct cmpque_fromtrig_global {
  ul_list_head_t global_list;
} cmpque_fromtrig_global_t;

char *cmpque_fromtrig_chkseq_data;
int  cmpque_fromtrig_chkseq_size;

UL_LIST_CUST_DEC(cmpque_global_list,  cmpque_fromtrig_global_t, cmpque_fromtrig_params_t,
		 global_list, global_node)

UL_LIST_CUST_DEC(cmpque_dio_entry_group, cmpque_fromtrig_params_t, cmpque_dio_entry_t,
		 entry_group_list, group_node)

cmpque_fromtrig_global_t cmpque_fromtrig_global;

void cmpque_fromtrig_callback(struct dio_trig_notify *notify, struct dio_trig *trig)
{
  cmpque_fromtrig_params_t *fromtrig =
		UL_CONTAINEROF(notify, cmpque_fromtrig_params_t, trig_notify);
  int res;
  cmpque_dio_entry_t *cmp;
  cmpque_dio_entry_t *cmp_arr[CMPQUE_FROMTRIG_PARAMS_DIO_MAX];
  pxmc_cmpque_queue_t *queue;
  long base_pos;
  long distance;
  int cnt = fromtrig->dio_slot_cnt;
  int i;
  int chkseq_bit = 0;

  base_pos = trig->motpos[fromtrig->regnum];
  if(!fromtrig->occ_cnt) {
    distance = 0;
  } else {
    int chkseq_dec_fl=0;
    distance = labs(base_pos - fromtrig->last_base_pos);
    if(fromtrig->distance_noise && (distance < fromtrig->distance_noise)) {
      if(!fromtrig->distance_min)
        fromtrig->last_base_pos = base_pos;
      return;
    }
    if(fromtrig->distance_min && (distance < fromtrig->distance_min)) {
      fromtrig->last_base_pos = base_pos;
      if(fromtrig->flags & DIO_CMP_REMOVE_NEAR) {
        while((cmp = cmpque_dio_entry_group_cut_first(fromtrig)) != NULL) {
	  cmpque_dio_destroy(cmp);
	  chkseq_dec_fl=1;
	}
      }
      if(chkseq_dec_fl)
        if(fromtrig->chkseq_act-- == 0)
          fromtrig->chkseq_act = cmpque_fromtrig_chkseq_size-1;
      return;
    }
  }

  list_del_init(&fromtrig->entry_group_list);

  fromtrig->occ_cnt++;
  if(!fromtrig->occ_cnt)
    fromtrig->occ_cnt++;
  fromtrig->last_base_pos = base_pos;

  for(i=0; i<cnt; i++) {
    cmp_arr[i] = malloc(sizeof(*cmp_arr[i]));
    if(!cmp_arr[i]) {
      while(i--)
        free(cmp_arr[i]);
      return;
    }
    memset(cmp_arr[i], 0, sizeof(*cmp_arr[i]));
  }

  queue = fromtrig->queue;

  if(cmpque_fromtrig_chkseq_data != NULL) {
    if(fromtrig->chkseq_act >= cmpque_fromtrig_chkseq_size)
      fromtrig->chkseq_act = 0;
    chkseq_bit = cmpque_fromtrig_chkseq_data[fromtrig->chkseq_act];
  }

  for(i=0; i<cnt; i++) {
    short flags;

    cmp = cmp_arr[i];
    cmp->cmd_io = fromtrig->cmd_io;
    cmp->base.fnc = cmpque_dio_proc;

    if(fromtrig->flags & DIO_CMP_INDIV_FLAGS)
      flags = fromtrig->dio_slot[i].flags;
    else
      flags = fromtrig->flags;

    cmp->flags = flags;
    cmp->regnum = fromtrig->regnum;
    cmp->repoffs = 0;
    cmp->base.cmp_pos = fromtrig->dio_slot[i].posoffs+base_pos;
    cmp->dionum = fromtrig->dio_slot[i].dionum;
    cmp->diomask = fromtrig->dio_slot[i].diomask;
    cmp->dioxor = fromtrig->dio_slot[i].dioxor;

    if((flags & DIO_CMP_CHKSEQ_OUT) && !chkseq_bit)
      cmp->dioxor = 0;

    cmp->base.cmp_req |= PXMC_CMPQUE_CMP_SINGLE;
    if(flags & DIO_CMP_GT)
      cmp->base.cmp_req |= PXMC_CMPQUE_CMP_GT;
    if(flags & DIO_CMP_LT)
      cmp->base.cmp_req |= PXMC_CMPQUE_CMP_LT;
    if(flags & DIO_CMP_OCC)
      cmp->base.cmp_req |= PXMC_CMPQUE_CMP_NOMISS;

    cmpque_dio_entry_group_insert(fromtrig, cmp);

    res = pxmc_cmpque_insert(queue, &cmp->base);
    if(res<0)
      goto insert_error;
  }

  if(++fromtrig->chkseq_act >= cmpque_fromtrig_chkseq_size)
    fromtrig->chkseq_act = 0;

  return;

insert_error:
  for(i=0; i<cnt; i++) {
    cmpque_dio_destroy(cmp_arr[i]);
  }
}

cmpque_fromtrig_params_t *cmpque_fromtrig_params_new(void)
{
  cmpque_fromtrig_params_t *fromtrig;

  fromtrig = malloc(sizeof(*fromtrig));
  if(fromtrig == NULL)
    return NULL;
  memset(fromtrig,0,sizeof(*fromtrig));

  cmpque_global_list_init_detached(fromtrig);
  cmpque_dio_entry_group_init_head(fromtrig);
  dio_trig_notify_init_detached(&fromtrig->trig_notify);
  fromtrig->trig_notify.fnc = cmpque_fromtrig_callback;

  return fromtrig;
}

void cmpque_fromtrig_params_destroy(cmpque_fromtrig_params_t *fromtrig)
{
  list_del_init(&fromtrig->entry_group_list);
  cmpque_global_list_del_item(fromtrig);
  dio_trig_notify_del_item(&fromtrig->trig_notify);
  free(fromtrig);
}

void cmpque_fromtrig_cmd_io_ceased(struct cmd_io *cmd_io)
{
   cmpque_fromtrig_params_t *fromtrig;

   if(cmpque_fromtrig_global.global_list.next == NULL)
     return;

   fromtrig = cmpque_global_list_first(&cmpque_fromtrig_global);
   while (fromtrig != NULL) {
     if (fromtrig->cmd_io == cmd_io)
       fromtrig->cmd_io = NULL;
     fromtrig = cmpque_global_list_next(&cmpque_fromtrig_global, fromtrig);
   }
}

int cmd_do_cmpque_fromtrig(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  cmpque_fromtrig_params_t *fromtrig;
  dio_trig_t *trig;
  char *p;
  int opchar;
  unsigned regnum;
  long l, trign;
  long posoffs = 0;
  int res = 0;
  int dio_cnt = 0;
  pxmc_cmpque_queue_t *queue;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((queue=cmd_opchar_cmpque(cmd_io,des,param,&regnum))==NULL) return -CMDERR_BADREG;

  switch(opchar){
    case ':':
      p=param[3];
      /* f,t,dm{,[f,]o,n,mask,xor}... */
      fromtrig = cmpque_fromtrig_params_new();
      if(!fromtrig) return -CMDERR_NOMEM;
      do {
	if(regnum>=DIO_TRIG_MOTNUM) { res = -CMDERR_BADREG; goto error_fromtrig; }
	fromtrig->regnum=regnum;

	if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	fromtrig->flags = l;

	if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	if(si_long(&p,&trign,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	if(trign>=DIO_TRIG_COUNT) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	trig=&dio_trig[trign];

	if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	fromtrig->distance_min = l  << queue->mcs->pxms_subdiv;

        if(fromtrig->flags&DIO_CMP_SKIP_NOISE) {
          if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
          if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
          fromtrig->distance_noise = l  << queue->mcs->pxms_subdiv;
        }

        do {
	  si_skspace(&p);
	  if(!*p) {
	    if(dio_cnt)
	      break;
	    else
	      { res = -CMDERR_BADSEP; goto error_fromtrig; }
	  }
	  if(dio_cnt >= CMPQUE_FROMTRIG_PARAMS_DIO_MAX)
	    { res = -CMDERR_BADPAR; goto error_fromtrig; }

	  if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }

	  if(fromtrig->flags & DIO_CMP_INDIV_FLAGS) {
	    if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	    fromtrig->dio_slot[dio_cnt].flags = l;
	    if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	  } else {
	    fromtrig->dio_slot[dio_cnt].flags = fromtrig->flags;
	  }

	  if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	  posoffs += l << queue->mcs->pxms_subdiv;
	  fromtrig->dio_slot[dio_cnt].posoffs = posoffs;

	  si_skspace(&p);
	  if(!*p) {
	    if(dio_cnt == 0)
	      { res = -CMDERR_BADPAR; goto error_fromtrig; }
	    fromtrig->dio_slot[dio_cnt].dionum = fromtrig->dio_slot[dio_cnt-1].dionum;
	    fromtrig->dio_slot[dio_cnt].diomask = fromtrig->dio_slot[dio_cnt-1].diomask;
	    fromtrig->dio_slot[dio_cnt].dioxor = fromtrig->dio_slot[dio_cnt-1].dioxor;
	    fromtrig->dio_slot[dio_cnt].dioxor ^= fromtrig->dio_slot[dio_cnt-1].diomask;
	    dio_cnt++;
	    break;
	  }

	  if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	  if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	  if(l>=DIO_COUNT) { res = -CMDERR_BADDIO; goto error_fromtrig; }
	  fromtrig->dio_slot[dio_cnt].dionum = l;

	  if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	  if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	  fromtrig->dio_slot[dio_cnt].diomask = l;

	  if(si_fndsep(&p,",")<0) { res = -CMDERR_BADSEP; goto error_fromtrig; }
	  if(si_long(&p,&l,0)<0) { res = -CMDERR_BADPAR; goto error_fromtrig; }
	  fromtrig->dio_slot[dio_cnt].dioxor = l;

	  dio_cnt++;

        } while(1);

	fromtrig->dio_slot_cnt = dio_cnt;

	fromtrig->cmd_io=cmd_io;
	fromtrig->queue=queue;

	trig->motmask|=1<<(regnum+8);
	dio_trig_notify_insert(trig, &fromtrig->trig_notify);

        if(cmpque_fromtrig_global.global_list.next == NULL)
	  cmpque_global_list_init_head(&cmpque_fromtrig_global);

        cmpque_global_list_insert(&cmpque_fromtrig_global, fromtrig);

	return 0;
      } while(0);
    error_fromtrig:
      free(fromtrig);
      return res;
  }
  return -CMDERR_OPCHAR;
}


int cmd_do_cmpque_fromtrig_flush(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  cmpque_fromtrig_params_t *fromtrig;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;

  switch(opchar){
    case ':':
      if(cmpque_fromtrig_global.global_list.next != NULL) {
        while((fromtrig = cmpque_global_list_cut_first(&cmpque_fromtrig_global)) != NULL) {
	  cmpque_fromtrig_params_destroy(fromtrig);
        }
      }
      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_cmpque_fromtrig_chkseq(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  char *p, *r, *s;

  cmpque_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;

  switch(opchar){
    case ':':
      p=param[3];
      si_skspace(&p);
      if(!*p)
	return -CMDERR_BADPAR;
      r = p;
      while((*r == '0') || (*r == '1'))
        r++;

      if(r == p)
      	return -CMDERR_BADPAR;

      s = r;
      si_skspace(&s);
      if(*s)
	return -CMDERR_GARBAG;

      if(cmpque_fromtrig_chkseq_data)
        free(cmpque_fromtrig_chkseq_data);

      cmpque_fromtrig_chkseq_size = r-p;
      cmpque_fromtrig_chkseq_data=malloc(cmpque_fromtrig_chkseq_size);

      if(!cmpque_fromtrig_chkseq_data)
        return -CMDERR_NOMEM;

      s = cmpque_fromtrig_chkseq_data;
      while(p < r)
        *(s++) = *(p++) == '1'? 1: 0;

      return 0;
  }
  return -CMDERR_OPCHAR;
}

cmd_des_t const cmd_des_cmpque_set={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUE?","comparator f,p,r,{do|n,mask,xor}",cmd_do_cmpque_set,
			{NULL,
			 0}};

cmd_des_t const cmd_des_cmpque_list={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUELIST?","list contents of given queue",cmd_do_cmpque_list,
			{NULL,
			 0}};

cmd_des_t const cmd_des_cmpque_flush={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUEFLUSH?","flush contents of given queue",cmd_do_cmpque_flush,
			{NULL,
			 0}};

cmd_des_t const cmd_des_cmpque_fromtrig={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUEFROMTRIG?","fill from trig f,t,dm{,o,n,mask,xor}...",
			cmd_do_cmpque_fromtrig,
			{NULL,
			 0}};

cmd_des_t const cmd_des_cmpque_fromtrig_flush={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUEFROMTRIGFLUSH","flush all from trig connections",
			cmd_do_cmpque_fromtrig_flush,
			{NULL,
			 0}};

cmd_des_t const cmd_des_cmpque_fromtrig_chkseq={0, CDESM_OPCHR|CDESM_WR,
			"CMPQUEFROMTRIGCHKSEQ","set control bit sequence for from trig connections",
			cmd_do_cmpque_fromtrig_chkseq,
			{NULL,
			 0}};


cmd_des_t const *const cmd_cmpque_tab[]={
  &cmd_des_cmpque_set,
  &cmd_des_cmpque_list,
  &cmd_des_cmpque_flush,
  &cmd_des_cmpque_fromtrig,
  &cmd_des_cmpque_fromtrig_flush,
  &cmd_des_cmpque_fromtrig_chkseq,
  NULL
};
