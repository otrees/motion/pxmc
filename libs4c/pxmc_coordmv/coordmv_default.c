/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  coordmv_default.c - coordinated multiple axes movements
                      declaration of the default coordinator state

  Copyright (C) 2001-2003 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include "pxmc_coordmv_internal.h"
#include <string.h>

/********************************************************************/
/* PXMC coordmv default/system wide state */

/* structure of main coordinator */
pxmc_coordmv_state_t pxmc_coordmv_state={
  mcs_flg:0,
 #ifndef PXMC_WITH_FLAGS_LONG_TYPE
  mcs_res1:0,
 #endif /*PXMC_WITH_FLAGS_LONG_TYPE*/
  mcs_par:0,
  mcs_rs:0,
  mcs_es:0,
  mcs_ms:0,
  mcs_ma:0,
  mcs_disca:1,
  mcs_con_list:&pxmc_main_list,
  mcs_con_msk:0,
  mcs_con_cnt:0,
  mcs_seg_max:200,
  mcs_seg_warn:50,
  mcs_seg_cnt:0,
  mcs_seg_head:NULL,
  mcs_seg_tail:NULL,
  mcs_seg_inpr:NULL,
};


/* this function is called from controllers isr */
void do_pxmc_coordmv(void)
{
  int ret;
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  if(!(mcs_state->mcs_flg&MCS_INPR_m)) return;
  do{
    if(mcs_state->mcs_seg_inpr)
      if(pxmc_coordmv_upda(mcs_state))
        break;
    if(!pxmc_coordmv_seg_next(mcs_state)){
      pxmc_cmvs_clear_flag_bit(mcs_state,MCS_INPR_b);
      pxmc_cmvs_set_flag_bit(mcs_state,MCS_BOTT_b);
    }
  }while(0);
  ret=mcs_state->mcs_gen(mcs_state, mcs_state->mcs_seg_inpr);
  if(ret<=0){
    if(ret<0)
      pxmc_cmvs_set_flag_bit(mcs_state,MCS_ERR_b);
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_STOP_b);
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_BOTT_b);
  };
}

