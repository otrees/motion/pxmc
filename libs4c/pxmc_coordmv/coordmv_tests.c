/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  coordmv_tests.c - coordinated multiple axes movements
                    routines for testing functionality of subsystem

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include "pxmc_coordmv_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <utils.h>

int pxmc_coordmv_print(pxmc_coordmv_state_t *mcs_state)
{
  pxmc_state_t *mcs;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      printf(" %8ld",mcs->pxms_rp);
    }
  }
  printf("\n");
  return 0;
}

long int pxmc_coordmv_path[5][4]={
  {100*1000l,200*1000l,400*1000l,500*1000l},
  {800*1000l,800*1000l,800*1000l,800*1000l},
  {800*1000l,700*1000l,600*1000l,700*1000l},
  {800*1000l,620*1000l,420*1000l,620*1000l},
  {000*1000l,000*1000l,000*1000l,000*1000l}
};

int pxmc_coordmv_path_regs =sizeof(pxmc_coordmv_path[0])/sizeof(pxmc_coordmv_path[0][0]);
int pxmc_coordmv_path_points=sizeof(pxmc_coordmv_path)/sizeof(pxmc_coordmv_path[0]);


int pxmc_coordmv_tst(char *s)
{
  int point;
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  pxmc_coordmv_seg_t *mcseg;
  int fl_p=0;
  int fl_d=1;
  if(s&&((s[0]=='p')||(s[0]=='t'))){
    fl_d=0;
    fl_p=1;
  }
  if(s&&(s[0]=='c')){	/* clear state for emulated run */
    pxmc_state_t *mcs;
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	pxmc_cmvs_clear_flag_bit(mcs_state,MCS_INPR_b);
	pxmc_axis_release(mcs);
	mcs->pxms_ep=0;
	pxmc_set_flag(mcs,PXMS_CMV_b);
      }
    }
    return 0;
  }
  if(s&&(s[0]=='a')){	/* add segment without real run */
    long final[MCS_LOCARR_CNT(mcs_state->mcs_con_cnt)], val;
    int mintim=0;
    int i=0;
    s++;
    if(s[0]=='t'){
      s++;
      if(si_long(&s,&val,10)<0) return -1;
      mintim=val;
      if(si_fndsep(&s,",")<=0) return -1;
    }
    while(1){
      if(si_long(&s,&val,10)<0) return -1;
      final[i]=val;
      if(++i>=mcs_state->mcs_con_cnt) break;
      if(si_fndsep(&s,",")<=0) return -1;
    }
    si_skspace(&s);if(*s) return -1;
    pxmc_coordmv_seg_add_line(mcs_state,mcs_state->mcs_con_cnt,final,0,mintim);
    pxmc_coordmv_optimize(mcs_state);
    return 0;
  }
  pxmc_coordmv_seg_clean(mcs_state,0);
  if(fl_d) fprintf(stderr,"Pass 1 - Edge limit speed computation\n");
  if(!s||((s[0]!='t')&&(s[0]!='u')))
  for(point=0;point<pxmc_coordmv_path_points;point++){
    pxmc_coordmv_seg_add_line(mcs_state,pxmc_coordmv_path_regs,pxmc_coordmv_path[point],0,0);
  }
  if(fl_d) fprintf(stderr,"Pass 2 - Edge and break computation\n");
  pxmc_coordmv_optimize(mcs_state);
  if(fl_d) {
    fprintf(stderr,"Pass 3 - Approximation\n");
    fprintf(stderr,"        rs       es     sels     eels       ms\n");
  }
  if(s&&(s[0]=='r')){
    if(fl_d) fprintf(stderr,"Starting real run\n");
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_INPR_b);
    return 0;
  }

  while(pxmc_coordmv_seg_next(mcs_state)){
    mcseg=mcs_state->mcs_seg_inpr;
    if(fl_d) {
      fprintf(stderr,"  %8lu %8lu %8lu %8lu %8lu\n",mcs_state->mcs_rs,mcs_state->mcs_es,
			mcseg->mcseg_sels,mcseg->mcseg_eels,mcseg->mcseg_ms);
    }
    mcs_state->mcs_gen(mcs_state, mcs_state->mcs_seg_inpr);
    if(fl_p) {
      printf("  %lu %lu %lu %lu",
		mcs_state->mcs_par,mcs_state->mcs_es,mcs_state->mcs_ms,mcs_state->mcs_ma);
      pxmc_coordmv_print(mcs_state);
    }
    while(pxmc_coordmv_upda(mcs_state)){
      mcs_state->mcs_gen(mcs_state, mcs_state->mcs_seg_inpr);
      if(fl_p) {
        printf("  %lu %lu %lu %lu",
		mcs_state->mcs_par,mcs_state->mcs_es,mcs_state->mcs_ms,mcs_state->mcs_ma);
        pxmc_coordmv_print(mcs_state);
      }
    }
  }
  if(fl_p)  {
    printf("  %lu %lu %lu %lu",
	mcs_state->mcs_par,mcs_state->mcs_es,mcs_state->mcs_ms,mcs_state->mcs_ma);
    pxmc_coordmv_print(mcs_state);
  }
  return 0;
}

/*
(MARS8)
REGACCA:20
REGACCB:20
REGACCC:20
REGACCD:20
REGMSA:2000
REGMSB:2000
REGMSC:2000
REGMSD:2000
coordtst

REGACCC:20
REGACCD:20
REGACCE:20
REGACCF:20
REGMSC:2000
REGMSD:2000
REGMSE:2000
REGMSF:2000
coordtst

REGACCC:20
REGACCD:20
REGACCE:20
REGACCF:20
REGMSC:1000
REGMSD:1000
REGMSE:1000
REGMSF:1000
coordtst r

COORDGRP:C,D,E,F
COORDMV:1000,10000,10000,10000
COORDMV:1000,-10000,10000,10000
COORDMV:1000,-10000,-10000,-10000
COORDMV:1000,10000,-10000,-10000
COORDMV:-1000,10000,-10000,-10000
COORDMV:0,0,0,0

HHA:
HHB:
HHC:
HHD:
HHE:
HHF:
ST?

transport position
GA:0
GB:0
GC:-15000
GD:17000
GE:-67000
GF:0

test of one axis
REGACCA:5
REGMSA:1000
COORDGRP:A
COORDMV:1000
COORDMV:2000
COORDMV:1000
COORDMV:0

test of timed movement
COORDDISCONT:0
COORDMVT:10000,1000
COORDMVT:5000,2000
COORDMVT:10000,1000
COORDMVT:5000,0
COORDDISCONT:1

test of two axes
REGACCA:2
REGMSA:1000
REGACCB:2
REGMSB:1000
COORDGRP:A,B
COORDMV:1000,1500
COORDMV:2000,2900
COORDMV:1000,1500
COORDMV:0,0

coordtst c
coordtst a 256000
coordtst a 512000
coordtst a 256000
coordtst a 256000
coordtst a 0
coordtst t

(GDB)
def coordsegs
  p $p=pxmc_coordmv_state->mcs_seg_head
  while $p
    p *$p
    p $p=$p->mcseg_next
  end
end
coordsegs
*/
