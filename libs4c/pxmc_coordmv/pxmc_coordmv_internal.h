/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  pxmc_coordmv_internal.h - coordinated multiple axes movements
                            internal/private header file

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#ifndef _COORDMV_INT_H_
#define _COORDMV_INT_H_

#include <cpu_def.h>
#include "pxmc_coordmv.h"

#define pxmc_coordmv_for_mcs(_M_mcs_state,_M_reg) \
  unsigned int	 _M_reg_msk; \
  pxmc_state_t **_M_reg_ind; \
  _M_reg_msk=_M_mcs_state->mcs_con_msk; \
  _M_reg_msk&=(1<<(_M_mcs_state->mcs_con_list->pxml_cnt))-1; \
  _M_reg_ind=_M_mcs_state->mcs_con_list->pxml_arr; \
  for(;_M_reg_msk;_M_reg_msk>>=1,_M_reg_ind++) \
    if((_M_reg_msk&1)&&(_M_reg=*_M_reg_ind))

typedef struct pxmc_coordmv_seg_ops{
  int (*segop_gen)(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg);
  int (*segop_prep)(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg);
  int (*segop_get_ss)(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg, long *ss);
  int (*segop_get_es)(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg, long *es);
}pxmc_coordmv_seg_ops_t;

#define pxmc_coordmv_seg_xop(mcseg,xop) \
  (pxmc_coordmv_seg_types[(mcseg)->mcseg_type]->segop_##xop)

extern pxmc_coordmv_seg_ops_t **pxmc_coordmv_seg_types;

/* update of control parameter at sample time */
int pxmc_coordmv_upda(pxmc_coordmv_state_t *mcs_state);

int pxmc_coordmv_seg_next(pxmc_coordmv_state_t *mcs_state);

void pxmc_coordmv_seg_clean(pxmc_coordmv_state_t *mcs_state, int force);

int pxmc_coordmv_optimize(pxmc_coordmv_state_t *mcs_state);

int pxmc_coordmv_seg_add_line(pxmc_coordmv_state_t *mcs_state,
			unsigned short final_cnt, long *final, int relative_fl,
			unsigned long mintim);

int pxmc_coordmv_seg_add_spline(pxmc_coordmv_state_t *mcs_state,
			unsigned short param_cnt, long *param, int order,
			unsigned long mintim);

#endif /* _COORDMV_INT_H_ */
