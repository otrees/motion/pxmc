/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  gen_phase_table.c - tool to pre-compute phase tables
             for BLDC, synchronous and stepper motors
	     winding voltage or current commutation
             for PXMC multi axis motion controller

  (C) 2001-2011 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2011 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/* gcc -lm -Wall -O1 gen_phase_table.c -o gen_phase_table */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <getopt.h>
#include <math.h>
#include <string.h>

typedef int phase_t;

int
gen_ptable_sin(phase_t *phase, int ptirc, int ptper, int ptlen, int ptamp, float offs)
{
  int i;
  float a;

  for(i=0;i<ptlen;i++) {
    a=(i+offs)*(float)ptper*2*M_PI/ptirc;
    phase[i]=sin(a)*ptamp;
  }
  return 0;
}

int
gen_ptable_sin3ph(phase_t *phase1, phase_t *phase2, phase_t *phase3, int ptirc, int ptper, int ptlen, int ptamp)
{
  int i;
  float a;

  for(i=0;i<ptlen;i++) {
    a=i*(float)ptper*2*M_PI/ptirc;
    phase1[i]=cos(a)*ptamp;
    if (phase2 != NULL)
      phase2[i]=cos(a+2*M_PI/3)*ptamp;
    if (phase3 != NULL)
      phase3[i]=cos(a+4*M_PI/3)*ptamp;
  }
  return 0;
}

int
gen_ptable_sin3phup(phase_t *phase1, phase_t *phase2, phase_t *phase3, int ptirc, int ptper, int ptlen, int ptamp)
{
  int i;
  int min_val;
  float a, amp;
  long p1, p2, p3;

  amp=(float)ptamp/2/sin(M_PI/3);

  for(i=0;i<ptlen;i++) {
    a=i*(float)ptper*2*M_PI/ptirc;
    p1=cos(a)*amp;
    p2=cos(a+2*M_PI/3)*amp;
    p3=cos(a+4*M_PI/3)*amp;

    min_val=p1;
    if(min_val>p2)
      min_val=p2;
    if(min_val>p3)
      min_val=p3;

    phase1[i]=p1-min_val;
    if (phase2 != NULL)
      phase2[i]=p2-min_val;
    if (phase3 != NULL)
      phase3[i]=p3-min_val;
  }
  return 0;
}

int
gen_ptable_mark_zeroideg(phase_t *phase1, phase_t *phase2, phase_t *phase3, int ptirc, int ptper,
                         int ptlen, int zeroideg, int zeroimask)
{
  int i;
  float a;

  for(i=0;i<ptlen;i++) {
    a=i*(float)ptper*360/ptirc;
    if((lround(a+90.0+120*0)+zeroideg)%180<=2*zeroideg)
      phase1[i]|=zeroimask;
    if (phase2 != NULL)
      if((lround(a+90.0+120*1)+zeroideg)%180<=2*zeroideg)
        phase2[i]|=zeroimask;
    if (phase3 != NULL)
      if((lround(a+90.0+120*2)+zeroideg)%180<=2*zeroideg)
        phase3[i]|=zeroimask;
  }
  return 0;
}

/********************************************************************/

int
log2int(int x)
{
  int r=0;
  for(;x!=0;x/=2)r++;
  return r;
}

void
fprintbin(FILE *stream, int num, int dig)
{
  int c;
  for(c=1<<(dig-1);c!=0;c>>=1){
    fputc((num/c==1)?'1':'0',stream);
    num%=c;
  }
}

/********************************************************************/

int o_ptirc=4*16;
int o_ptper=1;
int o_ptamp=0x7fff;
int o_phasenum=2;
int o_zeroideg=0;
int o_zeroimask=0x8000;
int o_combined=0;
char *o_shape="sin";
char *o_format=NULL;
char *o_output=NULL;
char *o_ptable_name="smc_ptable";

int o_ptlen;
int o_ptnum;

float o_ptoffset;

char *pt_dec_modifier="";

phase_t *phases[10];

void gen_ptables(void)
{
  int i;
  if (strcmp(o_shape, "sin") == 0) {
    for(i=0;i<o_ptnum;i++){
      gen_ptable_sin(phases[i], o_ptirc, o_ptper, o_ptlen, o_ptamp, i*o_ptoffset);
    }
  }

  if (strcmp(o_shape, "sin3ph") == 0) {
      gen_ptable_sin3ph(phases[0], phases[1], phases[2], o_ptirc, o_ptper, o_ptlen, o_ptamp);
      if(o_zeroideg) {
         gen_ptable_mark_zeroideg(phases[0], phases[1], phases[2], o_ptirc, o_ptper, o_ptlen, o_zeroideg, o_zeroimask);
      }
  }

  if (strcmp(o_shape, "sin3phup") == 0) {
      gen_ptable_sin3phup(phases[0], phases[1], phases[2], o_ptirc, o_ptper, o_ptlen, o_ptamp);
      if(o_zeroideg) {
         gen_ptable_mark_zeroideg(phases[0], phases[1], phases[2], o_ptirc, o_ptper, o_ptlen, o_zeroideg, o_zeroimask);
      }
  }

}

static void
usage(void)
{
  printf("usage: gen_phase_table <options>\n");
  printf("  -i, --ptirc     <irc>      number of the irc per one phase table\n");
  printf("  -p, --ptper     <per>      number of electrical periods per one phase table\n");
  printf("  -a, --ptamp     <amp>      phase table amplitude\n");
  printf("  -n, --phasenum  <num>      number of motor phases\n");
  printf("  -z, --zeroideg  <num>      angle in degrees to skip driving winding\n");
  printf("  -c, --combined             combined phase table to conserve space\n");
  printf("  -s, --shape     <shape>    phase shape {sin,sin3ph,sin3phup}\n");
  printf("  -f, --format    <format>   output format {c, c-oneline, c-struct,\n");
  printf("                                            vhdl-bias, vhdl-sig, vhdl-unsig}\n");
  printf("  -N, --name      <name>     phase table name\n");
  printf("  -o, --output    <file>     output filename\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "ptirc",   1, 0, 'i' },
    { "ptper",   1, 0, 'p' },
    { "ptamp",   1, 0, 'a' },
    { "phasenum",1, 0, 'n' },
    { "zeroideg",1, 0, 'z' },
    { "combined",0, 0, 'c' },
    { "shape",   1, 0, 's' },
    { "format",  1, 0, 'f' },
    { "name",    1, 0, 'N' },
    { "output",  1, 0, 'o' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int opt, i, j;
  FILE *F;

  while ((opt = getopt_long(argc, argv, "i:p:a:n:z:cs:f:N:o:h",
                            &long_opts[0], NULL)) != EOF){
    switch (opt) {
    case 'i':
      o_ptirc = strtol(optarg,NULL,0);
      break;
    case 'p':
      o_ptper = strtol(optarg,NULL,0);
      break;
    case 'a':
      o_ptamp = strtol(optarg,NULL,0);
      break;
    case 'n':
      o_phasenum = strtol(optarg,NULL,0);
      break;
    case 'z':
      o_zeroideg = strtol(optarg,NULL,0);
      break;
    case 'c':
      o_combined = 1;
      break;
    case 's':
      o_shape = optarg;
      break;
    case 'f':
      o_format = optarg;
      break;
    case 'N':
      o_ptable_name = optarg;
      break;
    case 'o':
      o_output = optarg;
      break;
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }
  }

  if(o_phasenum==2)
    o_ptoffset=(float)o_ptirc/(float)o_ptper/(float)o_phasenum/2;
  else
    o_ptoffset=(float)o_ptirc/(float)o_ptper/(float)o_phasenum;

  if(!o_combined){
    o_ptlen=o_ptirc;
    o_ptnum=o_phasenum;
  }else{
    o_ptlen=o_ptirc+round(o_ptoffset*(o_phasenum-1))+1;
    o_ptnum=1;
  }

  for(i=0;i<o_ptnum;i++){
     phases[i]=malloc(sizeof(phase_t)*o_ptlen);
  }

  gen_ptables();

  if(o_output==NULL){
    F=stdout;
  }else{
    F=fopen(o_output,"w");
    if(!F){
      fprintf(stderr, "%s: cannot open file %s for writting\n", argv[0], o_output);
      exit(1);
    }
  }

  if(o_format == NULL)
    o_format = "c";

  if (o_format[0] == 'c' && (!o_format[1] || (o_format[1] == '-'))) {
    if (strcmp(o_format, "c-struct")==0)
      pt_dec_modifier="static ";

    fprintf(F,"/* Generated phase table, do not edit */\n");
    fprintf(F,"/*   gen_phase_table");
    for(i=1;i<argc;i++)
      fprintf(F," %s",argv[i]);
    fprintf(F," */\n\n");
    fprintf(F,"/* ptirc=%d ptper=%d ptamp=%d phasenum=%d combined=%d*/\n",
		  o_ptirc,o_ptper,o_ptamp,o_phasenum,o_combined);
    fprintf(F,"/* shape=\"%s\"",o_shape);
    if(o_zeroideg)
      fprintf(F," zeroideg=%d",o_zeroideg);
    fprintf(F," */\n");
    fprintf(F,"/* ptable_name=\"%s\" */\n",o_ptable_name);

    if (strcmp(o_format, "c-struct")==0){
      fprintf(F,"\n#include \"pxmc_internal.h\"\n\n");
    } else {
      fprintf(F,"\n");
      fprintf(F,"#define %s_ptirc %d\n",o_ptable_name,o_ptirc);
      fprintf(F,"#define %s_ptper %d\n",o_ptable_name,o_ptper);
      fprintf(F,"\n");
    }

    for(i=0;i<o_ptnum;i++){
      fprintf(F,"%sconst short\n%s_phase%d[]={\n",pt_dec_modifier,o_ptable_name,i+1);
      if (strcmp(o_format, "c-oneline")==0) {
        for(j=0;j<o_ptlen;j++){
          fprintf(F,"%d%s",phases[i][j],j<o_ptlen-1?",":"");
        }
        fprintf(F,"};\n\n");
      } else {
        for(j=0;j<o_ptlen;j++){
          fprintf(F,"  %6d%s",phases[i][j],j<o_ptlen-1?",\n":"");
        }
        fprintf(F,"\n};\n\n");
      }
    }

    if (strcmp(o_format, "c-struct")==0) {
      fprintf(F,"const pxmc_ptprofile_t %s={\n",o_ptable_name);
      fprintf(F,"  .ptname=\"%s\",\n",o_shape);
      fprintf(F,"  .ptid=%u,\n",0);
      fprintf(F,"  .ptirc=%u,\n",o_ptirc);
      fprintf(F,"  .ptper=%u,\n",o_ptper);
      fprintf(F,"  .ptamp=%u,\n",o_ptamp);
      fprintf(F,"  .ptphnum=%u,\n",o_phasenum);

      if(!o_combined){
        for(i=0;i<o_phasenum;i++){
          fprintf(F,"  .ptptr%d=%s_phase%d,\n",
		i+1,o_ptable_name,i+1);
        }
      }else{
        for(i=0;i<o_phasenum;i++){
          fprintf(F,"  .ptptr%d=%s_phase1+%d,\n",
		i+1,o_ptable_name,(int)round(o_ptoffset*(i)));
        }
      }
      fprintf(F,"};\n");
    } else {
      if(o_combined){
        for(i=1;i<o_phasenum;i++){
          fprintf(F,"# define %s_phase%d (%s_phase1+%d)\n",
		o_ptable_name,i+1,o_ptable_name,(int)round(o_ptoffset*(i)));
        }
      }
    }
  }

  if (strcmp(o_format,"vhdl-bias")==0) {
    int j,v,dig;
    dig=log2int(o_ptamp)+1;
    for(j=0;j<o_ptlen;j++){
      v=phases[0][j]+o_ptamp+1;
      fprintbin(F,v,dig);
      putc('\n',F);
    }
  }

  if (strcmp(o_format,"vhdl-sig")==0) {
    int j,v,dig;
    dig=log2int(o_ptamp)+1;
    for(j=0;j<o_ptlen;j++){
      v=phases[0][j];
      if(v<0)v+=(1<<dig);
      fprintbin(F,v,dig);
      putc('\n',F);
    }
  }

  if (strcmp(o_format,"vhdl-unsig")==0) {
    int j,v,dig;
    dig=log2int(o_ptamp);
    for(j=0;j<o_ptlen;j++){
      v=phases[0][j];
      if(v<0){
        fprintf(stderr,"ERROR: vhdl-unsig format specified, but there are negative values!\n");
        exit(1);
      }
      fprintbin(F,v,dig);
      putc('\n',F);
    }
  }


  fclose(F);

  return 0;
}

